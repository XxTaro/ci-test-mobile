package main.java;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.util.Locale;
import java.util.ResourceBundle;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        setUserAgentStylesheet(STYLESHEET_MODENA);


        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/resources/views/MainView.fxml"), ResourceBundle.getBundle("main.resources.bundles.Pantheon", new Locale("pt", "BR")));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.resizableProperty().setValue(Boolean.FALSE);
        primaryStage.setTitle("Pantheon");
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/main/resources/images/eletra_icon.png")));
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
