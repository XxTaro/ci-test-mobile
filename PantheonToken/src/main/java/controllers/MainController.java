/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.controllers;

import com.google.common.base.Strings;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import main.java.security.SecurityHash;

import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author darlan.sousa
 */
public class MainController implements Initializable {


    @FXML
    private TextArea textCrypt;
    @FXML
    private TextField passCrypt;
    @FXML
    private TextArea hashCrypt;

    @FXML
    private AnchorPane paneMain;

    @FXML
    private ProgressIndicator progress;

    private ResourceBundle resourceBundle;
    private Thread thMain;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.resourceBundle = rb;
        this.progress.setProgress(new Float(-1.0f));
        this.passCrypt.setText(SecurityHash.ELETRA_KEY);
        try {
            SecretKeySpec key = SecurityHash.createSecretKey(this.passCrypt.getText().toString().toCharArray(),
                    SecurityHash.SALT.trim().getBytes(), 40000, 128);
            System.out.println(SecurityHash.encrypt("6b1302d0e63d88e6", key));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @FXML
    protected void loadPt(ActionEvent event) throws IOException {
        Stage stage = (Stage) this.paneMain.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/resources/views/MainView.fxml"), ResourceBundle.getBundle("main.resources.bundles.Pantheon", new Locale("pt", "BR")));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.resizableProperty().setValue(Boolean.FALSE);
        stage.setTitle("Pantheon");
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/main/resources/images/eletra_icon.png")));
        stage.show();
    }
    @FXML
    protected void loadEn(ActionEvent event) throws IOException {
        Stage stage = (Stage) this.paneMain.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/resources/views/MainView.fxml"), ResourceBundle.getBundle("main.resources.bundles.Pantheon", new Locale("en", "EN")));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.resizableProperty().setValue(Boolean.FALSE);
        stage.setTitle("Pantheon");
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/main/resources/images/eletra_icon.png")));
        stage.show();
    }
    @FXML
    private void generateKey(ActionEvent event) throws Exception {
        System.out.println("Gerando Chave...");
        this.progress.setVisible(true);
        this.hashCrypt.setVisible(false);
        Task<Void> taskMain = new Task<Void>() {
            String pass = MainController.this.passCrypt.getText();
            String text = MainController.this.textCrypt.getText();
            String hash = null;
            @Override

            public Void call() {
                try {
                    SecretKeySpec key = SecurityHash.createSecretKey(pass.toCharArray(),
                            SecurityHash.SALT.getBytes(), 40000, 128);
                    String decryptedPassword = SecurityHash.decrypt(this.text, key);
                    this.hash = SecurityHash.encrypt(decryptedPassword + "-" + SecurityHash.ELETRA_KEY, key);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
            @Override
            protected void succeeded() {
                MainController.this.progress.setVisible(false);
                if(Strings.isNullOrEmpty(this.hash)) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle(MainController.this.resourceBundle.getString("error"));
                    alert.setHeaderText(null);
                    alert.setContentText(MainController.this.resourceBundle.getString("error_generate"));
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image("/main/resources/images/eletra_icon.png"));
                    alert.showAndWait();
                } else {
                    MainController.this.hashCrypt.setText(this.hash);
                    MainController.this.hashCrypt.setVisible(true);
                }
                super.succeeded();
            }
        };
        thMain = new Thread(taskMain);
        thMain.start();
    }
}
