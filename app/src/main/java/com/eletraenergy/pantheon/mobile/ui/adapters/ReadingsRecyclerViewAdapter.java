package com.eletraenergy.pantheon.mobile.ui.adapters;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.ui.AlarmReadingActivity;
import com.eletraenergy.pantheon.mobile.ui.AlarmSettingsReadingActivity;
import com.eletraenergy.pantheon.mobile.ui.CSInformationActivity;
import com.eletraenergy.pantheon.mobile.ui.ConsumersUnitsActivity;

import java.util.ArrayList;

/**
 * Created by dcsda on 10/01/2018.
 */

public class ReadingsRecyclerViewAdapter extends RecyclerView.Adapter<ReadingsRecyclerViewAdapter.ViewHolder> {

    protected ArrayList<ItemMain> itens;
    protected AppCompatActivity context;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView textViewHolder;
        public ImageView imageViewHolder;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            textViewHolder = (TextView) view.findViewById(R.id.textViewItemListMain);
            imageViewHolder = (ImageView) view.findViewById(R.id.imageViewItemListMain);
        }

        @Override
        public void onClick(View view) {

            //CPUInformationActivity -> 0
            if(getAdapterPosition() == 0) {
                Intent intent = new Intent(ReadingsRecyclerViewAdapter.this.context, CSInformationActivity.class);
                ReadingsRecyclerViewAdapter.this.context.startActivity(intent);
            }

            //ConsumersUnitsActivity -> 1
            if(getAdapterPosition() == 1) {
                Intent intent = new Intent(ReadingsRecyclerViewAdapter.this.context, ConsumersUnitsActivity.class);
                ReadingsRecyclerViewAdapter.this.context.startActivity(intent);
            }

            //AlarmSettingsReadingActivity -> 2
            if(getAdapterPosition() == 2) {
                Intent intent = new Intent(ReadingsRecyclerViewAdapter.this.context, AlarmSettingsReadingActivity.class);
                ReadingsRecyclerViewAdapter.this.context.startActivity(intent);
            }

            //AlarmReadingActivity -> 3
            if(getAdapterPosition() == 3) {
                Intent intent = new Intent(ReadingsRecyclerViewAdapter.this.context, AlarmReadingActivity.class);
                ReadingsRecyclerViewAdapter.this.context.startActivity(intent);
            }

        }
    }

    public ReadingsRecyclerViewAdapter(AppCompatActivity context, ArrayList<ItemMain> itens) {
        this.itens = itens;
        this.context = context;
    }

    @Override
    public ReadingsRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                     int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_main, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ItemMain it = this.itens.get(position);
        holder.textViewHolder.setText(it.text);
        holder.imageViewHolder.setImageDrawable(it.image);
    }

    @Override
    public int getItemCount() {
        return itens.size();
    }

    public static class ItemMain {
        public Drawable image;
        public String text;
    }
}
