package com.eletraenergy.pantheon.mobile.ui.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.eletraenergy.pantheon.mobile.ui.fragments.ActionsFragment;
import com.eletraenergy.pantheon.mobile.ui.fragments.ChangesFragment;
import com.eletraenergy.pantheon.mobile.ui.fragments.ReadingsFragment;

/**
 * Created by dcsda on 10/01/2018.
 */

public class MainSectionsPagerAdapter extends FragmentPagerAdapter {

    public MainSectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0) return ReadingsFragment.newInstance();
        if(position == 1) return ChangesFragment.newInstance();
        if(position == 2) return ActionsFragment.newInstance();
        return ReadingsFragment.newInstance();
    }

    @Override
    public int getCount() {
        return 3;
    }
}
