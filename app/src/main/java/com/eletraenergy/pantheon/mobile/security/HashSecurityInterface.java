package com.eletraenergy.pantheon.mobile.security;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Diego Maia.
 */

interface HashSecurityInterface {

    String calculateHash(String androidUniqueID) throws GeneralSecurityException, UnsupportedEncodingException;

    SecretKeySpec createSecretKey(char[] password, byte[] salt, int iterationCount, int keyLength) throws NoSuchAlgorithmException, InvalidKeySpecException;

    String encrypt(String property, SecretKeySpec key) throws GeneralSecurityException, UnsupportedEncodingException;

    String decrypt(String string, SecretKeySpec key) throws GeneralSecurityException, IOException;

    boolean isHashValid(String hash, String androidID);
}
