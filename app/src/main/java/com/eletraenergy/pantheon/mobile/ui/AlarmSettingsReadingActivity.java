package com.eletraenergy.pantheon.mobile.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadAsyncTask;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadEnum;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadTitle;
import com.eletraenergy.pantheon.mobile.ui.adapters.AlarmSettingsReadingRecyclerViewAdapter;
import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.eletra.mci.dlms.DLMSAtt;

public class AlarmSettingsReadingActivity extends PantheonCompatActivity {

    protected Toolbar toolbar;
    protected EletraMCIReadAsyncTask eletraMCIReadAsyncTask;
    protected RecyclerView recyclerView;
    protected RelativeLayout relativeLayoutLoad;
    protected ProgressBar progressBar;
    protected RelativeLayout relativeLayoutError;
    protected TextView textView;
    protected EletraMCIReadTitle eletraMCIReadTitle;
    protected SwipeRefreshLayout swipeRefreshLayout;
    protected Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_settings_reading);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);

        eletraMCIReadTitle = new EletraMCIReadTitle();

        progressBar = (ProgressBar) findViewById(R.id.progressBarAlarmSettings);
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(AlarmSettingsReadingActivity.this);
        circularProgressDrawable.setStyle(CircularProgressDrawable.LARGE);
        circularProgressDrawable.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        progressBar.setIndeterminateDrawable(circularProgressDrawable);

        relativeLayoutLoad = (RelativeLayout) findViewById(R.id.relativeLayoutLoadAlarmSettings);
        relativeLayoutError = (RelativeLayout) findViewById(R.id.relativeLayoutErrorAlarmSettings);
        textView = (TextView) findViewById(R.id.textViewAlarmSettings);
        button = (Button) findViewById(R.id.buttonAlarmSettings);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshCPUInformation);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewCPUInformation);
        LinearLayoutManager layoutManager = new LinearLayoutManager(AlarmSettingsReadingActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        swipeRefreshLayout.setOnRefreshListener(() -> AlarmSettingsReadingActivity.this.loadAlarmSettings());
    }

    @Override
    protected void onResume() {
        super.onResume();
        relativeLayoutError.setVisibility(View.GONE);
        swipeRefreshLayout.setVisibility(View.GONE);
        relativeLayoutLoad.setVisibility(View.VISIBLE);
        this.loadAlarmSettings();
    }

    protected void loadAlarmSettings() {
        if (eletraMCIReadAsyncTask != null) eletraMCIReadAsyncTask.cancel(true);
        this.eletraMCIReadAsyncTask = new EletraMCIReadAsyncTask(
                AlarmSettingsReadingActivity.this,
                EletraMCIReadEnum.ALARM_CONFIGURATION,
                (atts, error)-> {
                    if(error > 0) {
                        textView.setText(error);
                        if(error == R.string.exception_generic) {
                            textView.setText(R.string.exception_generic_alarm_settings);
                        }
                        this.showButtonErro(error);
                        relativeLayoutLoad.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        swipeRefreshLayout.setVisibility(View.GONE);
                        relativeLayoutError.setVisibility(View.VISIBLE);
                    } else {
                        this.postRead(atts);
                    }
                },
                task -> {
                    if(this.isHightLevel() > 0) {
                        this.preRead(task);
                    }
                }
        );
        this.eletraMCIReadAsyncTask.execute();
    }

    protected void showButtonErro(int err) {
        if(err == R.string.error_level) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmSettingsReadingActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_auth) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmSettingsReadingActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_encrypt) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmSettingsReadingActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_security) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmSettingsReadingActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_ip) {
            button.setText(R.string.bt_communication_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmSettingsReadingActivity.this, CommunicationSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_port) {
            button.setText(R.string.bt_communication_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmSettingsReadingActivity.this, CommunicationSettingActivity.class));
            });
            return;
        }
        button.setText(R.string.bt_retry);
        button.setVisibility(View.VISIBLE);
        button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_refresh_black_24dp), null, null, null);
        button.setOnClickListener(v -> {
            relativeLayoutError.setVisibility(View.GONE);
            relativeLayoutLoad.setVisibility(View.VISIBLE);
            this.loadAlarmSettings();
        });
        return;
    }

    @Override
    public boolean onSupportNavigateUp(){
        this.onBackPressed();
        return true;
    }

    @Override
    public void onDestroy() {
        if (eletraMCIReadAsyncTask != null) eletraMCIReadAsyncTask.cancel(true);
        super.onDestroy();
    }

    protected void postRead(List<DLMSAtt> attributes) {

        //mask->0
        int mask = Integer.parseInt(attributes.get(0).getValueProperty());
        //config->1
        int config = Integer.parseInt(attributes.get(1).getValueProperty());

        boolean doorAlarm = (config & 0x01) == 0x01;
        boolean collectiveOpenAlarm = (config & 0x02) == 0x02;
        boolean rollbackAlarm = (config & 0x04) == 0x04;
        boolean hhuWriteAlarm = (config & 0x08) == 0x08;

        ArrayList<AlarmSettingsReadingRecyclerViewAdapter.AlarmSettingsReadingItem> itens = new ArrayList<AlarmSettingsReadingRecyclerViewAdapter.AlarmSettingsReadingItem>();
        AlarmSettingsReadingRecyclerViewAdapter.AlarmSettingsReadingItem it = new AlarmSettingsReadingRecyclerViewAdapter.AlarmSettingsReadingItem();
        it.title = getString(R.string.open_port_alarm);
        it.value = doorAlarm;
        itens.add(it);

        it = new AlarmSettingsReadingRecyclerViewAdapter.AlarmSettingsReadingItem();
        it.title = getString(R.string.collective_open_relay_alarm);
        it.value = collectiveOpenAlarm;
        itens.add(it);

        it = new AlarmSettingsReadingRecyclerViewAdapter.AlarmSettingsReadingItem();
        it.title = getString(R.string.hhu);
        it.value = hhuWriteAlarm;
        itens.add(it);

        AlarmSettingsReadingRecyclerViewAdapter adapter = new AlarmSettingsReadingRecyclerViewAdapter((AppCompatActivity) AlarmSettingsReadingActivity.this, itens);
        recyclerView.setAdapter(adapter);

        relativeLayoutError.setVisibility(View.GONE);
        relativeLayoutLoad.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setVisibility(View.VISIBLE);
    }

    protected void preRead(AsyncTask task) {
        task.cancel(true);
        textView.setText(this.isHightLevel());
        this.showButtonErro(this.isHightLevel());
        relativeLayoutLoad.setVisibility(View.GONE);
        swipeRefreshLayout.setVisibility(View.GONE);
        relativeLayoutError.setVisibility(View.VISIBLE);
    }
}
