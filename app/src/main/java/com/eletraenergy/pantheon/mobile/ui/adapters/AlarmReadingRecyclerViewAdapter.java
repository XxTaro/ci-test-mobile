package com.eletraenergy.pantheon.mobile.ui.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.ui.AlarmActivity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by dcsda on 16/01/2018.
 */

public class AlarmReadingRecyclerViewAdapter extends RecyclerView.Adapter<AlarmReadingRecyclerViewAdapter.ViewHolder> {

    protected List<AlarmReadingItem> itens;
    protected AppCompatActivity context;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView text1;
        public TextView text2;
        public ImageView imageView;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            text1 = (TextView) view.findViewById(android.R.id.text1);
            text2 = (TextView) view.findViewById(android.R.id.text2);
        }

        @Override
        public void onClick(View v) {
            AlarmReadingItem item = itens.get(getAdapterPosition());

            Intent intent = new Intent(AlarmReadingRecyclerViewAdapter.this.context, AlarmActivity.class);
            intent.putExtra("ITEM", item);
            AlarmReadingRecyclerViewAdapter.this.context.startActivity(intent);

        }
    }

    public AlarmReadingRecyclerViewAdapter(AppCompatActivity context, List<AlarmReadingItem> itens) {
        this.itens = itens;
        this.context = context;
    }

    @Override
    public AlarmReadingRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                         int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_alarm_reading, parent, false);
        AlarmReadingRecyclerViewAdapter.ViewHolder vh = new AlarmReadingRecyclerViewAdapter.ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(AlarmReadingRecyclerViewAdapter.ViewHolder holder, int position) {
        AlarmReadingItem it = this.itens.get(position);
        holder.text1.setText(it.index + " - " +context.getResources().getString(it.title));
        holder.text2.setText(it.dateTime);
    }

    @Override
    public int getItemCount() {
        return itens.size();
    }

    public enum PantheonAlarmType {
        DISCONNECTION(0x3C),
        CONNECTION(0x3E),
        CONFIGURATION(0x40),
        DOOR_OPEN(0x81),
        UNKNOW(0x00);
        private int id;
        PantheonAlarmType(Integer id) {
            this.id = id;
        }
        public int getId() {
            return id;
        }
    }

    public static class AlarmReadingItem implements Serializable {
        public int index;
        public String user;
        public byte[] rawData;
        public String dateTime;
        public int type;
        public int title;
    }
}