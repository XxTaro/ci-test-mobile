package com.eletraenergy.pantheon.mobile.ui;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.eletraenergy.pantheon.mobile.R;
import com.google.android.material.appbar.AppBarLayout;

public class InstructionsActivity extends PantheonCompatActivity {

    protected Toolbar toolbar;
    protected TextView versionTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructions);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);

        PackageInfo pinfo = null;
        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        int versionNumber = pinfo.versionCode;
        String versionName = pinfo.versionName;

        versionTextView = (TextView) findViewById(R.id.versionTextView);
        versionTextView.setText(getString(R.string.version) + " " + versionName);
    }

    @Override
    public boolean onSupportNavigateUp(){
        this.onBackPressed();
        return true;
    }
}
