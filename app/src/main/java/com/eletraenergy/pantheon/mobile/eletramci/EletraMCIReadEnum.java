package com.eletraenergy.pantheon.mobile.eletramci;

/**
 * Created by dcsda on 16/01/2018.
 */

public enum EletraMCIReadEnum {

    CPU_INFORMATION("mass_cpu"),
    RC_INFORMATION("mass_rc"),
    CONSUMER_UNIT("consumer_units"),
    RELAY_OPERATION_ACTION("relay_operation_action"),
    ALARM_CONFIGURATION("alarm_configuration"),
    ALARM_READ("alarm_read"),

    SOFTWARE_VERSION("programVersion"),
    FIRMWARE_VERSION("firmwareVersion"),
    CPU_ADDRESS("serialNumber"),
    CPU_DATE_TIME("dateAndTime"),
    DIGITAL_INPUT_STATUS("portStatus"),
    COLLECTIVE_OPEN_RELAY_STATUS("collectiveOpening"),
    COLLECTIVE_CLOSE_RELAY_STATUS("collectiveClosuring"),
    INSTALLATION_STATUS("instalationType"),
    METER_AUTO_DETECTION("Meter_auto-detection_information"),
    CPU_METER_LIST("cpu_meter_list"),
    METERS_SPECIFICATION("meters_Specification"),
    FLAGS("flags"),
    CONTROL_PARAMETER("control_parameter"),
    RELAY_STATUS("relays_status"),
    DIRECT_ACTIVE_ENERGY("direct_active_energy"),
    REVERSE_ACTIVE_ENERGY("reverse_active_energy"),
    VOLTAGE("voltage"),
    CURRENT("current"),
    POWER("active_power"),
    UPDATE_METER_FILE("meter_file_update"),
    CPU_GENERAL_STATE("cpuGeneralState"),
    METER_POS("meterPos"),
    METER_NUM("meterNum"),
    METER_TYPE("meterType"),
    ONLINE_READ("online_read"),
    CHECKPOINT_READ("checkpoint_read"),
    ALARMREAD("alarmRead"),
    RC_GENERAL_STATE("rcGeneralState");

    private final String readingCommand;

    EletraMCIReadEnum(String readingCommand) {
        this.readingCommand = readingCommand;
    }
    public String getReadingCommand() {
        return readingCommand;
    }
}
