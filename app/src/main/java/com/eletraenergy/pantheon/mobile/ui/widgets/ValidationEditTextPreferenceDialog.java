package com.eletraenergy.pantheon.mobile.ui.widgets;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import androidx.preference.PreferenceDialogFragmentCompat;

import com.eletraenergy.pantheon.mobile.R;
import com.google.android.material.textfield.TextInputLayout;

/**
 * Created by dcsda on 15/01/2018.
 */

public class ValidationEditTextPreferenceDialog extends PreferenceDialogFragmentCompat {

    protected TextInputLayout textInputLayout;
    protected EditText editText;
    protected ValidationEditTextPreference preference;

    public static ValidationEditTextPreferenceDialog newInstance(String key) {
        final ValidationEditTextPreferenceDialog
                fragment = new ValidationEditTextPreferenceDialog();
        final Bundle bd = new Bundle(1);
        bd.putString(ARG_KEY, key);
        fragment.setArguments(bd);
        return fragment;
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        preference = (ValidationEditTextPreference)getPreference();
        textInputLayout = (TextInputLayout) view.findViewById(R.id.textError);
        textInputLayout.setErrorEnabled(true);
        if(preference != null && preference.getText() != null) {
            if (preference.getText().length() != preference.getLength()) {
                textInputLayout.setErrorEnabled(true);
                textInputLayout.setError(preference.getText().length() + "/" + preference.getLength());
            } else {
                textInputLayout.setErrorEnabled(false);
            }
        } else {
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError("0/" + preference.getLength());
        }
        editText = (EditText) view.findViewById(android.R.id.edit);
        editText.setText(preference.getText());
        editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(preference.getLength()), new InputFilter.AllCaps()});
        editText.addTextChangedListener(new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String text = charSequence.toString();
                if(text.length() != preference.getLength()) {
                    textInputLayout.setErrorEnabled(true);
                    textInputLayout.setError(text.length() + "/" + preference.getLength());
                } else {
                    textInputLayout.setErrorEnabled(false);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            if(preference.getLength() == editText.getText().toString().length()) {
                preference.setText(editText.getText().toString());
                preference.setSummary(editText.getText().toString());
            } else {
                preference.setSummary(getString(R.string.error_key_size));
                //Snackbar.make(getView(), getString(R.string.error_key_size), Snackbar.LENGTH_LONG).show();
            }
        }
    }
}
