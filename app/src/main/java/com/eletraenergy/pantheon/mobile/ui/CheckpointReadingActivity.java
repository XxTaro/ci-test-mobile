package com.eletraenergy.pantheon.mobile.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadAsyncTask;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadEnum;
import com.eletraenergy.pantheon.mobile.ui.adapters.CheckpointReadingRecyclerViewAdapter;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.common.base.Strings;

import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.eletra.mci.dlms.DLMSAtt;
import br.com.eletra.mci.util.HexString;

public class CheckpointReadingActivity extends PantheonCompatActivity {

    protected Toolbar toolbar;
    protected EletraMCIReadAsyncTask eletraMCIReadAsyncTask;
    protected RecyclerView recyclerView;
    protected RelativeLayout relativeLayoutLoad;
    protected ProgressBar progressBar;
    protected RelativeLayout relativeLayoutError;
    protected TextView textView;
    protected SwipeRefreshLayout swipeRefreshLayout;
    protected Button button;

    protected AppBarLayout appBarLayout;
    protected CollapsingToolbarLayout collapsingToolbarLayout;

    protected TextView openPortAlarmTextView, collectiveOpenRelayTextView, hhuTextView, portStatusTextView, dateTimeTextView;

    private boolean isCollapsed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkpoint_reading);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        isCollapsed = false;
        appBarLayout = (AppBarLayout) findViewById(R.id.appBarLayout);
        appBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()) {
                // Collapsed
                isCollapsed = true;
            } else if (verticalOffset == 0) {
                // Expanded
                isCollapsed = false;
            } else {
                // Somewhere in between
                isCollapsed = false;
            }
        });


        progressBar = (ProgressBar) findViewById(R.id.progressBarCheckpointRead);
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(CheckpointReadingActivity.this);
        circularProgressDrawable.setStyle(CircularProgressDrawable.LARGE);
        circularProgressDrawable.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        progressBar.setIndeterminateDrawable(circularProgressDrawable);

        relativeLayoutLoad = (RelativeLayout) findViewById(R.id.relativeLayoutLoadCheckpointRead);
        relativeLayoutError = (RelativeLayout) findViewById(R.id.relativeLayoutErrorCheckpointRead);
        textView = (TextView) findViewById(R.id.textViewCheckpointRead);
        button = (Button) findViewById(R.id.buttonCheckpointRead);

        openPortAlarmTextView = (TextView) findViewById(R.id.alarmOpenPortTextView);
        collectiveOpenRelayTextView = (TextView) findViewById(R.id.alarmCollectiveOpenPortTextView);
        hhuTextView = (TextView) findViewById(R.id.alarmhhuTextView);
        portStatusTextView = (TextView) findViewById(R.id.portStatusTextView);
        dateTimeTextView = (TextView) findViewById(R.id.dateTimeTextView);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshCheckpointRead);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewCheckpointRead);
        LinearLayoutManager layoutManager = new LinearLayoutManager(CheckpointReadingActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(false);
        //this.loadCheckpointRead();
        swipeRefreshLayout.setOnRefreshListener(() -> {
            if(!isCollapsed) {
                appBarLayout.setExpanded(false, true);
            }
            CheckpointReadingActivity.this.loadCheckpointRead();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        relativeLayoutError.setVisibility(View.GONE);
        swipeRefreshLayout.setVisibility(View.GONE);
        relativeLayoutLoad.setVisibility(View.VISIBLE);
        appBarLayout.setExpanded(false);
        this.loadCheckpointRead();
    }

    protected void loadCheckpointRead() {
        if (eletraMCIReadAsyncTask != null) eletraMCIReadAsyncTask.cancel(true);
        this.eletraMCIReadAsyncTask = new EletraMCIReadAsyncTask(
                CheckpointReadingActivity.this,
                EletraMCIReadEnum.CHECKPOINT_READ,
                (attrs, error)-> {
                    if(error > 0) {
                        textView.setText(error);
                        if(error == R.string.exception_generic) {
                            textView.setText(R.string.exception_generic_units);
                        }
                        this.showButtonErro(this.isHightLevel());
                        relativeLayoutLoad.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        swipeRefreshLayout.setVisibility(View.GONE);
                        relativeLayoutError.setVisibility(View.VISIBLE);
                        if(!isCollapsed) {
                            appBarLayout.setExpanded(false, true);
                        }
                    } else {
                        this.postRead(attrs);
                    }
                },
                task -> {
                    if(this.isHightLevel() > 0) {
                        this.preRead(task);
                    }
                }
        );
        this.eletraMCIReadAsyncTask.execute();
    }

    protected void showButtonErro(int err) {
        if(err == R.string.error_level) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(CheckpointReadingActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_auth) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(CheckpointReadingActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_encrypt) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(CheckpointReadingActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_security) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(CheckpointReadingActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_ip) {
            button.setText(R.string.bt_communication_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(CheckpointReadingActivity.this, CommunicationSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_port) {
            button.setText(R.string.bt_communication_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(CheckpointReadingActivity.this, CommunicationSettingActivity.class));
            });
            return;
        }
        button.setText(R.string.bt_retry);
        button.setVisibility(View.VISIBLE);
        button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_refresh_black_24dp), null, null, null);
        button.setOnClickListener(v -> {
            relativeLayoutError.setVisibility(View.GONE);
            relativeLayoutLoad.setVisibility(View.VISIBLE);
            this.loadCheckpointRead();
        });
        return;
    }

    @Override
    public boolean onSupportNavigateUp(){
        this.onBackPressed();
        return true;
    }

    @Override
    public void onDestroy() {
        if (eletraMCIReadAsyncTask != null) eletraMCIReadAsyncTask.cancel(true);
        super.onDestroy();
    }
    public static byte[] hexStringToByteArray(String s) {
        String[] sBytes = s.split(" ");
        byte data[] = new byte[sBytes.length];
        for(int i=0; i < sBytes.length; i++) {
            data[i] = (Integer.decode("0x"+sBytes[i])).byteValue();
        }
        return data;
    }
    protected void postRead(List<DLMSAtt> attributes) {
        HashMap<Integer, String> phaseMap = new HashMap<Integer, String>();
        phaseMap.put(1, "A");
        phaseMap.put(2, "B");
        phaseMap.put(3, "C");

        phaseMap.put(4, "A");
        phaseMap.put(5, "B");
        phaseMap.put(6, "C");

        phaseMap.put(7, "A");
        phaseMap.put(8, "B");
        phaseMap.put(9, "C");

        phaseMap.put(10, "A");
        phaseMap.put(11, "B");
        phaseMap.put(12, "C");

        ArrayList<CheckpointReadingRecyclerViewAdapter.CheckpointReadItem> itens = new ArrayList<CheckpointReadingRecyclerViewAdapter.CheckpointReadItem>();
        CheckpointReadingRecyclerViewAdapter.CheckpointReadItem it;

        String meterNumber = attributes.get(0).getValueProperty();

        String attDateTime = attributes.get(1).getAtt().get(0).getValueProperty();
       /*
        byte[] dateTime = ByteBuffer.allocate(4).putInt(Integer.parseInt(attDateTime)).array();
        String[] sDateTime = attDateTime.split(" ");
        int year = ByteBuffer.wrap(new byte[]{0x00, 0x00, dateTime[0], dateTime[1]}).getInt();
        int month = Integer.decode("0x"+sDateTime[2]);
        int day = Integer.decode("0x"+sDateTime[3]);
        int hour = Integer.decode("0x"+sDateTime[5]);
        int minutes = Integer.decode("0x"+sDateTime[6]);
        int seconds = Integer.decode("0x"+sDateTime[7]);
        */
        dateTimeTextView.setText(getString(R.string.date_time) + ": " + attDateTime);



        byte[] cpuFlags = ByteBuffer.allocate(4).putInt(Integer.parseInt(attributes.get(1).getAtt().get(1).getValueProperty())).array();
        cpuFlags = HexString.hexStringToByteArray(HexString.bytesToHexString(cpuFlags).substring(9));

        Log.i("Alarme", "Alarmes: " +  HexString.bytesToHexString(cpuFlags));

        boolean alarmOpenPort = (cpuFlags[0] & 0x01) == 0x01;
        boolean openCollective = (cpuFlags[0] & 0x02) ==0x02;
        boolean hhu = (cpuFlags[0] & 0x04) == 0x04;
        boolean openPort = (cpuFlags[0] & 0x08) == 0x08;

        Log.i("Alarme", "Alarme de abertura de porta: " + (alarmOpenPort ? getResources().getString(R.string.enabled) : getResources().getString(R.string.disabled)));
        Log.i("Alarme", "Alarme de abertura de coletiva de relé: " + (openCollective ? getResources().getString(R.string.enabled) : getResources().getString(R.string.disabled)));
        Log.i("Alarme", "HHU: " + (hhu ? getResources().getString(R.string.enabled) : getResources().getString(R.string.disabled)));
        Log.i("Alarme", "Estado da porta: " + (openPort ? getResources().getString(R.string.opened) : getResources().getString(R.string.closed)));


        openPortAlarmTextView.setText(alarmOpenPort ? R.string.enabled : R.string.disabled);
        openPortAlarmTextView.setTextColor(alarmOpenPort ? getResources().getColor(android.R.color.holo_green_dark) : getResources().getColor(android.R.color.holo_red_dark));

        collectiveOpenRelayTextView.setText(openCollective ? R.string.enabled : R.string.disabled);
        collectiveOpenRelayTextView.setTextColor(openCollective ? getResources().getColor(android.R.color.holo_green_dark) : getResources().getColor(android.R.color.holo_red_dark));

        hhuTextView.setText(hhu ? R.string.enabled : R.string.disabled);
        hhuTextView.setTextColor(hhu ? getResources().getColor(android.R.color.holo_green_dark) : getResources().getColor(android.R.color.holo_red_dark));

        portStatusTextView.setText(openPort ? R.string.closed : R.string.opened);

        for(DLMSAtt attribute : attributes.get(2).getAtt()) {
            it = new CheckpointReadingRecyclerViewAdapter.CheckpointReadItem();
            String property = attribute.getValueProperty();
            if(Strings.isNullOrEmpty(property)) continue;

            byte[] consumerData =  hexStringToByteArray(property);
            double a1 = ((double) ByteBuffer.wrap(new byte[]{consumerData[8], consumerData[9], consumerData[10], consumerData[11]}).getInt()) / 100;
            double a2 = ((double) ByteBuffer.wrap(new byte[]{consumerData[12], consumerData[13], consumerData[14], consumerData[15]}).getInt()) / 100;

            //double a1 = Double.parseDouble(attribute.getAtt().get(3).getValueProperty()) / 100;
            //double a2 = Double.parseDouble(attribute.getAtt().get(4).getValueProperty()) / 100;

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            int digitsDecimal = Integer.parseInt(prefs.getString(getString(R.string.key_decimal_digits), "2"));
            int digitsInteger = Integer.parseInt(prefs.getString(getString(R.string.key_integer_digits), "2"));

            String sDirect = String.valueOf(new DecimalFormat("#.00").format(a1)).replace(".", ",");
            String sReverse = String.valueOf(new DecimalFormat("#.00").format(a2)).replace(".", ",");

            String[] dir = sDirect.split(",");
            String[] rev = sReverse.split(",");

            String intDir = Strings.padStart(dir[0], digitsInteger, '0');
            String intRev = Strings.padStart(rev[0], digitsInteger, '0');

            if(digitsDecimal == 1) {
                intDir += "," + dir[1].substring(0, dir[1].length() - 1);
                intRev += "," + rev[1].substring(0, dir[1].length() - 1);
            }
            if(digitsDecimal == 2) {
                intDir += "," + dir[1];
                intRev += "," + rev[1];
            }

            it.direct = "+A " + intDir + " kWh";
            it.reverse = "-A " +  intRev + " kWh";

            Log.i(CheckpointReadingActivity.class.getSimpleName(), it.direct);
            Log.i(CheckpointReadingActivity.class.getSimpleName(), it.reverse);

            String[] sBytes = property.split(" ");
            it.number = sBytes[0] +  sBytes[1] + sBytes[2] + sBytes[3] + sBytes[4];

            Log.i("DIRETA", it.number);

            int type = Integer.decode("0x"+sBytes[5].substring(0, 1));
            Log.i("TYPE", type + "");

            int pos = Integer.decode("0x"+sBytes[5].substring(1));
            Log.i("POS", pos + "");

            it.pos = new String[3];
            it.phase = new String[3];
            it.relay = new Integer[3];
            it.check = new Integer[3];

            int n1 = Integer.decode("0x"+sBytes[7].substring(1));
            int n2 = Integer.decode("0x"+sBytes[7].substring(0, 1));
            int n3 = Integer.decode("0x"+sBytes[6].substring(1));


            it.pos[0] = String.valueOf(pos);
            it.phase[0] = phaseMap.get(pos);
            it.relay[0] = getRelayStatus(n1);
            it.check[0] = getCheckStatus(n1);

            if(type > 0) {
                it.pos[1] = String.valueOf(pos + 1);
                it.phase[1] = phaseMap.get(pos + 1);
                it.relay[1] = getRelayStatus(n2);
                it.check[1] = getCheckStatus(n2);
            }

            if(type > 1) {
                it.pos[2] = String.valueOf(pos + 2);
                it.phase[2] = phaseMap.get(pos + 2);
                it.relay[2] = getRelayStatus(n3);
                it.check[2] = getCheckStatus(n3);
            }

            it.isEnable = true;
            itens.add(it);

            // it.number = attribute.getAtt().get(0).getValueProperty();
            /*
            double a1 = Double.parseDouble(attribute.getAtt().get(3).getValueProperty()) / 100;
            double a2 = Double.parseDouble(attribute.getAtt().get(4).getValueProperty()) / 100;

            Log.i("Valores +A", attribute.getAtt().get(3).getValueProperty());
            Log.i("Valores -A", attribute.getAtt().get(4).getValueProperty());
            Log.i("Valores +A", new DecimalFormat("#.000").format(a1));
            Log.i("Valores -A", new DecimalFormat("#.000").format(a2));

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            int digitsDecimal = Integer.parseInt(prefs.getString(getString(R.string.key_decimal_digits), "2"));
            int digitsInteger = Integer.parseInt(prefs.getString(getString(R.string.key_integer_digits), "2"));

            String sDirect = String.valueOf(new DecimalFormat("#.00").format(a1)).replace(".", ",");
            String sReverse = String.valueOf(new DecimalFormat("#.00").format(a2)).replace(".", ",");

            String[] dir = sDirect.split(",");
            String[] rev = sReverse.split(",");

            String intDir = Strings.padStart(dir[0], digitsInteger, '0');
            String intRev = Strings.padStart(rev[0], digitsInteger, '0');

            if(digitsDecimal == 1) {
                intDir += "," + dir[1].substring(0, dir[1].length() - 1);
                intRev += "," + rev[1].substring(0, dir[1].length() - 1);
            }
            if(digitsDecimal == 2) {
                intDir += "," + dir[1];
                intRev += "," + rev[1];
            }

            it.direct = "+A " + intDir + " kWh";
            it.reverse = "-A " +  intRev + " kWh";

            it.phase = new String[3];
            it.relay = new Integer[3];
            it.pos = new String[3];

            List<DLMSAtt> subAttributes = attribute.getAtt().get(5).getAtt();
            for(int cont = 0; cont <  subAttributes.size() ; cont ++) {
                it.pos[cont] = String.valueOf(Integer.parseInt(attribute.getAtt().get(1).getValueProperty()) + cont);
                it.phase[cont] = phaseMap.get(Integer.parseInt(attribute.getAtt().get(1).getValueProperty()) + cont);

                double a3 = Double.parseDouble(subAttributes.get(cont).getAtt().get(1).getValueProperty()) / 100;
                String vl1 = Strings.padStart(String.valueOf(new DecimalFormat("#.00").format(a3)).replace(".", ","), 4, '0');


                double a4 = Double.parseDouble(subAttributes.get(cont).getAtt().get(2).getValueProperty()) / 100;
                String vl2 = Strings.padStart(String.valueOf(new DecimalFormat("#.00").format(a4)).replace(".", ","), 4, '0');
                int relayStatus = Integer.parseInt(subAttributes.get(cont).getAtt().get(3).getValueProperty());

                if ((relayStatus & 0x01) == 0x01) {
                    // reading failed
                    it.relay[cont] = R.drawable.relay_warning;
                } else if ((relayStatus & 0x04) == 0x00) {
                    // relay error
                    it.relay[cont] = R.drawable.relay_open_volt;
                } else if ((relayStatus & 0x02) == 0x00) {
                    // closed relay
                    it.relay[cont] = R.drawable.relay_closed;
                } else {
                    // value = 6
                    // open relay
                    it.relay[cont] = R.drawable.relay_open;
                }
            }
            it.isEnable = true;
            itens.add(it);
            */
        }

        CheckpointReadingRecyclerViewAdapter adapter = new CheckpointReadingRecyclerViewAdapter((AppCompatActivity) CheckpointReadingActivity.this, itens);
        recyclerView.setAdapter(adapter);
        relativeLayoutError.setVisibility(View.GONE);
        relativeLayoutLoad.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setVisibility(View.VISIBLE);
        if(isCollapsed) {
            appBarLayout.setExpanded(true, true);
        }
    }
    private int getRelayStatus(int value) {
        int state = R.drawable.relay_warning;
        switch ((value & 0x03)) {
            case 0:
                state = R.drawable.relay_open;
                if((value & 0x04) == 4) {
                    state = R.drawable.relay_open_volt;
                }
                break;
            case 1:
                state = R.drawable.relay_closed;
                break;
            case 2:
                state = R.drawable.relay_open_warning;
                if((value & 0x04) == 4) {
                    state = R.drawable.relay_open_warning_volt_v7;
                }
                break;
        }
        return state;
    }
    private int getCheckStatus(int value) {
        int state = R.drawable.error;
        switch ((value & 0x08)) {
            case 0:
                state = R.drawable.check;
                break;
            case 1:
                state = R.drawable.error;
                break;
        }
        return state;
    }

    protected void preRead(AsyncTask task) {
        task.cancel(true);
        textView.setText(this.isHightLevel());
        this.showButtonErro(this.isHightLevel());
        relativeLayoutLoad.setVisibility(View.GONE);
        swipeRefreshLayout.setVisibility(View.GONE);
        relativeLayoutError.setVisibility(View.VISIBLE);
        if(!isCollapsed) {
            appBarLayout.setExpanded(false, true);
        }
    }
}