package com.eletraenergy.pantheon.mobile.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadTitle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dcsda on 16/01/2018.
 */

public class OnlineReadingRecyclerViewAdapter extends RecyclerView.Adapter<OnlineReadingRecyclerViewAdapter.ViewHolder> {

    protected List<OnlineReadItem> itens;
    protected AppCompatActivity context;
    protected EletraMCIReadTitle eletraMCIReadTitle;
    HashMap<Integer, String> phaseMap = new HashMap<Integer, String>();

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView numberTextViewHolder;
        public TextView posTextViewHolder;
        public TextView directTextViewHolder;
        public TextView reverseTextViewHolder;
        public TextView phase1TextViewHolder;
        public TextView phase2TextViewHolder;
        public TextView phase3TextViewHolder;
        public TextView pos1TextViewHolder;
        public TextView pos2TextViewHolder;
        public TextView pos3TextViewHolder;
        public LinearLayout layout1TextViewHolder;
        public LinearLayout layout2TextViewHolder;
        public LinearLayout layout3TextViewHolder;
        public ImageView relay1ImageViewHolder;
        public ImageView relay2ImageViewHolder;
        public ImageView relay3ImageViewHolder;

        public LinearLayout layoutCheck1TextViewHolder;
        public LinearLayout layoutCheck2TextViewHolder;
        public LinearLayout layoutCheck3TextViewHolder;
        public ImageView check1ImageViewHolder;
        public ImageView check2ImageViewHolder;
        public ImageView check3ImageViewHolder;

        public ImageButton imageButtonHolder;
        public LinearLayout detailLinearLayoutHolder;
        public LinearLayout enableLinearLayoutHolder;
        public LinearLayout disableLinearLayoutHolder;
        public TextView disPosTextViewHolder;
        public TextView disPhaseTextViewHolder;

        public ImageView imageViewViewHolder;

        public ViewHolder(View view) {
            super(view);

            imageViewViewHolder = (ImageView) view.findViewById(R.id.imageViewItemOnlineRead);

            imageButtonHolder = (ImageButton) view.findViewById(R.id.imageButtonItemOnlineRead);
            detailLinearLayoutHolder = (LinearLayout) view.findViewById(R.id.detailLinearLayoutItemOnlineRead);

            disableLinearLayoutHolder = (LinearLayout) view.findViewById(R.id.disablePositionLinearLayout);
            enableLinearLayoutHolder = (LinearLayout) view.findViewById(R.id.enablePositionLinearLayout);

            imageButtonHolder.setOnClickListener(v -> {
                OnlineReadItem it = OnlineReadingRecyclerViewAdapter.this.itens.get(getAdapterPosition());
                if (!it.isOpen) {
                    imageButtonHolder.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                    detailLinearLayoutHolder.setVisibility(View.VISIBLE);
                    OnlineReadingRecyclerViewAdapter.this.itens.get(getAdapterPosition()).isOpen = true;
                } else {
                    imageButtonHolder.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                    detailLinearLayoutHolder.setVisibility(View.GONE);
                    OnlineReadingRecyclerViewAdapter.this.itens.get(getAdapterPosition()).isOpen = false;
                }
            });

            disPosTextViewHolder = (TextView) view.findViewById(R.id.posDisTextViewItemOnlineRead);
            disPhaseTextViewHolder = (TextView) view.findViewById(R.id.phaseDisTextViewItemOnlineRead);

            numberTextViewHolder = (TextView) view.findViewById(R.id.numberTextViewItemOnlineRead);
            posTextViewHolder = (TextView) view.findViewById(R.id.posTextViewItemOnlineRead);
            directTextViewHolder = (TextView) view.findViewById(R.id.directTextViewItemOnlineRead);
            reverseTextViewHolder = (TextView) view.findViewById(R.id.reverseTextViewItemOnlineRead);

            phase1TextViewHolder = (TextView) view.findViewById(R.id.phase1TextViewItemOnlineRead);
            phase2TextViewHolder = (TextView) view.findViewById(R.id.phase2TextViewItemOnlineRead);
            phase3TextViewHolder = (TextView) view.findViewById(R.id.phase3TextViewItemOnlineRead);

            pos1TextViewHolder = (TextView) view.findViewById(R.id.pos1TextViewItemOnlineRead);
            pos2TextViewHolder = (TextView) view.findViewById(R.id.pos2TextViewItemOnlineRead);
            pos3TextViewHolder = (TextView) view.findViewById(R.id.pos3TextViewItemOnlineRead);

            layout1TextViewHolder = (LinearLayout) view.findViewById(R.id.layout1LinearLayoutItemOnlineRead);
            layout2TextViewHolder = (LinearLayout) view.findViewById(R.id.layout2LinearLayoutItemOnlineRead);
            layout3TextViewHolder = (LinearLayout) view.findViewById(R.id.layout3LinearLayoutItemOnlineRead);

            relay1ImageViewHolder = (ImageView) view.findViewById(R.id.relay1ImageViewItemOnlineRead);
            relay2ImageViewHolder = (ImageView) view.findViewById(R.id.relay2ImageViewItemOnlineRead);
            relay3ImageViewHolder = (ImageView) view.findViewById(R.id.relay3ImageViewItemOnlineRead);


            layoutCheck1TextViewHolder = (LinearLayout) view.findViewById(R.id.layoutCheck1LinearLayoutItemOnlineRead);
            layoutCheck2TextViewHolder = (LinearLayout) view.findViewById(R.id.layoutCheck2LinearLayoutItemOnlineRead);
            layoutCheck3TextViewHolder = (LinearLayout) view.findViewById(R.id.layoutCheck3LinearLayoutItemOnlineRead);

            check1ImageViewHolder = (ImageView) view.findViewById(R.id.check1ImageViewItemOnlineRead);
            check2ImageViewHolder = (ImageView) view.findViewById(R.id.check2ImageViewItemOnlineRead);
            check3ImageViewHolder = (ImageView) view.findViewById(R.id.check3ImageViewItemOnlineRead);
        }
    }

    public OnlineReadingRecyclerViewAdapter(AppCompatActivity context, List<OnlineReadItem> itensEnable) {
        this.itens = new ArrayList<OnlineReadItem>();
        int pos = 1;
        boolean add = true;
        while (pos < 13) {
            add = true;
            for(OnlineReadItem unt: itensEnable) {
                if(Integer.parseInt(unt.pos[0]) == pos) {
                    this.itens.add(unt);
                    add = false;
                }
                if (unt.pos[1] != null &&
                        Integer.parseInt(unt.pos[1]) == pos) {
                    add = false;
                }
                if(unt.pos[2] != null &&
                        Integer.parseInt(unt.pos[2]) == pos) {
                    add = false;
                }
            }
            if(add) {
                OnlineReadItem it = new OnlineReadItem();
                it.pos = new String[1];
                it.pos[0] = String.valueOf(pos);
                it.isEnable = false;
                this.itens.add(it);
            }
            pos++;
        }
        this.context = context;
        this.eletraMCIReadTitle = new EletraMCIReadTitle();

        phaseMap.put(1, "A");
        phaseMap.put(2, "B");
        phaseMap.put(3, "C");

        phaseMap.put(4, "A");
        phaseMap.put(5, "B");
        phaseMap.put(6, "C");

        phaseMap.put(7, "A");
        phaseMap.put(8, "B");
        phaseMap.put(9, "C");

        phaseMap.put(10, "A");
        phaseMap.put(11, "B");
        phaseMap.put(12, "C");
    }

    @Override
    public OnlineReadingRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                          int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_online_read, parent, false);
        OnlineReadingRecyclerViewAdapter.ViewHolder vh = new OnlineReadingRecyclerViewAdapter.ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(OnlineReadingRecyclerViewAdapter.ViewHolder holder, int position) {
        OnlineReadItem it = this.itens.get(position);
        if(it.isEnable) {
            holder.numberTextViewHolder.setText(it.number);
            holder.posTextViewHolder.setText(it.pos[0]);
            holder.directTextViewHolder.setText(it.direct);
            holder.reverseTextViewHolder.setText(it.reverse);

            holder.phase1TextViewHolder.setVisibility(View.GONE);
            holder.phase2TextViewHolder.setVisibility(View.GONE);
            holder.phase3TextViewHolder.setVisibility(View.GONE);

            holder.pos1TextViewHolder.setVisibility(View.GONE);
            holder.pos2TextViewHolder.setVisibility(View.GONE);
            holder.pos3TextViewHolder.setVisibility(View.GONE);

            holder.layout1TextViewHolder.setVisibility(View.GONE);
            holder.layout2TextViewHolder.setVisibility(View.GONE);
            holder.layout3TextViewHolder.setVisibility(View.GONE);

            holder.relay1ImageViewHolder.setVisibility(View.GONE);
            holder.relay2ImageViewHolder.setVisibility(View.GONE);
            holder.relay3ImageViewHolder.setVisibility(View.GONE);

            holder.layoutCheck1TextViewHolder.setVisibility(View.GONE);
            holder.layoutCheck2TextViewHolder.setVisibility(View.GONE);
            holder.layoutCheck3TextViewHolder.setVisibility(View.GONE);

            holder.check1ImageViewHolder.setVisibility(View.GONE);
            holder.check2ImageViewHolder.setVisibility(View.GONE);
            holder.check3ImageViewHolder.setVisibility(View.GONE);

            if (it.pos[0] != null) {
                holder.phase1TextViewHolder.setText(it.phase[0]);
                holder.pos1TextViewHolder.setText(it.pos[0]);
                holder.relay1ImageViewHolder.setImageResource(it.relay[0]);
                holder.check1ImageViewHolder.setImageResource(it.check[0]);

                holder.phase1TextViewHolder.setVisibility(View.VISIBLE);
                holder.pos1TextViewHolder.setVisibility(View.VISIBLE);
                holder.layout1TextViewHolder.setVisibility(View.VISIBLE);
                holder.relay1ImageViewHolder.setVisibility(View.VISIBLE);

                holder.layoutCheck1TextViewHolder.setVisibility(View.VISIBLE);
                holder.check1ImageViewHolder.setVisibility(View.VISIBLE);
                holder.imageViewViewHolder.setImageResource(R.drawable.singlephase);
            }

            if (it.pos[1] != null) {
                holder.phase2TextViewHolder.setText(it.phase[1]);
                holder.pos2TextViewHolder.setText(it.pos[1]);
                holder.relay2ImageViewHolder.setImageResource(it.relay[1]);
                holder.check2ImageViewHolder.setImageResource(it.check[1]);

                holder.phase2TextViewHolder.setVisibility(View.VISIBLE);
                holder.pos2TextViewHolder.setVisibility(View.VISIBLE);
                holder.layout2TextViewHolder.setVisibility(View.VISIBLE);
                holder.relay2ImageViewHolder.setVisibility(View.VISIBLE);

                holder.layoutCheck2TextViewHolder.setVisibility(View.VISIBLE);
                holder.check2ImageViewHolder.setVisibility(View.VISIBLE);
                holder.imageViewViewHolder.setImageResource(R.drawable.biphase);
            }

            if (it.pos[2] != null) {
                holder.phase3TextViewHolder.setText(it.phase[2]);
                holder.pos3TextViewHolder.setText(it.pos[2]);
                holder.relay3ImageViewHolder.setImageResource(it.relay[2]);
                holder.check3ImageViewHolder.setImageResource(it.check[2]);

                holder.phase3TextViewHolder.setVisibility(View.VISIBLE);
                holder.pos3TextViewHolder.setVisibility(View.VISIBLE);
                holder.layout3TextViewHolder.setVisibility(View.VISIBLE);
                holder.relay3ImageViewHolder.setVisibility(View.VISIBLE);

                holder.layoutCheck3TextViewHolder.setVisibility(View.VISIBLE);
                holder.check3ImageViewHolder.setVisibility(View.VISIBLE);
                holder.imageViewViewHolder.setImageResource(R.drawable.threephase);
            }
            holder.disableLinearLayoutHolder.setVisibility(View.GONE);
            holder.enableLinearLayoutHolder.setVisibility(View.VISIBLE);
        } else {
            holder.enableLinearLayoutHolder.setVisibility(View.GONE);
            holder.disableLinearLayoutHolder.setVisibility(View.VISIBLE);

            holder.disPosTextViewHolder.setText(context.getString(R.string.attr_meter_pos) + " " +  it.pos[0]);
            holder.disPhaseTextViewHolder.setText(context.getString(R.string.attr_phase) + " " + phaseMap.get(Integer.parseInt(it.pos[0])));
        }
    }

    @Override
    public int getItemCount() {
        return itens.size();
    }

    public static class OnlineReadItem {
        public String number;
        public String[] pos;
        public String direct;
        public String reverse;
        public String[] phase;
        public Integer[] relay;
        public Integer[] check;
        public boolean isOpen = false;
        public boolean isEnable = false;
    }
}