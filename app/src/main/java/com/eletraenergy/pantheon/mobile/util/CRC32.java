package com.eletraenergy.pantheon.mobile.util;

import java.nio.ByteBuffer;

public class CRC32 {
    private static final java.util.zip.CRC32 crc32 = new java.util.zip.CRC32();

    public CRC32() {
    }

    public static byte[] calc(byte[] data, int offset, int size) {
        crc32.reset();
        crc32.update(data, offset, size);
        return ByteBuffer.allocate(4).putInt((int)crc32.getValue()).array();
    }

    public static byte[] calc(byte[] data) {
        return calc(data, 0, data.length);
    }
}
