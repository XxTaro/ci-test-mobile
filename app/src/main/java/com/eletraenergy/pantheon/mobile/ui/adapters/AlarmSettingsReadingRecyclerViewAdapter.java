package com.eletraenergy.pantheon.mobile.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.eletraenergy.pantheon.mobile.R;

import java.util.List;

/**
 * Created by dcsda on 16/01/2018.
 */

public class AlarmSettingsReadingRecyclerViewAdapter extends RecyclerView.Adapter<AlarmSettingsReadingRecyclerViewAdapter.ViewHolder> {

    protected List< AlarmSettingsReadingItem> itens;
    protected AppCompatActivity context;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView titleTextViewHolder;
        public TextView valueTextViewHolder;

        public ViewHolder(View view) {
            super(view);
            titleTextViewHolder = (TextView) view.findViewById(android.R.id.text1);
            valueTextViewHolder = (TextView) view.findViewById(android.R.id.text2);
        }
    }

    public AlarmSettingsReadingRecyclerViewAdapter(AppCompatActivity context, List< AlarmSettingsReadingItem> itens) {
        this.itens = itens;
        this.context = context;
    }

    @Override
    public AlarmSettingsReadingRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                                 int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_alarm_settings_readings, parent, false);
        AlarmSettingsReadingRecyclerViewAdapter.ViewHolder vh = new AlarmSettingsReadingRecyclerViewAdapter.ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(AlarmSettingsReadingRecyclerViewAdapter.ViewHolder holder, int position) {
        AlarmSettingsReadingItem it = this.itens.get(position);
        holder.titleTextViewHolder.setText(it.title);
        holder.valueTextViewHolder.setText(it.value ? context.getResources().getText(R.string.enabled): context.getResources().getText(R.string.disabled));
        holder.valueTextViewHolder.setTextColor(it.value ? context.getResources().getColor(android.R.color.holo_green_dark) : context.getResources().getColor(android.R.color.holo_red_dark));
    }

    @Override
    public int getItemCount() {
        return itens.size();
    }

    public static class  AlarmSettingsReadingItem {
        public String title;
        public boolean value;
    }
}