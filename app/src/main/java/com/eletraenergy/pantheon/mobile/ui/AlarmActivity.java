package com.eletraenergy.pantheon.mobile.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.ui.adapters.AlarmReadingRecyclerViewAdapter;
import com.google.android.material.appbar.AppBarLayout;

import java.util.Arrays;

public class AlarmActivity extends PantheonCompatActivity {

    protected Toolbar toolbar;
    protected TextView titleTextView;
    protected TextView dateTextView;
    protected TextView userTextView;
    protected CardView consumersUnitsCardView;
    protected CardView configurationCardView;

    protected ImageView[] statusImageView = {null, null, null, null, null, null, null, null, null, null, null, null};

    protected TextView[] statusTextView = {null, null, null, null, null, null, null, null, null, null, null, null};

    protected ImageView[] alarmImageView = {null, null, null, null};

    protected TextView[] alarmTextView = {null, null, null, null};


    private AlarmReadingRecyclerViewAdapter.AlarmReadingItem item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        item = (AlarmReadingRecyclerViewAdapter.AlarmReadingItem) getIntent().getExtras().get("ITEM");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);

        titleTextView = (TextView) findViewById(R.id.titleTextViewAlarm);
        dateTextView = (TextView) findViewById(R.id.dateTextViewAlarm);
        userTextView = (TextView) findViewById(R.id.userTextViewAlarm);
        consumersUnitsCardView = (CardView) findViewById(R.id.consumersUnitsCardViewAlarm);
        configurationCardView = (CardView) findViewById(R.id.configurationCardViewAlarm);

        statusImageView[0] = (ImageView) findViewById(R.id.status1ImageView);
        statusImageView[1] = (ImageView) findViewById(R.id.status2ImageView);
        statusImageView[2] = (ImageView) findViewById(R.id.status3ImageView);
        statusImageView[3] = (ImageView) findViewById(R.id.status4ImageView);
        statusImageView[4] = (ImageView) findViewById(R.id.status5ImageView);
        statusImageView[5] = (ImageView) findViewById(R.id.status6ImageView);
        statusImageView[6] = (ImageView) findViewById(R.id.status7ImageView);
        statusImageView[7] = (ImageView) findViewById(R.id.status8ImageView);
        statusImageView[8] = (ImageView) findViewById(R.id.status9ImageView);
        statusImageView[9] = (ImageView) findViewById(R.id.status10ImageView);
        statusImageView[10] = (ImageView) findViewById(R.id.status11ImageView);
        statusImageView[11] = (ImageView) findViewById(R.id.status12ImageView);

        statusTextView[0] = (TextView) findViewById(R.id.status1TextView);
        statusTextView[1] = (TextView) findViewById(R.id.status2TextView);
        statusTextView[2] = (TextView) findViewById(R.id.status3TextView);
        statusTextView[3] = (TextView) findViewById(R.id.status4TextView);
        statusTextView[4] = (TextView) findViewById(R.id.status5TextView);
        statusTextView[5] = (TextView) findViewById(R.id.status6TextView);
        statusTextView[6] = (TextView) findViewById(R.id.status7TextView);
        statusTextView[7] = (TextView) findViewById(R.id.status8TextView);
        statusTextView[8] = (TextView) findViewById(R.id.status9TextView);
        statusTextView[9] = (TextView) findViewById(R.id.status10TextView);
        statusTextView[10] = (TextView) findViewById(R.id.status11TextView);
        statusTextView[11] = (TextView) findViewById(R.id.status12TextView);

        alarmImageView[0] = (ImageView) findViewById(R.id.alarm1ImageView);
        alarmImageView[1] = (ImageView) findViewById(R.id.alarm2ImageView);
        alarmImageView[2] = (ImageView) findViewById(R.id.alarm3ImageView);
        alarmImageView[3] = (ImageView) findViewById(R.id.alarm4ImageView);

        alarmTextView[0] = (TextView) findViewById(R.id.alarm1TextView);
        alarmTextView[1] = (TextView) findViewById(R.id.alarm2TextView);
        alarmTextView[2] = (TextView) findViewById(R.id.alarm3TextView);
        alarmTextView[3] = (TextView) findViewById(R.id.alarm4TextView);

        titleTextView.setText(item.title);
        dateTextView.setText(item.dateTime);
        userTextView.setText(item.user);

        if(item.type == 0x3C || item.type == 0x3E) {
            consumersUnitsCardView.setVisibility(View.VISIBLE);

            byte[] causeDisStates = Arrays.copyOfRange(item.rawData, 0, 5);
            byte[] valueDisStates = Arrays.copyOfRange(item.rawData, 5, 14);

            for(int pos = 0; pos < 12; pos++) {
                if(getBit(causeDisStates, pos) == 1) {

                    int value = (getBit(valueDisStates, (pos * 2) + 1) << 1) + getBit(valueDisStates, (pos * 2));
                    if (item.type == 0x3C) {
                        switch (value) {
                            case 0:
                                statusTextView[pos].setText("Cortou, mas já estava sem tensão");
                                statusImageView[pos].setColorFilter(ContextCompat.getColor(this, android.R.color.holo_red_dark));
                                break;
                            case 1:
                                statusTextView[pos].setText("Cortou corretamente");
                                statusImageView[pos].setColorFilter(ContextCompat.getColor(this, android.R.color.holo_green_dark));
                                break;
                            case 2:
                                statusTextView[pos].setText("Cortou, mas continua com tensão");
                                statusImageView[pos].setColorFilter(ContextCompat.getColor(this, android.R.color.holo_red_dark));
                                break;
                        }
                    }
                    if (item.type == 0x3E) {
                        switch (value) {
                            case 0:
                                statusTextView[pos].setText("Ligou, mas já estava com tensão");
                                statusImageView[pos].setColorFilter(ContextCompat.getColor(this, android.R.color.holo_green_dark));
                                break;
                            case 1:
                                statusTextView[pos].setText("Ligou corretamente");
                                statusImageView[pos].setColorFilter(ContextCompat.getColor(this, android.R.color.holo_green_dark));
                                break;
                            case 2:
                                statusTextView[pos].setText("Ligou, mas continua sem tensão");
                                statusImageView[pos].setColorFilter(ContextCompat.getColor(this, android.R.color.holo_red_dark));
                                break;
                            case 3:
                                statusTextView[pos].setText("Não Ligou, pois já estava com tensão");
                                statusImageView[pos].setColorFilter(ContextCompat.getColor(this, android.R.color.holo_red_dark));
                                break;
                        }
                    }
                } else {
                    statusImageView[pos].setColorFilter(ContextCompat.getColor(this, android.R.color.darker_gray));
                    statusTextView[pos].setText("Não participou do alarme");
                }
            }
        } else {
            consumersUnitsCardView.setVisibility(View.GONE);
        }

        if(item.type == 0x40) {
            configurationCardView.setVisibility(View.VISIBLE);

            boolean doorOpenEnable = (item.rawData[0] & 0x01) == 1;
            boolean relayCollectiveOpenEnable = (item.rawData[0] & 0x02) == 2;
            boolean relayCollectiveExecEnable = (item.rawData[0] & 0x04) == 4;
            boolean hhuWriteEnableEnable = (item.rawData[0] & 0x08) == 8;

            boolean doorOpenEnableValue = (item.rawData[1] & 0x01) == 1;
            boolean relayCollectiveOpenEnableValue = (item.rawData[1] & 0x02) == 2;
            boolean relayCollectiveExecEnableValue = (item.rawData[1] & 0x04) == 4;
            boolean hhuWriteEnableEnableValue = (item.rawData[1] & 0x08) == 8;

            if(doorOpenEnable) {
                if(doorOpenEnableValue) {
                    alarmImageView[0].setColorFilter(ContextCompat.getColor(this, android.R.color.holo_green_dark));
                    alarmTextView[0].setText("Habilitado");
                } else {
                    alarmImageView[0].setColorFilter(ContextCompat.getColor(this, android.R.color.holo_red_dark));
                    alarmTextView[0].setText("Desabilitado");
                }
            } else {
                alarmImageView[0].setColorFilter(ContextCompat.getColor(this, android.R.color.darker_gray));
                alarmTextView[0].setText("Não foi alterado");
            }

            if(relayCollectiveOpenEnable) {
                if(relayCollectiveOpenEnableValue) {
                    alarmTextView[1].setText("Habilitado");
                    alarmImageView[1].setColorFilter(ContextCompat.getColor(this, android.R.color.holo_green_dark));
                } else {
                    alarmTextView[1].setText("Desabilitado");
                    alarmImageView[1].setColorFilter(ContextCompat.getColor(this, android.R.color.holo_red_dark));
                }

            } else {
                alarmImageView[1].setColorFilter(ContextCompat.getColor(this, android.R.color.darker_gray));
                alarmTextView[1].setText("Não foi alterado");
            }

            if(relayCollectiveExecEnable) {
                if(relayCollectiveExecEnableValue) {
                    alarmTextView[2].setText("Habilitado");
                    alarmImageView[2].setColorFilter(ContextCompat.getColor(this, android.R.color.holo_green_dark));
                } else {
                    alarmTextView[2].setText("Desabilitado");
                    alarmImageView[2].setColorFilter(ContextCompat.getColor(this, android.R.color.holo_red_dark));
                }
            } else {
                alarmImageView[2].setColorFilter(ContextCompat.getColor(this, android.R.color.darker_gray));
                alarmTextView[2].setText("Não foi alterado");
            }

            if(hhuWriteEnableEnable) {
                if(hhuWriteEnableEnableValue) {
                    alarmTextView[3].setText("Habilitado");
                    alarmImageView[3].setColorFilter(ContextCompat.getColor(this, android.R.color.holo_green_dark));
                } else {
                    alarmTextView[3].setText("Desabilitado");
                    alarmImageView[3].setColorFilter(ContextCompat.getColor(this, android.R.color.holo_red_dark));
                }

            } else {
                alarmImageView[3].setColorFilter(ContextCompat.getColor(this, android.R.color.darker_gray));
                alarmTextView[3].setText("Não foi alterado");
            }
        } else {
            configurationCardView.setVisibility(View.GONE);
        }

    }

    private int getBit(byte[] bData, int i) {
        int bitPos = ((int) i % 8);
        int bytePos = ((int) i / 8);
        return (bData[(bData.length-1) - bytePos] >> bitPos) & ((byte) 0x01);
    }

    @Override
    public boolean onSupportNavigateUp(){
        this.onBackPressed();
        return true;
    }
}
