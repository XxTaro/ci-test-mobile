package com.eletraenergy.pantheon.mobile.ui.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.ui.adapters.ReadingsRecyclerViewAdapter;

import java.util.ArrayList;


public class ReadingsFragment extends Fragment {

    protected RecyclerView recyclerView;

    public ReadingsFragment() {
        // Required empty public constructor
    }
    public static ReadingsFragment newInstance() {
        ReadingsFragment fragment = new ReadingsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_readings, container, false);
        recyclerView = view.findViewById(R.id.recyclerViewReadings);

        ArrayList<ReadingsRecyclerViewAdapter.ItemMain> itens = new ArrayList<>();
        ReadingsRecyclerViewAdapter.ItemMain item = new ReadingsRecyclerViewAdapter.ItemMain();
        item.text = getResources().getString(R.string.cs_informations);
        item.image = getResources().getDrawable(R.drawable.ic_cpu_information);
        itens.add(item);
        item = new ReadingsRecyclerViewAdapter.ItemMain();
        item.text = getResources().getString(R.string.consumers_units);
        item.image = getResources().getDrawable(R.drawable.ic_consumer_units);
        itens.add(item);

        /*
        item = new ReadingsRecyclerViewAdapter.ItemMain();
        item.text = getResources().getString(R.string.online_read);
        item.image = getResources().getDrawable(R.drawable.ic_online_read);
        itens.add(item);

        item = new ReadingsRecyclerViewAdapter.ItemMain();
        item.text = getResources().getString(R.string.checkpoint_read);
        item.image = getResources().getDrawable(R.drawable.ic_checkpoint_read);
        itens.add(item);
        */

        item = new ReadingsRecyclerViewAdapter.ItemMain();
        item.text = getResources().getString(R.string.alarm_settings_read);
        item.image = getResources().getDrawable(R.drawable.ic_alarm_settings_read);
        itens.add(item);

        item = new ReadingsRecyclerViewAdapter.ItemMain();
        item.text = getResources().getString(R.string.alarm_readings);
        item.image = getResources().getDrawable(R.drawable.baseline_error_outline_black_24);
        itens.add(item);

        ReadingsRecyclerViewAdapter adapter = new ReadingsRecyclerViewAdapter((AppCompatActivity) this.getActivity(), itens);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(adapter);

        return view;
    }

}
