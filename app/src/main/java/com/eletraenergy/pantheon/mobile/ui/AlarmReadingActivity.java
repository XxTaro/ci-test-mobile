package com.eletraenergy.pantheon.mobile.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadAsyncTask;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadEnum;
import com.eletraenergy.pantheon.mobile.ui.adapters.AlarmReadingRecyclerViewAdapter;
import com.google.android.material.appbar.AppBarLayout;
import com.google.common.base.Strings;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.eletra.mci.app.MeterComm;
import br.com.eletra.mci.app.MeterSchema;
import br.com.eletra.mci.dlms.DLMSAtt;
import br.com.eletra.mci.dlms.DLMSCommand;
import br.com.eletra.mci.util.HexString;

public class AlarmReadingActivity extends PantheonCompatActivity {

    protected Toolbar toolbar;
    protected EletraMCIReadAsyncTask eletraMCIReadAsyncTask;
    protected RelativeLayout relativeLayoutError;
    protected LinearLayout linearLayout;
    protected TextView textView;
    protected Button button;
    protected Button readButton;
    protected AlertDialog progressWrite;

    protected EditText editText;
    protected RecyclerView recyclerView;
    protected RelativeLayout relativeLayoutLoad;
    protected RelativeLayout relativeLayoutInit;
    protected AppCompatCheckBox checkBox;

    //private final byte[] DEFAULT_SIMPLE_INDEX_FILTER_DATA = HexString.hexStringToByteArray("02 02 04 06 00 00 00 01 06 00 00 00 01 12 00 00 12 00 00");
    private final byte[] DEFAULT_SIMPLE_INDEX_FILTER_DATA = HexString.hexStringToByteArray("02 02 04 06 00 00 00 01 06 00 00 00 00 12 00 00 12 00 00");

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_reading);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);

        linearLayout = (LinearLayout) findViewById(R.id.linearLayoutAlarmReading);
        relativeLayoutError = (RelativeLayout) findViewById(R.id.relativeLayoutErrorAlarmReading);
        textView = (TextView) findViewById(R.id.textViewAlarmReading);
        readButton = (Button) findViewById(R.id.readButton);
        button = (Button) findViewById(R.id.buttonAlarmReading);

        editText = (EditText) findViewById(R.id.editTextAlarmReading);
        editText.setEnabled(false);
        checkBox = (AppCompatCheckBox) findViewById(R.id.checkBoxAlarmReading);
        checkBox.setChecked(true);
        checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked) {
                editText.setText("");
                editText.setEnabled(false);
                linearLayout.requestFocus();
            } else {
                editText.setEnabled(true);
                editText.requestFocus();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewAlarmReading);
        LinearLayoutManager layoutManager = new LinearLayoutManager(AlarmReadingActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        relativeLayoutLoad = (RelativeLayout) findViewById(R.id.relativeLayoutLoadAlarmReading);
        relativeLayoutInit = (RelativeLayout) findViewById(R.id.relativeLayoutInitAlarmReading);
    }

    protected void showButtonErro(int err) {
        if (err == R.string.error_level) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmReadingActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if (err == R.string.error_auth) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmReadingActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if (err == R.string.error_encrypt) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmReadingActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if (err == R.string.error_security) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmReadingActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if (err == R.string.error_ip) {
            button.setText(R.string.bt_communication_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmReadingActivity.this, CommunicationSettingActivity.class));
            });
            return;
        }
        if (err == R.string.error_port) {
            button.setText(R.string.bt_communication_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmReadingActivity.this, CommunicationSettingActivity.class));
            });
            return;
        }
        button.setText(R.string.bt_retry);
        button.setVisibility(View.VISIBLE);
        button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_refresh_black_24dp), null, null, null);
        button.setOnClickListener(v -> {
            relativeLayoutError.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            relativeLayoutInit.setVisibility(View.GONE);
            relativeLayoutLoad.setVisibility(View.VISIBLE);
            linearLayout.setVisibility(View.VISIBLE);
            editText.setEnabled(false);
            readButton.setEnabled(false);
            this.sendAlarmReadingCommand(null);
        });
        return;
    }

    @Override
    public boolean onSupportNavigateUp() {
        this.onBackPressed();
        return true;
    }

    @Override
    public void onDestroy() {
        if (eletraMCIReadAsyncTask != null) eletraMCIReadAsyncTask.cancel(true);
        super.onDestroy();
    }

    private boolean isValid() {
        boolean res = true;
        if(!checkBox.isChecked()) {
            if (Strings.isNullOrEmpty(editText.getText().toString()) ||
                    Integer.parseInt(editText.getText().toString()) < 1) {
                Toast.makeText(this, R.string.valid_number_records, Toast.LENGTH_SHORT).show();
                res = false;
                return res;
            }
        }
        return res;
    }

    public void sendAlarmReadingCommand(View view) {

        if(isValid()) {

            if (eletraMCIReadAsyncTask != null) eletraMCIReadAsyncTask.cancel(true);
            MeterSchema meterSchema = MeterSchema.PANTHEON_V1;
            MeterComm meterComm = new MeterComm();
            meterComm.loadSchema(meterSchema, "DLMSTCP");
            DLMSCommand dlmsCommand = (DLMSCommand) meterComm.getAvailableCommands().get(EletraMCIReadEnum.ALARM_READ.getReadingCommand());

            if(!checkBox.isChecked()) {
                String number = editText.getText().toString();
                byte[] finalData = DEFAULT_SIMPLE_INDEX_FILTER_DATA;
                int iAttData = Integer.parseInt(number);
                byte[] bAttData = ByteBuffer.allocate(4).putInt(iAttData).array();
                int offset = 9;
                for (int i = 0; i < 4; i++) {
                    finalData[offset + i] = bAttData[i];
                }
                dlmsCommand.getAttById(EletraMCIReadEnum.ALARMREAD.getReadingCommand()).setValueProperty(HexString.bytesToHexString(finalData));
            }

            this.eletraMCIReadAsyncTask = new EletraMCIReadAsyncTask(AlarmReadingActivity.this,
                    dlmsCommand,
                    (atts, error) -> {
                        if (error > 0) {
                            textView.setText(error);
                            if (error == R.string.exception_generic) {
                                textView.setText(R.string.exception_generic_alarm_reading);
                            }
                            this.showButtonErro(this.isHightLevel());
                            readButton.setEnabled(false);
                            editText.setEnabled(false);
                            checkBox.setEnabled(false);
                            relativeLayoutLoad.setVisibility(View.GONE);
                            linearLayout.setVisibility(View.GONE);
                            relativeLayoutError.setVisibility(View.VISIBLE);
                        } else {
                            this.postRead(atts);
                        }
                    },
                    task -> {
                        if(this.isHightLevel() > 0) {
                            this.preRead(task);
                        } else {
                            recyclerView.setVisibility(View.GONE);
                            relativeLayoutInit.setVisibility(View.GONE);
                            relativeLayoutLoad.setVisibility(View.VISIBLE);
                            editText.setEnabled(false);
                            checkBox.setEnabled(false);
                            readButton.setEnabled(false);
                        }
                    }
            );
            this.eletraMCIReadAsyncTask.execute();
        }
    }

    protected void preRead(AsyncTask task) {
        task.cancel(true);
        textView.setText(this.isHightLevel());
        showButtonErro(this.isHightLevel());
        linearLayout.setVisibility(View.GONE);
        relativeLayoutError.setVisibility(View.VISIBLE);
    }

    protected void postRead(List<DLMSAtt> attributes) {

        Log.i("TAMANHO", " -- " +attributes.size());

        ArrayList<AlarmReadingRecyclerViewAdapter.AlarmReadingItem> itens = new ArrayList<AlarmReadingRecyclerViewAdapter.AlarmReadingItem>();
        AlarmReadingRecyclerViewAdapter.AlarmReadingItem it;

        if(attributes.get(0).getValueProperty() == null) {

            textView.setText(R.string.alarm_empty);
            showButtonErro(1);
            readButton.setEnabled(false);
            editText.setEnabled(false);
            checkBox.setEnabled(false);
            relativeLayoutLoad.setVisibility(View.GONE);
            linearLayout.setVisibility(View.GONE);
            relativeLayoutError.setVisibility(View.VISIBLE);

        } else {

            int index = 0;

            for (DLMSAtt attribute : attributes) {

                Log.i("TAMANHO", " ---- " + attribute.getValueProperty());

                byte[] strData = HexString.hexStringToByteArray(attribute.getValueProperty());
                byte[] userIdArray = Arrays.copyOfRange(strData, 2, 6);
                String userId = HexString.bytesToHexString(userIdArray);
                String dateTime = String.format("%02x/%02x/%02x %02x:%02x:%02x",
                        strData[8], strData[7], strData[6], strData[9], strData[10], strData[11]);
                int alarmCode = strData[12] & 0xFF;
                byte[] alarmData = Arrays.copyOfRange(strData, 13, strData.length);

                int title = getTitleAlarm(alarmCode);

                it = new AlarmReadingRecyclerViewAdapter.AlarmReadingItem();
                it.index = ++index;
                it.title = title;
                it.dateTime = dateTime;
                it.user = userId;
                it.type = alarmCode;
                it.rawData = alarmData;
                itens.add(it);
            }

            AlarmReadingRecyclerViewAdapter adapter = new AlarmReadingRecyclerViewAdapter((AppCompatActivity) AlarmReadingActivity.this, itens);
            recyclerView.setAdapter(adapter);

            relativeLayoutError.setVisibility(View.GONE);
            relativeLayoutLoad.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            linearLayout.setVisibility(View.VISIBLE);
            checkBox.setEnabled(true);
            if(checkBox.isChecked()) {
                editText.setEnabled(false);
            } else{
                editText.setEnabled(true);
            }
            checkBox.setEnabled(true);
            readButton.setEnabled(true);
        }
    }

    private int getTitleAlarm(int value) {
        int state = R.string.alarm_unknow;
        switch (value) {
            case 0x3C:
                state = R.string.alarm_disconnection;
                break;
            case 0x3E:
                state = R.string.alarm_connection;
                break;
            case 0x40:
                state = R.string.alarm_confguration;
                break;
            case 0x81:
                state = R.string.alarm_dor_open;
                break;
        }
        return state;
    }
}