package com.eletraenergy.pantheon.mobile.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIExecAsyncTask;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIExecEnum;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadAsyncTask;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadEnum;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadTitle;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIWriteEnum;
import com.eletraenergy.pantheon.mobile.ui.adapters.RelaysControlsRecyclerViewAdapter;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.common.base.Strings;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.eletra.mci.app.MeterComm;
import br.com.eletra.mci.app.MeterSchema;
import br.com.eletra.mci.dlms.DLMSAtt;
import br.com.eletra.mci.dlms.DLMSCommand;

public class RelaysControlsActivity extends PantheonCompatActivity {

    protected Toolbar toolbar;
    protected EletraMCIReadAsyncTask eletraMCIReadAsyncTask;
    protected EletraMCIReadAsyncTask eletraMCIReadAsyncTask2;
    protected EletraMCIExecAsyncTask eletraMCIExecAsyncTask;
    protected SwipeRefreshLayout swipeRefreshLayout;
    protected RecyclerView recyclerView;
    protected RelativeLayout relativeLayoutLoad;
    protected ProgressBar progressBar;
    protected Button openButton;
    protected Button closeButton;
    protected Button errorButton;
    protected RelativeLayout relativeLayoutError;
    protected LinearLayout timerCountLayout;
    protected TextView textCountDown;
    protected TextView waitTimeMessage;
    protected TextView textView;
    protected EletraMCIReadTitle eletraMCIReadTitle;
    protected ArrayList<String> units;
    protected AlertDialog progressWrite;
    protected RelaysControlsRecyclerViewAdapter adapter;

    private boolean isEnableHHU;

    protected void availableTransfer(){
        if(units.size() > 0) {
            openButton.setEnabled(true);
            closeButton.setEnabled(true);
        } else {
            openButton.setEnabled(false);
            closeButton.setEnabled(false);
        }
    }

    public void addUnit(String unit) {
        this.units.add(unit);
        this.availableTransfer();
    }

    public void rmUnit(String unit) {
        this.units.remove(unit);
        this.availableTransfer();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relays_controls);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        units = new ArrayList<String>();

        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);

        eletraMCIReadTitle = new EletraMCIReadTitle();

        progressBar = (ProgressBar) findViewById(R.id.progressBarRelayControl);
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(RelaysControlsActivity.this);
        circularProgressDrawable.setStyle(CircularProgressDrawable.LARGE);
        circularProgressDrawable.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        progressBar.setIndeterminateDrawable(circularProgressDrawable);

        relativeLayoutLoad = (RelativeLayout) findViewById(R.id.relativeLayoutLoadRelayControl);
        relativeLayoutError = (RelativeLayout) findViewById(R.id.relativeLayoutErrorRelayControl);
        timerCountLayout = (LinearLayout) findViewById(R.id.timerCountLayout);
        textCountDown = (TextView) findViewById(R.id.relaysControlTextCountDown);
        waitTimeMessage = (TextView) findViewById(R.id.waitTimeMessage);
        textView = (TextView) findViewById(R.id.textViewRelayControl);
        errorButton = (Button) findViewById(R.id.errorButtonRelayControl);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshRelayControl);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        openButton = (Button) findViewById(R.id.openButtonRelayControl);
        closeButton = (Button) findViewById(R.id.closeButtonRelayControl);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewRelayControl);
        LinearLayoutManager layoutManager = new LinearLayoutManager(RelaysControlsActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        swipeRefreshLayout.setOnRefreshListener(() -> RelaysControlsActivity.this.loadRelaysControl());
    }

    @Override
    protected void onResume() {
        super.onResume();
        relativeLayoutError.setVisibility(View.GONE);
        swipeRefreshLayout.setVisibility(View.GONE);
        timerCountLayout.setVisibility(View.GONE);
        relativeLayoutLoad.setVisibility(View.VISIBLE);
        this.loadRelaysControl();
    }

    protected void loadRelaysControl() {
        if (eletraMCIReadAsyncTask != null) eletraMCIReadAsyncTask.cancel(true);

        this.eletraMCIReadAsyncTask = new EletraMCIReadAsyncTask(
                RelaysControlsActivity.this,
                EletraMCIReadEnum.CONSUMER_UNIT,
                (attrs, error)-> {
                    if(error > 0) {
                        textView.setText(error);
                        if(error == R.string.exception_generic) {
                            textView.setText(R.string.exception_generic_units);
                        }
                        this.showButtonErro(this.isHightLevel());
                        relativeLayoutLoad.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        swipeRefreshLayout.setVisibility(View.GONE);
                        openButton.setEnabled(false);
                        closeButton.setEnabled(false);
                        relativeLayoutError.setVisibility(View.VISIBLE);
                    } else {
                        this.postRead(attrs);
                    }
                },
                task -> {
                    if(this.isHightLevel() > 0) {
                        this.preRead(task);
                    } else {
                        openButton.setEnabled(false);
                        closeButton.setEnabled(false);
                    }
                }
        );

        this.eletraMCIReadAsyncTask.execute();
    }

    public void open(View view) {
        if(units.size() > 0) {
            this.showOpenTransferDialog();
        } else {
            this.showNotTransferDialog();
        }
    }

    public void close(View view) {
        if(units.size() > 0) {
            this.showCloseTransferDialog();
        } else {
            this.showNotTransferDialog();
        }
    }

    private void showNotTransferDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(RelaysControlsActivity.this);
        builder.setTitle(getString(R.string.warning));
        builder.setMessage(R.string.app_not_transfer);
        builder.setIcon(R.drawable.eletra_icon);
        builder.setPositiveButton(getString(R.string.close),
                (dialog, which) -> {
                    dialog.dismiss();
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showOpenTransferDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(RelaysControlsActivity.this);
        builder.setTitle(getString(R.string.warning));
        builder.setMessage(R.string.app_transfer_open);
        builder.setIcon(R.drawable.eletra_icon);
        builder.setPositiveButton(getString(R.string.yes),
                (dialog, which) -> {
                    dialog.dismiss();
                    if(units.size() > 0) {
                        this.sendOpenRelaysCommand();
                    } else {
                        this.showNotTransferDialog();
                    }
                });
        builder.setNegativeButton(getString(R.string.no),
                (dialog, which) -> dialog.dismiss());
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showCloseTransferDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(RelaysControlsActivity.this);
        builder.setTitle(getString(R.string.warning));
        builder.setMessage(R.string.app_transfer_close);
        builder.setIcon(R.drawable.eletra_icon);
        builder.setPositiveButton(getString(R.string.yes),
                (dialog, which) -> {
                    dialog.dismiss();
                    if(units.size() > 0) {
                        this.sendCloseRelaysCommand();
                    } else {
                        this.showNotTransferDialog();
                    }
                });
        builder.setNegativeButton(getString(R.string.no),
                (dialog, which) -> dialog.dismiss());
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void sendOpenRelaysCommand() {
        if (eletraMCIReadAsyncTask != null) eletraMCIReadAsyncTask.cancel(true);
        if (eletraMCIReadAsyncTask2 != null) eletraMCIReadAsyncTask2.cancel(true);

        this.eletraMCIReadAsyncTask2 = new EletraMCIReadAsyncTask(
                RelaysControlsActivity.this,
                EletraMCIReadEnum.ALARM_CONFIGURATION,
                (attrs, error)-> {
                    int alarmHHUConfig = Integer.parseInt(attrs.get(1).getValueProperty());
                    boolean hhuWriteAlarm = (alarmHHUConfig & 0x08) == 0x08;
                    this.isEnableHHU = hhuWriteAlarm;
                },
                task -> {
                    if(this.isHightLevel() > 0) {
                        this.preRead(task);
                    } else {
                        openButton.setEnabled(false);
                        closeButton.setEnabled(false);
                    }
                }
        );
        this.eletraMCIReadAsyncTask2.execute();

        MeterSchema meterSchema = MeterSchema.PANTHEON_V1;
        MeterComm meterComm = new MeterComm();
        meterComm.loadSchema(meterSchema, "DLMSTCP");
        DLMSCommand dlmsCommand = (DLMSCommand) meterComm.getAvailableCommands().get(EletraMCIExecEnum.RELAY_OPERATION_ACTION.getWriteCommand());

        HashMap<EletraMCIWriteEnum, String> values = new HashMap<EletraMCIWriteEnum, String>();
        String attr = "02 03 11 00 01 0" + Integer.toString(units.size(), 16) + buildArrayOfMeters(units) + " 11 01";

        values.put(EletraMCIWriteEnum.ACTION_RELAY, attr);

        for (Map.Entry<EletraMCIWriteEnum, String> entry : values.entrySet()) {
            dlmsCommand.getAttById(entry.getKey().getWriteCommand()).setValueProperty(entry.getValue());
        }

        this.eletraMCIExecAsyncTask = new EletraMCIExecAsyncTask(RelaysControlsActivity.this,
                dlmsCommand,
                (error)-> {
                    if(error > 0) {
                        textView.setText(error);
                        if(error == R.string.exception_generic) {
                            textView.setText(R.string.exception_generic_exec_relays);
                        }
                        this.showButtonErro(this.isHightLevel());
                        relativeLayoutLoad.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        swipeRefreshLayout.setVisibility(View.GONE);
                        openButton.setEnabled(false);
                        closeButton.setEnabled(false);
                        relativeLayoutError.setVisibility(View.VISIBLE);
                        if(this.progressWrite != null) this.progressWrite.dismiss();
                    } else {
                        if(this.progressWrite != null) this.progressWrite.dismiss();
                        units = new ArrayList<String>();
                        openButton.setEnabled(false);
                        closeButton.setEnabled(false);

                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                        int waitTime = Integer.parseInt(prefs.getString(getString(R.string.key_wait_time), "20"));

                        if(isEnableHHU) {
                            Snackbar.make(toolbar, getString(R.string.success_action), Snackbar.LENGTH_LONG).show();

                            swipeRefreshLayout.setVisibility(View.GONE);
                            if(waitTime != 0) timerCountLayout.setVisibility(View.VISIBLE);
                            relativeLayoutLoad.setVisibility(View.VISIBLE);
                            waitTimeMessage.setText(R.string.wait_time_message);

                            new CountDownTimer(waitTime*1000, 1000) {

                                public void onTick(long millisUntilFinished) {
                                    textCountDown.setText("00:" + (millisUntilFinished / 1000 < 10 ? "0"+(millisUntilFinished / 1000) : millisUntilFinished / 1000));
                                }

                                public void onFinish() {
                                    textCountDown.setText("00:00");
                                    waitTimeMessage.setText(R.string.loading_status);
                                    loadRelaysControl();
                                }
                            }.start();
                        }
                        else {
                            textView.setText(R.string.exception_hhu_disable);
                            this.showButtonErro(this.isHightLevel());
                            relativeLayoutLoad.setVisibility(View.GONE);
                            swipeRefreshLayout.setRefreshing(false);
                            swipeRefreshLayout.setVisibility(View.GONE);
                            openButton.setEnabled(false);
                            closeButton.setEnabled(false);
                            relativeLayoutError.setVisibility(View.VISIBLE);
                            if(this.progressWrite != null) this.progressWrite.dismiss();
                        }

                    }
                },
                task -> {
                    if(this.isHightLevel() > 0) {
                        this.preRead(task);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(RelaysControlsActivity.this);
                        LayoutInflater inflater = LayoutInflater.from(RelaysControlsActivity.this);
                        View view = inflater.inflate(R.layout.dialog_progress, null, false);
                        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBarProgress);
                        TextView textView = (TextView) view.findViewById(R.id.textViewProgress);
                        textView.setText(R.string.processing_open_relays);
                        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(RelaysControlsActivity.this);
                        circularProgressDrawable.setStyle(CircularProgressDrawable.LARGE);
                        circularProgressDrawable.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
                        progressBar.setIndeterminateDrawable(circularProgressDrawable);
                        builder.setView(view);
                        builder.setCancelable(false);
                        this.progressWrite = builder.create();
                        this.progressWrite.show();
                    }
                }
        );
        this.eletraMCIExecAsyncTask.execute();
    }

    private void sendCloseRelaysCommand() {
        if (eletraMCIReadAsyncTask != null) eletraMCIReadAsyncTask.cancel(true);
        if (eletraMCIReadAsyncTask2 != null) eletraMCIReadAsyncTask2.cancel(true);

        this.eletraMCIReadAsyncTask2 = new EletraMCIReadAsyncTask(
                RelaysControlsActivity.this,
                EletraMCIReadEnum.ALARM_CONFIGURATION,
                (attrs, error)-> {
                    int alarmHHUConfig = Integer.parseInt(attrs.get(1).getValueProperty());
                    boolean hhuWriteAlarm = (alarmHHUConfig & 0x08) == 0x08;
                    this.isEnableHHU = hhuWriteAlarm;
                },
                task -> {
                    if(this.isHightLevel() > 0) {
                        this.preRead(task);
                    } else {
                        openButton.setEnabled(false);
                        closeButton.setEnabled(false);
                    }
                }
        );
        this.eletraMCIReadAsyncTask2.execute();
        MeterSchema meterSchema = MeterSchema.PANTHEON_V1;
        MeterComm meterComm = new MeterComm();
        meterComm.loadSchema(meterSchema, "DLMSTCP");
        DLMSCommand dlmsCommand = (DLMSCommand) meterComm.getAvailableCommands().get(EletraMCIExecEnum.RELAY_OPERATION_ACTION.getWriteCommand());

        HashMap<EletraMCIWriteEnum, String> values = new HashMap<EletraMCIWriteEnum, String>();

        String attr = "02 03 11 00 01 0" + Integer.toString(units.size(), 16) + buildArrayOfMeters(units) + " 11 00";

        values.put(EletraMCIWriteEnum.ACTION_RELAY, attr);

        for (Map.Entry<EletraMCIWriteEnum, String> entry : values.entrySet()) {
            dlmsCommand.getAttById(entry.getKey().getWriteCommand()).setValueProperty(entry.getValue());
        }

        this.eletraMCIExecAsyncTask = new EletraMCIExecAsyncTask(RelaysControlsActivity.this,
                dlmsCommand,
                (error)-> {
                    if(error > 0) {
                        textView.setText(error);
                        if(error == R.string.exception_generic) {
                            textView.setText(R.string.exception_generic_exec_relays);
                        }
                        this.showButtonErro(this.isHightLevel());
                        relativeLayoutLoad.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        swipeRefreshLayout.setVisibility(View.GONE);
                        openButton.setEnabled(false);
                        closeButton.setEnabled(false);
                        relativeLayoutError.setVisibility(View.VISIBLE);
                        if(this.progressWrite != null) this.progressWrite.dismiss();
                    } else {
                        if(this.progressWrite != null) this.progressWrite.dismiss();
                        units = new ArrayList<String>();

                        openButton.setEnabled(false);
                        closeButton.setEnabled(false);


                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                        int waitTime = Integer.parseInt(prefs.getString(getString(R.string.key_wait_time), "20"));

                        if(isEnableHHU) {
                            Snackbar.make(toolbar, getString(R.string.success_action), Snackbar.LENGTH_LONG).show();

                            swipeRefreshLayout.setVisibility(View.GONE);
                            if(waitTime != 0) timerCountLayout.setVisibility(View.VISIBLE);
                            relativeLayoutLoad.setVisibility(View.VISIBLE);
                            waitTimeMessage.setText(R.string.wait_time_message);

                            new CountDownTimer(waitTime*1000, 1000) {

                                public void onTick(long millisUntilFinished) {
                                    textCountDown.setText("00:" + (millisUntilFinished / 1000 < 10 ? "0"+millisUntilFinished / 1000 : millisUntilFinished / 1000));
                                }

                                public void onFinish() {
                                    textCountDown.setText("00:00");
                                    waitTimeMessage.setText(R.string.loading_status);
                                    loadRelaysControl();
                                }
                            }.start();
                        }

                        else {
                            textView.setText(R.string.exception_hhu_disable);
                            this.showButtonErro(this.isHightLevel());
                            relativeLayoutLoad.setVisibility(View.GONE);
                            swipeRefreshLayout.setRefreshing(false);
                            swipeRefreshLayout.setVisibility(View.GONE);
                            openButton.setEnabled(false);
                            closeButton.setEnabled(false);
                            relativeLayoutError.setVisibility(View.VISIBLE);
                            if(this.progressWrite != null) this.progressWrite.dismiss();
                        }
                    }
                },
                task -> {
                    if(this.isHightLevel() > 0) {
                        this.preRead(task);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(RelaysControlsActivity.this);
                        LayoutInflater inflater = LayoutInflater.from(RelaysControlsActivity.this);
                        View view = inflater.inflate(R.layout.dialog_progress, null, false);
                        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBarProgress);
                        TextView textView = (TextView) view.findViewById(R.id.textViewProgress);
                        textView.setText(R.string.processing_close_relays);
                        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(RelaysControlsActivity.this);
                        circularProgressDrawable.setStyle(CircularProgressDrawable.LARGE);
                        circularProgressDrawable.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
                        progressBar.setIndeterminateDrawable(circularProgressDrawable);
                        builder.setView(view);
                        builder.setCancelable(false);
                        this.progressWrite = builder.create();
                        this.progressWrite.show();
                    }
                }
        );
        this.eletraMCIExecAsyncTask.execute();
    }

    private String buildArrayOfMeters(ArrayList<String> result) {
        StringBuilder builder = new StringBuilder();
        String space = result.size() > 0 ? " " : "";
        for (String anArr : result) {
            builder.append(space).append("11 0").append(anArr);
        }
        return builder.toString();
    }

    @Override
    public boolean onSupportNavigateUp(){
        this.onBackPressed();
        return true;
    }

    @Override
    public void onDestroy() {
        if (eletraMCIReadAsyncTask != null) eletraMCIReadAsyncTask.cancel(true);
        if (eletraMCIReadAsyncTask2 != null) eletraMCIReadAsyncTask2.cancel(true);
        if (eletraMCIExecAsyncTask != null) eletraMCIExecAsyncTask.cancel(true);
        super.onDestroy();
    }

    protected void postRead(List<DLMSAtt> attributes) {

        HashMap<Integer, String> phaseMap = new HashMap<Integer, String>();
        phaseMap.put(1, "A");
        phaseMap.put(2, "B");
        phaseMap.put(3, "C");

        phaseMap.put(4, "A");
        phaseMap.put(5, "B");
        phaseMap.put(6, "C");

        phaseMap.put(7, "A");
        phaseMap.put(8, "B");
        phaseMap.put(9, "C");

        phaseMap.put(10, "A");
        phaseMap.put(11, "B");
        phaseMap.put(12, "C");

        ArrayList<RelaysControlsRecyclerViewAdapter.RelayControlItem> itens = new ArrayList<RelaysControlsRecyclerViewAdapter.RelayControlItem>();
        RelaysControlsRecyclerViewAdapter.RelayControlItem it;

        for(DLMSAtt attribute : attributes) {

            if(Strings.isNullOrEmpty(attribute.getAtt().get(0).getValueProperty())) continue;

            it = new RelaysControlsRecyclerViewAdapter.RelayControlItem();

            it.number = attribute.getAtt().get(0).getValueProperty();
            it.phase = new String[3];
            it.relay = new Integer[3];
            it.pos = new String[3];
            it.postRelay = new String[3];
            it.fails = new String[3];
            it.reverseCurrent = new String[3];
            it.timeout = new String[3];

            //magnitudes = 9
            List<DLMSAtt> subAttributes = attribute.getAtt().get(9).getAtt();

            for(int cont = 0; cont <  subAttributes.size() ; cont ++) {

                it.phase[cont] = phaseMap.get(Integer.parseInt(attribute.getAtt().get(1).getValueProperty()) + cont);
                it.pos[cont] = String.valueOf(Integer.parseInt(attribute.getAtt().get(1).getValueProperty()) + cont);
                int relayStatus = Integer.parseInt(subAttributes.get(cont).getAtt().get(3).getValueProperty());

                if ((relayStatus & 0x01) == 0x01) {
                    // reading failed
                    it.relay[cont] = R.drawable.relay_warning;
                    it.postRelay[cont] = RelaysControlsActivity.this.getString(R.string.no);
                } else if ((relayStatus & 0x04) == 0x00) {
                    // relay error
					it.relay[cont] = R.drawable.relay_open_warning_volt_v7;
                    it.postRelay[cont] = RelaysControlsActivity.this.getString(R.string.yes);
				} else if ((relayStatus & 0x02) == 0x00) {
                    // closed relay
                    it.relay[cont] = R.drawable.relay_closed;
                    it.postRelay[cont] = RelaysControlsActivity.this.getString(R.string.no);
                } else {
                    // value = 6
                    // open relay
                    it.relay[cont] = R.drawable.relay_open;
                    it.postRelay[cont] = RelaysControlsActivity.this.getString(R.string.no);
                }

                if(subAttributes.get(cont).getAtt().get(4).getValueProperty() == null ||
                        subAttributes.get(cont).getAtt().get(4).getValueProperty().isEmpty())   subAttributes.get(cont).getAtt().get(4).setValueProperty("0");

                int decFlags = Integer.parseInt(subAttributes.get(cont).getAtt().get(4).getValueProperty());
                String binStrVal = Integer.toBinaryString(decFlags);

                while(binStrVal.length() < 5) binStrVal = "0"+binStrVal;

                it.fails[cont] = getResult(binStrVal, 2, 5);
                it.reverseCurrent[cont] = getResult(binStrVal, 1, 2);
                it.timeout[cont] = getResult(binStrVal, 0, 1);
            }
            int rStatus = Integer.parseInt(subAttributes.get(0).getAtt().get(3).getValueProperty());
            Log.i(RelaysControlsActivity.class.getName(), "RELAY STATUS: " + rStatus);
            it.isEnable = true;
            itens.add(it);
        }
        this.adapter = new RelaysControlsRecyclerViewAdapter((RelaysControlsActivity) RelaysControlsActivity.this, itens);

        recyclerView.setAdapter(this.adapter);
        relativeLayoutError.setVisibility(View.GONE);
        relativeLayoutLoad.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
        this.units.clear();
        openButton.setEnabled(false);
        closeButton.setEnabled(false);
        swipeRefreshLayout.setVisibility(View.VISIBLE);

        if(this.progressWrite != null) this.progressWrite.dismiss();
    }

    private String getResult(String binStr, int begin, int end) {
        return binStr.subSequence(begin, end).toString().contains("1") ? RelaysControlsActivity.this.getString(R.string.yes) : RelaysControlsActivity.this.getString(R.string.no);
    }

    protected void showButtonErro(int err) {
        if(err == R.string.error_level) {
            errorButton.setText(R.string.bt_credential_setting);
            errorButton.setVisibility(View.VISIBLE);
            errorButton.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            errorButton.setOnClickListener(v -> {
                this.startActivity(new Intent(RelaysControlsActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_auth) {
            errorButton.setText(R.string.bt_credential_setting);
            errorButton.setVisibility(View.VISIBLE);
            errorButton.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            errorButton.setOnClickListener(v -> {
                this.startActivity(new Intent(RelaysControlsActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_encrypt) {
            errorButton.setText(R.string.bt_credential_setting);
            errorButton.setVisibility(View.VISIBLE);
            errorButton.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            errorButton.setOnClickListener(v -> {
                this.startActivity(new Intent(RelaysControlsActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_security) {
            errorButton.setText(R.string.bt_credential_setting);
            errorButton.setVisibility(View.VISIBLE);
            errorButton.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            errorButton.setOnClickListener(v -> {
                this.startActivity(new Intent(RelaysControlsActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_ip) {
            errorButton.setText(R.string.bt_communication_setting);
            errorButton.setVisibility(View.VISIBLE);
            errorButton.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp), null, null, null);
            errorButton.setOnClickListener(v -> {
                this.startActivity(new Intent(RelaysControlsActivity.this, CommunicationSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_port) {
            errorButton.setText(R.string.bt_communication_setting);
            errorButton.setVisibility(View.VISIBLE);
            errorButton.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp), null, null, null);
            errorButton.setOnClickListener(v -> {
                this.startActivity(new Intent(RelaysControlsActivity.this, CommunicationSettingActivity.class));
            });
            return;
        }
        errorButton.setText(R.string.bt_retry);
        errorButton.setVisibility(View.VISIBLE);
        errorButton.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_refresh_black_24dp), null, null, null);
        errorButton.setOnClickListener(v -> {
            relativeLayoutError.setVisibility(View.GONE);
            relativeLayoutLoad.setVisibility(View.VISIBLE);
            this.loadRelaysControl();
        });
        return;
    }

    protected void preRead(AsyncTask task) {
        task.cancel(true);
        textView.setText(this.isHightLevel());
        this.showButtonErro(this.isHightLevel());
        relativeLayoutLoad.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
        openButton.setEnabled(false);
        closeButton.setEnabled(false);
        swipeRefreshLayout.setVisibility(View.GONE);
        relativeLayoutError.setVisibility(View.VISIBLE);
    }
}
