package com.eletraenergy.pantheon.mobile.ui.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.ui.adapters.ChangesRecyclerViewAdapter;

import java.util.ArrayList;


public class ChangesFragment extends Fragment {

    protected RecyclerView recyclerView;

    public ChangesFragment() {
        // Required empty public constructor
    }

    public static ChangesFragment newInstance() {
        ChangesFragment fragment = new ChangesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_actions, container, false);
        recyclerView = view.findViewById(R.id.recyclerViewActions);

        ArrayList<ChangesRecyclerViewAdapter.ItemMain> itens = new ArrayList<>();

        ChangesRecyclerViewAdapter.ItemMain item = new ChangesRecyclerViewAdapter.ItemMain();
        item.text = getResources().getString(R.string.alarm_settings);
        item.image = getResources().getDrawable(R.drawable.ic_alarm_settings);
        itens.add(item);

        item = new ChangesRecyclerViewAdapter.ItemMain();
        item.text = getResources().getString(R.string.sync_date_time);
        item.image = getResources().getDrawable(R.drawable.ic_date_time_sync);
        itens.add(item);

        ChangesRecyclerViewAdapter adapter = new ChangesRecyclerViewAdapter((AppCompatActivity) this.getActivity(), itens);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(adapter);

        return view;
    }

}
