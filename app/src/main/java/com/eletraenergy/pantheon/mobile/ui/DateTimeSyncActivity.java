package com.eletraenergy.pantheon.mobile.ui;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadAsyncTask;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadTitle;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIWriteAsyncTask;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIWriteEnum;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.common.base.Strings;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import br.com.eletra.mci.app.MeterComm;
import br.com.eletra.mci.app.MeterSchema;
import br.com.eletra.mci.dlms.DLMSCommand;
import br.com.eletra.mci.util.HexString;

public class DateTimeSyncActivity extends PantheonCompatActivity {

    protected Toolbar toolbar;
    protected EletraMCIReadAsyncTask eletraMCIReadAsyncTask;
    protected EletraMCIWriteAsyncTask eletraMCIWriteAsyncTask;
    protected AppCompatButton sendButton;
    protected LinearLayout linearLayoutError;
    protected TextView textViewError;
    protected EditText dateTextView;
    protected EditText timeTextView;
    protected EletraMCIReadTitle eletraMCIReadTitle;
    protected ArrayList<String> units;
    protected AlertDialog progressWrite;
    protected Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_time_sync);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        units = new ArrayList<String>();
        calendar = Calendar.getInstance();
        sendButton = (AppCompatButton) findViewById(R.id.sendButton);

        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);

        linearLayoutError = (LinearLayout) findViewById(R.id.linearLayoutErrorSync);
        textViewError = (TextView) findViewById(R.id.textViewSync);

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);

        dateTextView = (EditText) findViewById(R.id.dateEditTextSync);
        String data = Strings.padStart(String.valueOf(day), 2, '0') + "/" + Strings.padStart(String.valueOf(month+1), 2, '0') + "/" + String.valueOf(year);
        dateTextView.setText(data);

        timeTextView = (EditText) findViewById(R.id.timeEditTextSync);
        String hora = Strings.padStart(String.valueOf(hour), 2, '0') + ":" + Strings.padStart(String.valueOf(minute), 2, '0');
        timeTextView.setText(hora);

        eletraMCIReadTitle = new EletraMCIReadTitle();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void showDate(View view) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(DateTimeSyncActivity.this,  R.style.PickerDialogStyle, (DatePickerDialog.OnDateSetListener) (view1, year1, month1, dayOfMonth) -> {
            String data = Strings.padStart(String.valueOf(dayOfMonth), 2, '0') + "/" + Strings.padStart(String.valueOf(month1+1), 2, '0') + "/" + String.valueOf(year1);

            calendar.set(Calendar.YEAR, year1);
            calendar.set(Calendar.MONTH, month1);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            dateTextView.setText(data);
        }, year, month, day);
        datePickerDialog.show();
    }

    public void showTime(View view) {
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        TimePickerDialog datePickerDialog = new TimePickerDialog(DateTimeSyncActivity.this, R.style.PickerDialogStyle, (view1, hourOfDay, minute1) -> {
            String hora = Strings.padStart(String.valueOf(hourOfDay), 2, '0') + ":" + Strings.padStart(String.valueOf(minute1), 2, '0');

            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendar.set(Calendar.MINUTE, minute1);

            timeTextView.setText(hora);
        }, hour, minute, false);
        datePickerDialog.show();
    }

    public void showSendTransferDialog(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(DateTimeSyncActivity.this);
        builder.setTitle(getString(R.string.warning));
        builder.setMessage(R.string.app_date_time);
        builder.setIcon(R.drawable.eletra_icon);
        builder.setPositiveButton(getString(R.string.yes),
                (dialog, which) -> {
                    sendSyncDateTimeCommand();
                    dialog.dismiss();

                });
        builder.setNegativeButton(getString(R.string.no),
                (dialog, which) -> dialog.dismiss());
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private String getDateTime() {
        String sBytes = "";

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        Log.i("Ano", HexString.IntToString(year));
        sBytes += " " + HexString.IntToString(year);

        Log.i("Mês", HexString.IntToString(month).substring(3, 5));
        sBytes += " " + HexString.IntToString(month).substring(3, 5);

        Log.i("Dia", HexString.IntToString(day).substring(3, 5));
        sBytes += " " + HexString.IntToString(day).substring(3, 5);

        Log.i("Semana", HexString.IntToString(calendar.get(Calendar.DAY_OF_WEEK)).substring(3, 5));
        sBytes += " " + HexString.IntToString(calendar.get(Calendar.DAY_OF_WEEK)).substring(3, 5);

        Log.i("Hora", HexString.IntToString(hour).substring(3, 5));
        sBytes += " " + HexString.IntToString(hour).substring(3, 5);

        Log.i("Minuto", HexString.IntToString(minute).substring(3, 5));
        sBytes += " " + HexString.IntToString(minute).substring(3, 5);

        Log.i("Segundo", "00");
        sBytes += " 00 00 80 00 00";

        return sBytes;
    }

    private void sendSyncDateTimeCommand() {
        if (eletraMCIReadAsyncTask != null) eletraMCIReadAsyncTask.cancel(true);
        MeterSchema meterSchema = MeterSchema.PANTHEON_V1;
        MeterComm meterComm = new MeterComm();
        meterComm.loadSchema(meterSchema, "DLMSTCP");
        DLMSCommand dlmsCommand = (DLMSCommand) meterComm.getAvailableCommands().get(EletraMCIWriteEnum.CHECKPOINT_READ.getWriteCommand());

        HashMap<EletraMCIWriteEnum, String> values = new HashMap<EletraMCIWriteEnum, String>();
        String attr = "01 02 04 02 04 12 00 08 09 06 00 00 01 00 00 FF 0F 02 12 00 00 09 0C 07 E3 02 0D 03 00 00 00 FF 80 00 00 09 0C " + getDateTime();
        Log.i("Sync", attr);
        values.put(EletraMCIWriteEnum.CHECKPOINTREAD_att, attr);

        for (Map.Entry<EletraMCIWriteEnum, String> entry : values.entrySet()) {
            dlmsCommand.getAttById(entry.getKey().getWriteCommand()).setValueProperty(entry.getValue());
        }

        this.eletraMCIReadAsyncTask = new EletraMCIReadAsyncTask(DateTimeSyncActivity.this,
                dlmsCommand,
                (attrs, error)-> {
                    if(error > 0) {
                        textViewError.setText(error);
                        if(error == R.string.exception_generic) {
                            textViewError.setText(R.string.exception_generic_sync);
                        }
                        linearLayoutError.setVisibility(View.VISIBLE);
                        if(this.progressWrite != null) this.progressWrite.dismiss();
                    } else {
                        if(this.progressWrite != null) this.progressWrite.dismiss();
                        sendButton.setEnabled(true);
                        Snackbar.make(toolbar, getString(R.string.success_action), Snackbar.LENGTH_SHORT).show();
                    }
                },
                task -> {
                    if(this.isHightLevel() > 0) {
                        this.preWrite(task);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(DateTimeSyncActivity.this);
                        LayoutInflater inflater = LayoutInflater.from(DateTimeSyncActivity.this);
                        View view = inflater.inflate(R.layout.dialog_progress, null, false);
                        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBarProgress);
                        TextView textView = (TextView) view.findViewById(R.id.textViewProgress);
                        textView.setText(R.string.processing_sync_date_time);
                        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(DateTimeSyncActivity.this);
                        circularProgressDrawable.setStyle(CircularProgressDrawable.LARGE);
                        circularProgressDrawable.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
                        progressBar.setIndeterminateDrawable(circularProgressDrawable);
                        builder.setView(view);
                        builder.setCancelable(false);
                        this.progressWrite = builder.create();
                        this.progressWrite.show();
                    }
                }
        );
        this.eletraMCIReadAsyncTask.execute();
    }

    protected void preWrite(AsyncTask task) {
        task.cancel(true);
        textViewError.setText(this.isHightLevel());
        sendButton.setEnabled(false);
        linearLayoutError.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onSupportNavigateUp(){
        this.onBackPressed();
        return true;
    }

    @Override
    public void onDestroy() {
        if (eletraMCIReadAsyncTask != null) eletraMCIReadAsyncTask.cancel(true);
        if (eletraMCIWriteAsyncTask != null) eletraMCIWriteAsyncTask.cancel(true);
        super.onDestroy();
    }

}
