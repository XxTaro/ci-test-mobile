package com.eletraenergy.pantheon.mobile.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadAsyncTask;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadEnum;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadTitle;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIWriteAsyncTask;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIWriteEnum;
import com.eletraenergy.pantheon.mobile.ui.adapters.AlarmSettingsRecyclerViewAdapter;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import br.com.eletra.mci.app.MeterComm;
import br.com.eletra.mci.app.MeterSchema;
import br.com.eletra.mci.dlms.DLMSAtt;
import br.com.eletra.mci.dlms.DLMSCommand;
import br.com.eletra.mci.util.HexString;

public class AlarmSettingsActivity extends PantheonCompatActivity {

    protected Toolbar toolbar;
    protected EletraMCIReadAsyncTask eletraMCIReadAsyncTask;
    protected EletraMCIWriteAsyncTask eletraMCIWriteAsyncTask;
    protected RecyclerView recyclerView;
    protected RelativeLayout relativeLayoutLoad;
    protected ProgressBar progressBar;
    protected RelativeLayout relativeLayoutError;
    protected TextView textView;
    protected EletraMCIReadTitle eletraMCIReadTitle;
    protected SwipeRefreshLayout swipeRefreshLayout;
    protected Button button;
    protected Button sendButton;
    protected AlertDialog progressWrite;

    protected Boolean[] enabledItens = {false, false, false};
    protected Boolean[] valueItens = {false, false, false};
    protected Boolean isEnableHHU;

    protected void availableTransfer(){
        boolean available = false;
        if(enabledItens[0]) available = true;
        if(enabledItens[1]) available = true;
        if(enabledItens[2]) available = true;
        sendButton.setEnabled(available);
    }
    public void enableItem(int i, boolean value) {
        this.enabledItens[i] = value;
        this.availableTransfer();
    }
    public void seValueItem(int i, boolean value) {
        this.valueItens[i] = value;
        this.availableTransfer();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_settings);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);

        eletraMCIReadTitle = new EletraMCIReadTitle();

        progressBar = (ProgressBar) findViewById(R.id.progressBarAlarmSettings);
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(AlarmSettingsActivity.this);
        circularProgressDrawable.setStyle(CircularProgressDrawable.LARGE);
        circularProgressDrawable.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        progressBar.setIndeterminateDrawable(circularProgressDrawable);

        relativeLayoutLoad = (RelativeLayout) findViewById(R.id.relativeLayoutLoadAlarmSettings);
        relativeLayoutError = (RelativeLayout) findViewById(R.id.relativeLayoutErrorAlarmSettings);
        textView = (TextView) findViewById(R.id.textViewAlarmSettings);
        sendButton = (Button) findViewById(R.id.sendButton);
        button = (Button) findViewById(R.id.buttonAlarmSettings);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshCPUInformation);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewCPUInformation);
        LinearLayoutManager layoutManager = new LinearLayoutManager(AlarmSettingsActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        swipeRefreshLayout.setOnRefreshListener(() -> AlarmSettingsActivity.this.loadAlarmSettings());
    }

    @Override
    protected void onResume() {
        super.onResume();
        relativeLayoutError.setVisibility(View.GONE);
        swipeRefreshLayout.setVisibility(View.GONE);
        relativeLayoutLoad.setVisibility(View.VISIBLE);
        this.loadAlarmSettings();
    }

    protected void loadAlarmSettings() {
        if (eletraMCIReadAsyncTask != null) eletraMCIReadAsyncTask.cancel(true);
        this.eletraMCIReadAsyncTask = new EletraMCIReadAsyncTask(
                AlarmSettingsActivity.this,
                EletraMCIReadEnum.ALARM_CONFIGURATION,
                (atts, error)-> {
                    Log.i(AlarmSettingsActivity.class.getSimpleName(), "Leitura terminada...");
                    if(error > 0) {
                        textView.setText(error);
                        if(error == R.string.exception_generic) {
                            textView.setText(R.string.exception_generic_alarm_settings);
                        }
                        this.showButtonErro(error);
                        relativeLayoutLoad.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        swipeRefreshLayout.setVisibility(View.GONE);
                        relativeLayoutError.setVisibility(View.VISIBLE);
                    } else {
                        this.postRead(atts);
                    }
                },
                task -> {
                    Log.i(AlarmSettingsActivity.class.getSimpleName(), "Leitura iniciada...");
                    if(this.isHightLevel() > 0) {
                        this.preRead(task);
                    }
                }
        );
        this.eletraMCIReadAsyncTask.execute();
    }

    protected void showButtonErro(int err) {
        if(err == R.string.error_level) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmSettingsActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_auth) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmSettingsActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_encrypt) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmSettingsActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_security) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmSettingsActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_ip) {
            button.setText(R.string.bt_communication_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmSettingsActivity.this, CommunicationSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_port) {
            button.setText(R.string.bt_communication_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmSettingsActivity.this, CommunicationSettingActivity.class));
            });
            return;
        }
        button.setText(R.string.bt_retry);
        button.setVisibility(View.VISIBLE);
        button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_refresh_black_24dp), null, null, null);
        button.setOnClickListener(v -> {
            relativeLayoutError.setVisibility(View.GONE);
            relativeLayoutLoad.setVisibility(View.VISIBLE);
            this.loadAlarmSettings();
        });
        return;
    }

    @Override
    public boolean onSupportNavigateUp(){
        this.onBackPressed();
        return true;
    }

    @Override
    public void onDestroy() {
        if (eletraMCIReadAsyncTask != null) eletraMCIReadAsyncTask.cancel(true);
        if (eletraMCIWriteAsyncTask != null) eletraMCIWriteAsyncTask.cancel(true);
        super.onDestroy();
    }

    protected void postRead(List<DLMSAtt> attributes) {

        //mask->0
        int mask = Integer.parseInt(attributes.get(0).getValueProperty());
        //config->1
        int config = Integer.parseInt(attributes.get(1).getValueProperty());

        boolean doorAlarm = (config & 0x01) == 0x01;
        boolean collectiveOpenAlarm = (config & 0x02) == 0x02;
        boolean hhuWriteAlarm = (config & 0x08) == 0x08;
        this.isEnableHHU = hhuWriteAlarm;

        ArrayList<AlarmSettingsRecyclerViewAdapter.AlarmSettingsItem> itens = new ArrayList<AlarmSettingsRecyclerViewAdapter.AlarmSettingsItem>();
        AlarmSettingsRecyclerViewAdapter.AlarmSettingsItem it = new AlarmSettingsRecyclerViewAdapter.AlarmSettingsItem();
        it.title = getString(R.string.open_port_alarm);
        it.value = doorAlarm;
        itens.add(it);

        it = new AlarmSettingsRecyclerViewAdapter.AlarmSettingsItem();
        it.title = getString(R.string.collective_open_relay_alarm);
        it.value = collectiveOpenAlarm;
        itens.add(it);

        it = new AlarmSettingsRecyclerViewAdapter.AlarmSettingsItem();
        it.title = getString(R.string.hhu);
        it.value = hhuWriteAlarm;
        itens.add(it);

        this.enabledItens[0] = false;
        this.enabledItens[1] = false;
        this.enabledItens[2] = false;

        this.valueItens[0] = doorAlarm;
        this.valueItens[1] = collectiveOpenAlarm;
        this.valueItens[2] = hhuWriteAlarm;

        AlarmSettingsRecyclerViewAdapter adapter = new AlarmSettingsRecyclerViewAdapter(AlarmSettingsActivity.this, itens);
        recyclerView.setAdapter(adapter);

        relativeLayoutError.setVisibility(View.GONE);
        relativeLayoutLoad.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setVisibility(View.VISIBLE);
    }

    public void showSendTransferDialog(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AlarmSettingsActivity.this);
        builder.setTitle(getString(R.string.warning));
        builder.setMessage(R.string.app_alarm_settings);
        builder.setIcon(R.drawable.eletra_icon);
        builder.setPositiveButton(getString(R.string.yes),
                (dialog, which) -> {
                    sendAlarmSettingsCommand();
                    dialog.dismiss();
                });
        builder.setNegativeButton(getString(R.string.no),
                (dialog, which) -> dialog.dismiss());
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void sendAlarmSettingsCommand() {

        if (eletraMCIWriteAsyncTask != null) eletraMCIWriteAsyncTask.cancel(true);

        MeterSchema meterSchema = MeterSchema.PANTHEON_V1;
        MeterComm meterComm = new MeterComm();
        meterComm.loadSchema(meterSchema, "DLMSTCP");
        DLMSCommand dlmsCommand = (DLMSCommand) meterComm.getAvailableCommands().get(EletraMCIWriteEnum.ALARM_CONFIGURATION.getWriteCommand());

        String attr = String.valueOf(Integer.parseInt(getMask(), 16) + " | " + String.valueOf(Integer.parseInt(getSettings(), 16)));
        Log.i("Alarm Settings", attr);

        dlmsCommand.getAttById(EletraMCIWriteEnum.ALARMCONFIGURATION_att.getWriteCommand()).getAttById(EletraMCIWriteEnum.MASK_att.getWriteCommand()).setValueProperty(String.valueOf(Integer.parseInt(getMask(), 16)));
        dlmsCommand.getAttById(EletraMCIWriteEnum.ALARMCONFIGURATION_att.getWriteCommand()).getAttById(EletraMCIWriteEnum.CONFIG_att.getWriteCommand()).setValueProperty(String.valueOf(Integer.parseInt(getSettings(), 16)));

        this.eletraMCIWriteAsyncTask = new EletraMCIWriteAsyncTask(
                AlarmSettingsActivity.this,
                dlmsCommand,
                (error)-> {
                    Log.i(AlarmSettingsActivity.class.getSimpleName(), "Escrita terminada...");
                    Log.i(AlarmSettingsActivity.class.getSimpleName(), "Erro: " + String.valueOf(error));
                    if(error > 0) {
                        textView.setText(error);
                        if(error == R.string.exception_generic) {
                            textView.setText(R.string.exception_generic_alarm_settings);
                        }
                        this.showButtonErro(this.isHightLevel());
                        relativeLayoutLoad.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        swipeRefreshLayout.setVisibility(View.GONE);
                        sendButton.setEnabled(false);
                        relativeLayoutError.setVisibility(View.VISIBLE);
                        if(this.progressWrite != null) this.progressWrite.dismiss();
                    } else {
                        if(this.progressWrite != null) this.progressWrite.dismiss();
                        sendButton.setEnabled(false);
                        Snackbar.make(toolbar, getString(R.string.success_action), Snackbar.LENGTH_SHORT).show();
                        swipeRefreshLayout.setVisibility(View.GONE);
                        relativeLayoutLoad.setVisibility(View.VISIBLE);
                        this.loadAlarmSettings();
                    }
                },
                task -> {
                    Log.i(AlarmSettingsActivity.class.getSimpleName(), "Escrita iniciada...");
                    if(this.isHightLevel() > 0) {
                        this.preRead(task);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(AlarmSettingsActivity.this);
                        LayoutInflater inflater = LayoutInflater.from(AlarmSettingsActivity.this);
                        View view = inflater.inflate(R.layout.dialog_progress, null, false);
                        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBarProgress);
                        TextView textView = (TextView) view.findViewById(R.id.textViewProgress);
                        textView.setText(R.string.processing_alarm_settings);
                        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(AlarmSettingsActivity.this);
                        circularProgressDrawable.setStyle(CircularProgressDrawable.LARGE);
                        circularProgressDrawable.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
                        progressBar.setIndeterminateDrawable(circularProgressDrawable);
                        builder.setView(view);
                        builder.setCancelable(false);
                        this.progressWrite = builder.create();
                        this.progressWrite.show();
                    }
                }
        , isEnableHHU);
        this.eletraMCIWriteAsyncTask.execute();

    }

    private String getMask() {
        int mask = 0;
        if (this.enabledItens[0]) {
            mask |= 0x01;
        }
        if (this.enabledItens[1]) {
            mask |= 0x02;
        }
        if (this.enabledItens[2]) {
            mask |= 0x08;
        }
        return HexString.IntToString(mask).substring(3);
    }

    private String getSettings() {
        int value = 0;
        if (this.enabledItens[0] && this.valueItens[0]) {
            value |= 0x01;
        }
        if (this.enabledItens[1] && this.valueItens[1]) {
            value |= 0x02;
        }
        if (this.enabledItens[2] && this.valueItens[2]) {
            value |= 0x08;
        }
        return HexString.IntToString(value).substring(3);
    }

    protected void preRead(AsyncTask task) {
        task.cancel(true);
        textView.setText(this.isHightLevel());
        this.showButtonErro(this.isHightLevel());
        relativeLayoutLoad.setVisibility(View.GONE);
        swipeRefreshLayout.setVisibility(View.GONE);
        relativeLayoutError.setVisibility(View.VISIBLE);
    }
}
