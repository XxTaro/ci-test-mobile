package com.eletraenergy.pantheon.mobile.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIWriteAsyncTask;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIWriteEnum;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.common.base.Strings;

import br.com.eletra.mci.app.MeterComm;
import br.com.eletra.mci.app.MeterSchema;
import br.com.eletra.mci.dlms.DLMSAtt;
import br.com.eletra.mci.dlms.DLMSCommand;

public class AlarmClearingActivity extends PantheonCompatActivity {

    protected Toolbar toolbar;
    protected EletraMCIWriteAsyncTask eletraMCIWriteAsyncTask;
    protected RelativeLayout relativeLayoutError;
    protected LinearLayout linearLayout;
    protected TextView textView;
    protected Button button;
    protected Button sendButton;
    protected AlertDialog progressWrite;

    protected EditText editText;
    protected AppCompatCheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_clearing);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);

        linearLayout = (LinearLayout) findViewById(R.id.linearLayoutAlarmClearing);
        relativeLayoutError = (RelativeLayout) findViewById(R.id.relativeLayoutErrorAlarmClearing);
        textView = (TextView) findViewById(R.id.textViewAlarmClearing);
        sendButton = (Button) findViewById(R.id.sendButton);
        button = (Button) findViewById(R.id.buttonAlarmClearing);

        editText = (EditText) findViewById(R.id.editTextAlarmClearing);
        editText.setEnabled(false);
        checkBox = (AppCompatCheckBox) findViewById(R.id.checkBoxAlarmClearing);
        checkBox.setChecked(true);
        checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked) {
                editText.setText("");
                editText.setEnabled(false);
                linearLayout.requestFocus();
            } else {
                editText.setEnabled(true);
                editText.requestFocus();
            }
        });

    }

    protected void showButtonErro(int err) {
        if (err == R.string.error_level) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmClearingActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if (err == R.string.error_auth) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmClearingActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if (err == R.string.error_encrypt) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmClearingActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if (err == R.string.error_security) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmClearingActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if (err == R.string.error_ip) {
            button.setText(R.string.bt_communication_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmClearingActivity.this, CommunicationSettingActivity.class));
            });
            return;
        }
        if (err == R.string.error_port) {
            button.setText(R.string.bt_communication_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(AlarmClearingActivity.this, CommunicationSettingActivity.class));
            });
            return;
        }
        button.setText(R.string.bt_retry);
        button.setVisibility(View.VISIBLE);
        button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_refresh_black_24dp), null, null, null);
        button.setOnClickListener(v -> {
            relativeLayoutError.setVisibility(View.GONE);
            sendAlarmClearingCommand();
        });
        return;
    }

    @Override
    public boolean onSupportNavigateUp() {
        this.onBackPressed();
        return true;
    }

    @Override
    public void onDestroy() {
        if (eletraMCIWriteAsyncTask != null) eletraMCIWriteAsyncTask.cancel(true);
        super.onDestroy();
    }

    private boolean isValid() {
        boolean res = true;
        if(!checkBox.isChecked()) {
            if (Strings.isNullOrEmpty(editText.getText().toString()) ||
                    Integer.parseInt(editText.getText().toString()) < 1) {
                Toast.makeText(this, R.string.valid_number_records, Toast.LENGTH_SHORT).show();
                res = false;
                return res;
            }
            if (Strings.isNullOrEmpty(editText.getText().toString()) ||
                    Integer.parseInt(editText.getText().toString()) > 255) {
                Toast.makeText(this, R.string.valid_number_records, Toast.LENGTH_SHORT).show();
                res = false;
                return res;
            }
        }
        return res;
    }

    public void showSendTransferDialog(View view) {
        if(isValid()) {

            AlertDialog.Builder builder = new AlertDialog.Builder(AlarmClearingActivity.this);
            builder.setTitle(getString(R.string.warning));
            builder.setMessage(R.string.app_confirm_alarm_clearing);
            builder.setIcon(R.drawable.eletra_icon);
            builder.setView(R.layout.dialog_alarm_clearing_confirm);
            builder.setPositiveButton(getString(R.string.yes),
                    (dialog, which) -> {
                        //sendDisplayPairingCommand();
                        CheckBox checkBox = ((AlertDialog) dialog).findViewById(R.id.confirmCheckBoxAlarmClearing);
                        if (checkBox.isChecked()) {
                            sendAlarmClearingCommand();
                        } else {
                            Toast.makeText(this, R.string.action_canceled, Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    });
            builder.setNegativeButton(getString(R.string.no),
                    (dialog, which) -> dialog.dismiss());
            AlertDialog dialog = builder.create();
            dialog.show();

            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);

            CheckBox checkBox = ((AlertDialog) dialog).findViewById(R.id.confirmCheckBoxAlarmClearing);
            checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked) {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                } else {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                }
            });
        }
    }

    private void sendAlarmClearingCommand() {

        if (eletraMCIWriteAsyncTask != null) eletraMCIWriteAsyncTask.cancel(true);

        MeterSchema meterSchema = MeterSchema.PANTHEON_V1;
        MeterComm meterComm = new MeterComm();
        meterComm.loadSchema(meterSchema, "DLMSTCP");
        DLMSCommand dlmsCommand = (DLMSCommand) meterComm.getAvailableCommands().get(EletraMCIWriteEnum.ALARM_CLEAR.getWriteCommand());
        DLMSAtt clearAtt = dlmsCommand.getAttById(EletraMCIWriteEnum.ALARMCLEAR_att.getWriteCommand());

        if(checkBox.isChecked()) {
            clearAtt.setValueProperty("00");
            clearAtt.getAttById(EletraMCIWriteEnum.TYPE_att.getWriteCommand()).setValueProperty("00");
            clearAtt.getAttById(EletraMCIWriteEnum.AMOUNT_att.getWriteCommand()).setValueProperty("00");
        } else {
            clearAtt.getAttById(EletraMCIWriteEnum.TYPE_att.getWriteCommand()).setValueProperty("01");
            clearAtt.getAttById(EletraMCIWriteEnum.AMOUNT_att.getWriteCommand()).setValueProperty(editText.getText().toString());
        }

        this.eletraMCIWriteAsyncTask = new EletraMCIWriteAsyncTask(
                AlarmClearingActivity.this,
                dlmsCommand,
                (error)-> {
                    Log.i(AlarmSettingsActivity.class.getSimpleName(), "Escrita terminada...");
                    Log.i(AlarmSettingsActivity.class.getSimpleName(), "Erro: " + String.valueOf(error));
                    if(error > 0) {
                        textView.setText(error);
                        if (error == R.string.exception_generic) {
                            textView.setText(R.string.exception_generic_alarm_clearing);
                        }
                        this.showButtonErro(this.isHightLevel());
                        sendButton.setEnabled(false);
                        editText.setEnabled(false);
                        checkBox.setEnabled(false);
                        relativeLayoutError.setVisibility(View.VISIBLE);
                        linearLayout.setVisibility(View.GONE);
                        if (this.progressWrite != null) this.progressWrite.dismiss();

                    } else {
                        if (this.progressWrite != null) this.progressWrite.dismiss();
                        sendButton.setEnabled(true);
                        checkBox.setEnabled(true);
                        if(checkBox.isChecked()) {
                            editText.setEnabled(false);
                        } else{
                            editText.setEnabled(true);
                        }
                        Snackbar.make(toolbar, getString(R.string.success_action), Snackbar.LENGTH_SHORT).show();
                    }
                    /*
                    try {
                        meterComm.disconnect();
                    } catch (ComPortException e) {
                        Log.e(CheckpointReadingActivity.class.getName(), e.getMessage());
                    } catch (MeterPhyConnectionException e) {
                        Log.e(CheckpointReadingActivity.class.getName(), e.getMessage());
                    } catch (Exception e) {
                        Log.e(CheckpointReadingActivity.class.getName(), CheckpointReadingActivity.this.getString(R.string.exception_generic));
                    }
                     */
                },
                task -> {
                    if(this.isHightLevel() > 0) {
                        this.preWrite(task);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(AlarmClearingActivity.this);
                        LayoutInflater inflater = LayoutInflater.from(AlarmClearingActivity.this);
                        View view = inflater.inflate(R.layout.dialog_progress, null, false);
                        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBarProgress);
                        TextView textView = (TextView) view.findViewById(R.id.textViewProgress);
                        textView.setText(R.string.processing_alarm_clearing);
                        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(AlarmClearingActivity.this);
                        circularProgressDrawable.setStyle(CircularProgressDrawable.LARGE);
                        circularProgressDrawable.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
                        progressBar.setIndeterminateDrawable(circularProgressDrawable);
                        builder.setView(view);
                        builder.setCancelable(false);
                        this.progressWrite = builder.create();
                        this.progressWrite.show();
                    }
                }
        );
        this.eletraMCIWriteAsyncTask.execute();
    }

    protected void preWrite(AsyncTask task) {
        task.cancel(true);
        textView.setText(this.isHightLevel());
        this.showButtonErro(this.isHightLevel());
        linearLayout.setVisibility(View.GONE);
        relativeLayoutError.setVisibility(View.VISIBLE);
    }
}