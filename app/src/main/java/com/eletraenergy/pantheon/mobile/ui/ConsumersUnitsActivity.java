package com.eletraenergy.pantheon.mobile.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadAsyncTask;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadEnum;
import com.eletraenergy.pantheon.mobile.ui.adapters.ConsumersUnitsRecyclerViewAdapter;
import com.google.android.material.appbar.AppBarLayout;
import com.google.common.base.Strings;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.eletra.mci.dlms.DLMSAtt;

public class ConsumersUnitsActivity extends PantheonCompatActivity {

    protected Toolbar toolbar;
    protected EletraMCIReadAsyncTask eletraMCIReadAsyncTask;
    protected RecyclerView recyclerView;
    protected RelativeLayout relativeLayoutLoad;
    protected ProgressBar progressBar;
    protected RelativeLayout relativeLayoutError;
    protected TextView textView;
    protected SwipeRefreshLayout swipeRefreshLayout;
    protected Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consumers_units);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);

        progressBar = (ProgressBar) findViewById(R.id.progressBarConsumersUnits);
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(ConsumersUnitsActivity.this);
        circularProgressDrawable.setStyle(CircularProgressDrawable.LARGE);
        circularProgressDrawable.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        progressBar.setIndeterminateDrawable(circularProgressDrawable);

        relativeLayoutLoad = (RelativeLayout) findViewById(R.id.relativeLayoutLoadConsumersUnits);
        relativeLayoutError = (RelativeLayout) findViewById(R.id.relativeLayoutErrorConsumersUnits);
        textView = (TextView) findViewById(R.id.textViewConsumersUnits);
        button = (Button) findViewById(R.id.buttonConsumersUnits);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshConsumersUnits);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewConsumersUnits);
        LinearLayoutManager layoutManager = new LinearLayoutManager(ConsumersUnitsActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(false);
        swipeRefreshLayout.setOnRefreshListener(() -> ConsumersUnitsActivity.this.loadConsumersUnits());
    }

    @Override
    protected void onResume() {
        super.onResume();
        relativeLayoutError.setVisibility(View.GONE);
        swipeRefreshLayout.setVisibility(View.GONE);
        relativeLayoutLoad.setVisibility(View.VISIBLE);
        this.loadConsumersUnits();
    }

    protected void loadConsumersUnits() {
        if (eletraMCIReadAsyncTask != null) eletraMCIReadAsyncTask.cancel(true);
        this.eletraMCIReadAsyncTask = new EletraMCIReadAsyncTask(
                ConsumersUnitsActivity.this,
                EletraMCIReadEnum.CONSUMER_UNIT,
                (attrs, error)-> {
                    if(error > 0) {
                        textView.setText(error);
                        if(error == R.string.exception_generic) {
                            textView.setText(R.string.exception_generic_units);
                        }
                        this.showButtonErro(this.isHightLevel());
                        relativeLayoutLoad.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        swipeRefreshLayout.setVisibility(View.GONE);
                        relativeLayoutError.setVisibility(View.VISIBLE);
                    } else {
                        this.postRead(attrs);
                    }
                },
                task -> {
                    if(this.isHightLevel() > 0) {
                        this.preRead(task);
                    }
                }
        );
        this.eletraMCIReadAsyncTask.execute();
    }

    protected void showButtonErro(int err) {
        if(err == R.string.error_level) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(ConsumersUnitsActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_auth) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(ConsumersUnitsActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_encrypt) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(ConsumersUnitsActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_security) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(ConsumersUnitsActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_ip) {
            button.setText(R.string.bt_communication_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(ConsumersUnitsActivity.this, CommunicationSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_port) {
            button.setText(R.string.bt_communication_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(ConsumersUnitsActivity.this, CommunicationSettingActivity.class));
            });
            return;
        }
        button.setText(R.string.bt_retry);
        button.setVisibility(View.VISIBLE);
        button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_refresh_black_24dp), null, null, null);
        button.setOnClickListener(v -> {
            relativeLayoutError.setVisibility(View.GONE);
            relativeLayoutLoad.setVisibility(View.VISIBLE);
            this.loadConsumersUnits();
        });
        return;
    }

    @Override
    public boolean onSupportNavigateUp(){
        this.onBackPressed();
        return true;
    }

    @Override
    public void onDestroy() {
        if (eletraMCIReadAsyncTask != null) eletraMCIReadAsyncTask.cancel(true);
        super.onDestroy();
    }

    protected void postRead(List<DLMSAtt> attributes) {
        HashMap<Integer, String> phaseMap = new HashMap<Integer, String>();
        phaseMap.put(1, "A");
        phaseMap.put(2, "B");
        phaseMap.put(3, "C");

        phaseMap.put(4, "A");
        phaseMap.put(5, "B");
        phaseMap.put(6, "C");

        phaseMap.put(7, "A");
        phaseMap.put(8, "B");
        phaseMap.put(9, "C");

        phaseMap.put(10, "A");
        phaseMap.put(11, "B");
        phaseMap.put(12, "C");

        ArrayList<ConsumersUnitsRecyclerViewAdapter.ConsumerUnitItem> itens = new ArrayList<ConsumersUnitsRecyclerViewAdapter.ConsumerUnitItem>();
        ConsumersUnitsRecyclerViewAdapter.ConsumerUnitItem it;

        for(DLMSAtt attribute : attributes) {

            if(Strings.isNullOrEmpty(attribute.getAtt().get(0).getValueProperty())) continue;
            it = new ConsumersUnitsRecyclerViewAdapter.ConsumerUnitItem();
            it.number = attribute.getAtt().get(0).getValueProperty();

            switch (Integer.parseInt(attribute.getAtt().get(2).getValueProperty())) {
                case 0:
                    it.type = ConsumersUnitsActivity.this.getString(R.string.single_phase);
                    break;
                case 1:
                    it.type = ConsumersUnitsActivity.this.getString(R.string.two_phase);
                    break;
                case 2:
                    it.type = ConsumersUnitsActivity.this.getString(R.string.three_phase);
                    break;
                default:
                    it.type = "";
            }

            double a1 = 0;
            try {
                a1 = Double.parseDouble(attribute.getAtt().get(3).getValueProperty());
            } catch (Exception e) {
                a1 = 0;
            }
            double a2 = 0;
            try {
                a2 = Double.parseDouble(attribute.getAtt().get(4).getValueProperty());
            } catch (Exception e) {
                a2 = 0;
            }

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            int digitsDecimal = Integer.parseInt(prefs.getString(getString(R.string.key_decimal_digits), "2"));
            int digitsInteger = Integer.parseInt(prefs.getString(getString(R.string.key_integer_digits), "2"));

            String sDirect = String.valueOf(new DecimalFormat("#.00").format(a1)).replace(".", ",");
            String sReverse = String.valueOf(new DecimalFormat("#.00").format(a2)).replace(".", ",");

            String[] dir = sDirect.split(",");
            String[] rev = sReverse.split(",");

            String intDir = Strings.padStart(dir[0], digitsInteger, '0');
            String intRev = Strings.padStart(rev[0], digitsInteger, '0');

            if(digitsDecimal == 1) {
                intDir += "," + dir[1].substring(0, dir[1].length() - 1);
                intRev += "," + rev[1].substring(0, dir[1].length() - 1);
            }
            if(digitsDecimal == 2) {
                intDir += "," + dir[1];
                intRev += "," + rev[1];
            }

            it.direct = "+A " + intDir + " kWh";
            it.reverse = "-A " +  intRev + " kWh";
            Log.i(ConsumersUnitsActivity.class.getSimpleName(), it.direct);
            Log.i(ConsumersUnitsActivity.class.getSimpleName(), it.reverse);

            it.voltage = new String[3];
            it.current = new String[3];
            it.power = new String[3];
            it.phase = new String[3];
            it.relay = new Integer[3];
            it.pos = new String[3];
            it.postRelay = new String[3];
            it.fails = new String[3];
            it.reverseCurrent = new String[3];
            it.timeout = new String[3];

            //magnitudes -> 9
            List<DLMSAtt> subAttributes = attribute.getAtt().get(9).getAtt();

            for(int cont = 0; cont <  subAttributes.size() ; cont ++) {
                it.pos[cont] = String.valueOf(Integer.parseInt(attribute.getAtt().get(1).getValueProperty()) + cont);
                it.phase[cont] = phaseMap.get(Integer.parseInt(attribute.getAtt().get(1).getValueProperty()) + cont);
                it.voltage[cont] = subAttributes.get(cont).getAtt().get(0).getValueProperty() + "V";

                double a3 = Double.parseDouble(subAttributes.get(cont).getAtt().get(1).getValueProperty());
                String vl1 = Strings.padStart(String.valueOf(new DecimalFormat("#.00").format(a3)).replace(".", ","), 4, '0');
                it.current[cont] = Strings.padStart(vl1, 4, '0') + "A";

                double a4 = Double.parseDouble(subAttributes.get(cont).getAtt().get(2).getValueProperty());
                String vl2 = Strings.padStart(String.valueOf(new DecimalFormat("#.00").format(a4)).replace(".", ","), 4, '0');
                it.power[cont] = Strings.padStart(vl2, 4, '0') + "W";
                int relayStatus = Integer.parseInt(subAttributes.get(cont).getAtt().get(3).getValueProperty());

                if ((relayStatus & 0x01) == 0x01) {
                    // reading failed
                    it.relay[cont] = R.drawable.relay_warning;
                    it.postRelay[cont] = ConsumersUnitsActivity.this.getString(R.string.no);
                } else if ((relayStatus & 0x04) == 0x00) {
                    // relay error
                    it.relay[cont] = R.drawable.relay_open_warning_volt_v7;
                    it.postRelay[cont] = ConsumersUnitsActivity.this.getString(R.string.yes);
                } else if ((relayStatus & 0x02) == 0x00) {
                    // closed relay
                    it.relay[cont] = R.drawable.relay_closed;
                    it.postRelay[cont] = ConsumersUnitsActivity.this.getString(R.string.no);
                } else {
                    // value = 6
                    // open relay
                    it.relay[cont] = R.drawable.relay_open;
                    it.postRelay[cont] = ConsumersUnitsActivity.this.getString(R.string.no);
                }

                if(subAttributes.get(cont).getAtt().get(4).getValueProperty() == null ||
                        subAttributes.get(cont).getAtt().get(4).getValueProperty().isEmpty())   subAttributes.get(cont).getAtt().get(4).setValueProperty("0");

                int decFlags = Integer.parseInt(subAttributes.get(cont).getAtt().get(4).getValueProperty());
                String binStrVal = Integer.toBinaryString(decFlags);

                while(binStrVal.length() < 5) binStrVal = "0"+binStrVal;

                it.fails[cont] = getResult(binStrVal, 2, 5);
                it.reverseCurrent[cont] = getResult(binStrVal, 1, 2);
                it.timeout[cont] = getResult(binStrVal, 0, 1);

            }
            it.isEnable = true;
            itens.add(it);
        }
        ConsumersUnitsRecyclerViewAdapter adapter = new ConsumersUnitsRecyclerViewAdapter((AppCompatActivity) ConsumersUnitsActivity.this, itens);
        recyclerView.setAdapter(adapter);
        relativeLayoutError.setVisibility(View.GONE);
        relativeLayoutLoad.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setVisibility(View.VISIBLE);
    }

    private String getResult(String binStr, int begin, int end) {
        return binStr.subSequence(begin, end).toString().contains("1") ? ConsumersUnitsActivity.this.getString(R.string.yes) : ConsumersUnitsActivity.this.getString(R.string.no);
    }

    protected void preRead(AsyncTask task) {
        task.cancel(true);
        textView.setText(this.isHightLevel());
        this.showButtonErro(this.isHightLevel());
        relativeLayoutLoad.setVisibility(View.GONE);
        swipeRefreshLayout.setVisibility(View.GONE);
        relativeLayoutError.setVisibility(View.VISIBLE);
    }
}