package com.eletraenergy.pantheon.mobile.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadTitle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dcsda on 16/01/2018.
 */

public class ConsumersUnitsRecyclerViewAdapter extends RecyclerView.Adapter<ConsumersUnitsRecyclerViewAdapter.ViewHolder> {

    protected List<ConsumerUnitItem> itens;
    protected AppCompatActivity context;
    protected EletraMCIReadTitle eletraMCIReadTitle;
    HashMap<Integer, String> phaseMap = new HashMap<Integer, String>();

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView numberTextViewHolder;
        public TextView typeTextViewHolder;
        public TextView posTextViewHolder;
        public TextView directTextViewHolder;
        public TextView reverseTextViewHolder;
        public TextView voltage1TextViewHolder;
        public TextView voltage2TextViewHolder;
        public TextView voltage3TextViewHolder;
        public TextView current1TextViewHolder;
        public TextView current2TextViewHolder;
        public TextView current3TextViewHolder;
        public TextView power1TextViewHolder;
        public TextView power2TextViewHolder;
        public TextView power3TextViewHolder;
        public TextView phase1TextViewHolder;
        public TextView phase2TextViewHolder;
        public TextView phase3TextViewHolder;
        public TextView pos1TextViewHolder;
        public TextView pos2TextViewHolder;
        public TextView pos3TextViewHolder;
        public TextView pos1PostRelayHolder;
        public TextView pos2PostRelayHolder;
        public TextView pos3PostRelayHolder;
        public TextView pos1FailFlagHolder;
        public TextView pos2FailFlagHolder;
        public TextView pos3FailFlagHolder;
        public TextView pos1ReverseCurrentHolder;
        public TextView pos2ReverseCurrentHolder;
        public TextView pos3ReverseCurrentHolder;
        public TextView pos1TimeoutHolder;
        public TextView pos2TimeoutHolder;
        public TextView pos3TimeoutHolder;
        public LinearLayout layout1TextViewHolder;
        public LinearLayout layout2TextViewHolder;
        public LinearLayout layout3TextViewHolder;
        public ImageView relayImageViewHolder;
        public ImageView relay1ImageViewHolder;
        public ImageView relay2ImageViewHolder;
        public ImageView relay3ImageViewHolder;
        public ImageButton imageButtonHolder;
        public LinearLayout detailLinearLayoutHolder;
        public LinearLayout enableLinearLayoutHolder;
        public LinearLayout disableLinearLayoutHolder;
        public TextView disPosTextViewHolder;
        public TextView disPhaseTextViewHolder;
        public ImageView imageViewViewHolder;

        public ViewHolder(View view) {
            super(view);

            imageButtonHolder = (ImageButton) view.findViewById(R.id.imageButtonItemConsumersUnits);
            detailLinearLayoutHolder = (LinearLayout) view.findViewById(R.id.detailLinearLayoutItemConsumersUnits);

            disableLinearLayoutHolder = (LinearLayout) view.findViewById(R.id.disablePositionLinearLayout);
            enableLinearLayoutHolder = (LinearLayout) view.findViewById(R.id.enablePositionLinearLayout);
            imageViewViewHolder = (ImageView) view.findViewById(R.id.imageViewItemConsumersUnits);

            imageButtonHolder.setOnClickListener(v -> {
                ConsumerUnitItem it = ConsumersUnitsRecyclerViewAdapter.this.itens.get(getAdapterPosition());
                if (!it.isOpen) {
                    imageButtonHolder.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                    detailLinearLayoutHolder.setVisibility(View.VISIBLE);
                    ConsumersUnitsRecyclerViewAdapter.this.itens.get(getAdapterPosition()).isOpen = true;
                } else {
                    imageButtonHolder.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                    detailLinearLayoutHolder.setVisibility(View.GONE);
                    ConsumersUnitsRecyclerViewAdapter.this.itens.get(getAdapterPosition()).isOpen = false;
                }
            });

            disPosTextViewHolder = (TextView) view.findViewById(R.id.posDisTextViewItemConsumersUnits);
            disPhaseTextViewHolder = (TextView) view.findViewById(R.id.phaseDisTextViewItemConsumersUnits);

            numberTextViewHolder = (TextView) view.findViewById(R.id.numberTextViewItemConsumersUnits);
            typeTextViewHolder = (TextView) view.findViewById(R.id.typeTextViewItemConsumersUnits);
            posTextViewHolder = (TextView) view.findViewById(R.id.posTextViewItemConsumersUnits);
            directTextViewHolder = (TextView) view.findViewById(R.id.directTextViewItemConsumersUnits);
            reverseTextViewHolder = (TextView) view.findViewById(R.id.reverseTextViewItemConsumersUnits);

            voltage1TextViewHolder = (TextView) view.findViewById(R.id.voltage1TextViewItemConsumersUnits);
            voltage2TextViewHolder = (TextView) view.findViewById(R.id.voltage2TextViewItemConsumersUnits);
            voltage3TextViewHolder = (TextView) view.findViewById(R.id.voltage3TextViewItemConsumersUnits);

            current1TextViewHolder = (TextView) view.findViewById(R.id.current1TextViewItemConsumersUnits);
            current2TextViewHolder = (TextView) view.findViewById(R.id.current2TextViewItemConsumersUnits);
            current3TextViewHolder = (TextView) view.findViewById(R.id.current3TextViewItemConsumersUnits);

            power1TextViewHolder = (TextView) view.findViewById(R.id.power1TextViewItemConsumersUnits);
            power2TextViewHolder = (TextView) view.findViewById(R.id.power2TextViewItemConsumersUnits);
            power3TextViewHolder = (TextView) view.findViewById(R.id.power3TextViewItemConsumersUnits);

            phase1TextViewHolder = (TextView) view.findViewById(R.id.phase1TextViewItemConsumersUnits);
            phase2TextViewHolder = (TextView) view.findViewById(R.id.phase2TextViewItemConsumersUnits);
            phase3TextViewHolder = (TextView) view.findViewById(R.id.phase3TextViewItemConsumersUnits);

            pos1TextViewHolder = (TextView) view.findViewById(R.id.pos1TextViewItemConsumersUnits);
            pos2TextViewHolder = (TextView) view.findViewById(R.id.pos2TextViewItemConsumersUnits);
            pos3TextViewHolder = (TextView) view.findViewById(R.id.pos3TextViewItemConsumersUnits);

            pos1PostRelayHolder = (TextView) view.findViewById(R.id.pos1PostRelay);
            pos2PostRelayHolder = (TextView) view.findViewById(R.id.pos2PostRelay);
            pos3PostRelayHolder = (TextView) view.findViewById(R.id.pos3PostRelay);

            pos1FailFlagHolder = (TextView) view.findViewById(R.id.pos1FailFlag);
            pos2FailFlagHolder = (TextView) view.findViewById(R.id.pos2FailFlag);
            pos3FailFlagHolder = (TextView) view.findViewById(R.id.pos3FailFlag);

            pos1ReverseCurrentHolder = (TextView) view.findViewById(R.id.pos1ReverseCurrent);
            pos2ReverseCurrentHolder = (TextView) view.findViewById(R.id.pos2ReverseCurrent);
            pos3ReverseCurrentHolder = (TextView) view.findViewById(R.id.pos3ReverseCurrent);

            pos1TimeoutHolder = (TextView) view.findViewById(R.id.pos1Timeout);
            pos2TimeoutHolder = (TextView) view.findViewById(R.id.pos2Timeout);
            pos3TimeoutHolder = (TextView) view.findViewById(R.id.pos3Timeout);

            layout1TextViewHolder = (LinearLayout) view.findViewById(R.id.layout1LinearLayoutItemConsumersUnits);
            layout2TextViewHolder = (LinearLayout) view.findViewById(R.id.layout2LinearLayoutItemConsumersUnits);
            layout3TextViewHolder = (LinearLayout) view.findViewById(R.id.layout3LinearLayoutItemConsumersUnits);

            relayImageViewHolder = (ImageView) view.findViewById(R.id.relayImageViewItemConsumersUnits);

            relay1ImageViewHolder = (ImageView) view.findViewById(R.id.relay1ImageViewItemConsumersUnits);
            relay2ImageViewHolder = (ImageView) view.findViewById(R.id.relay2ImageViewItemConsumersUnits);
            relay3ImageViewHolder = (ImageView) view.findViewById(R.id.relay3ImageViewItemConsumersUnits);
        }
    }

    public ConsumersUnitsRecyclerViewAdapter(AppCompatActivity context, List<ConsumerUnitItem> itensEnable) {
        this.itens = new ArrayList<ConsumerUnitItem>();
        int pos = 1;
        boolean add = true;
        while (pos < 13) {
            add = true;
            for(ConsumerUnitItem unt: itensEnable) {
                if(Integer.parseInt(unt.pos[0]) == pos) {
                    this.itens.add(unt);
                    add = false;
                }
                if (unt.pos[1] != null &&
                        Integer.parseInt(unt.pos[1]) == pos) {
                    add = false;
                }
                if(unt.pos[2] != null &&
                        Integer.parseInt(unt.pos[2]) == pos) {
                    add = false;
                }
            }
            if(add) {
                ConsumerUnitItem it = new ConsumerUnitItem();
                it.pos = new String[1];
                it.pos[0] = String.valueOf(pos);
                it.isEnable = false;
                this.itens.add(it);
            }
            pos++;
        }
        this.context = context;
        this.eletraMCIReadTitle = new EletraMCIReadTitle();

        phaseMap.put(1, "A");
        phaseMap.put(2, "B");
        phaseMap.put(3, "C");

        phaseMap.put(4, "A");
        phaseMap.put(5, "B");
        phaseMap.put(6, "C");

        phaseMap.put(7, "A");
        phaseMap.put(8, "B");
        phaseMap.put(9, "C");

        phaseMap.put(10, "A");
        phaseMap.put(11, "B");
        phaseMap.put(12, "C");
    }

    @Override
    public ConsumersUnitsRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_consumers_units, parent, false);
        ConsumersUnitsRecyclerViewAdapter.ViewHolder vh = new ConsumersUnitsRecyclerViewAdapter.ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ConsumersUnitsRecyclerViewAdapter.ViewHolder holder, int position) {
        ConsumerUnitItem it = this.itens.get(position);
        if(it.isEnable) {

            holder.numberTextViewHolder.setText(it.number);
            holder.typeTextViewHolder.setText(it.type);
            // holder.posTextViewHolder.setText(TextUtils.join(", ", it.pos).replace(", null", ""));
            holder.posTextViewHolder.setText(it.pos[0]);
            holder.directTextViewHolder.setText(it.direct);
            holder.reverseTextViewHolder.setText(it.reverse);

            Integer imgDef = it.relay[0];
            for(Integer img : it.relay) {
                if(img != null && !img.equals(imgDef)) {
                    imgDef = R.drawable.relay_warning;
                }
            }
            holder.relayImageViewHolder.setImageResource(imgDef);

            holder.voltage1TextViewHolder.setVisibility(View.GONE);
            holder.voltage2TextViewHolder.setVisibility(View.GONE);
            holder.voltage3TextViewHolder.setVisibility(View.GONE);

            holder.current1TextViewHolder.setVisibility(View.GONE);
            holder.current2TextViewHolder.setVisibility(View.GONE);
            holder.current3TextViewHolder.setVisibility(View.GONE);

            holder.power1TextViewHolder.setVisibility(View.GONE);
            holder.power2TextViewHolder.setVisibility(View.GONE);
            holder.power3TextViewHolder.setVisibility(View.GONE);

            holder.phase1TextViewHolder.setVisibility(View.GONE);
            holder.phase2TextViewHolder.setVisibility(View.GONE);
            holder.phase3TextViewHolder.setVisibility(View.GONE);

            holder.pos1TextViewHolder.setVisibility(View.GONE);
            holder.pos2TextViewHolder.setVisibility(View.GONE);
            holder.pos3TextViewHolder.setVisibility(View.GONE);

            holder.layout1TextViewHolder.setVisibility(View.GONE);
            holder.layout2TextViewHolder.setVisibility(View.GONE);
            holder.layout3TextViewHolder.setVisibility(View.GONE);

            holder.relay1ImageViewHolder.setVisibility(View.GONE);
            holder.relay2ImageViewHolder.setVisibility(View.GONE);
            holder.relay3ImageViewHolder.setVisibility(View.GONE);

            holder.pos1PostRelayHolder.setVisibility(View.GONE);
            holder.pos2PostRelayHolder.setVisibility(View.GONE);
            holder.pos3PostRelayHolder.setVisibility(View.GONE);

            holder.pos1FailFlagHolder.setVisibility(View.GONE);
            holder.pos2FailFlagHolder.setVisibility(View.GONE);
            holder.pos3FailFlagHolder.setVisibility(View.GONE);

            holder.pos1ReverseCurrentHolder.setVisibility(View.GONE);
            holder.pos2ReverseCurrentHolder.setVisibility(View.GONE);
            holder.pos3ReverseCurrentHolder.setVisibility(View.GONE);

            holder.pos1TimeoutHolder.setVisibility(View.GONE);
            holder.pos2TimeoutHolder.setVisibility(View.GONE);
            holder.pos3TimeoutHolder.setVisibility(View.GONE);

            if (it.voltage[0] != null) {
                holder.voltage1TextViewHolder.setText(it.voltage[0]);
                holder.current1TextViewHolder.setText(it.current[0]);
                holder.power1TextViewHolder.setText(it.power[0]);
                holder.phase1TextViewHolder.setText(it.phase[0]);
                holder.phase1TextViewHolder.setText(it.phase[0]);
                holder.pos1TextViewHolder.setText(it.pos[0]);
                holder.relay1ImageViewHolder.setImageResource(it.relay[0]);
                holder.pos1PostRelayHolder.setText(it.postRelay[0]);
                holder.pos1FailFlagHolder.setText(it.fails[0]);
                holder.pos1ReverseCurrentHolder.setText(it.reverseCurrent[0]);
                holder.pos1TimeoutHolder.setText(it.timeout[0]);

                holder.voltage1TextViewHolder.setVisibility(View.VISIBLE);
                holder.current1TextViewHolder.setVisibility(View.VISIBLE);
                holder.power1TextViewHolder.setVisibility(View.VISIBLE);
                holder.phase1TextViewHolder.setVisibility(View.VISIBLE);
                holder.pos1TextViewHolder.setVisibility(View.VISIBLE);
                holder.layout1TextViewHolder.setVisibility(View.VISIBLE);
                holder.relay1ImageViewHolder.setVisibility(View.VISIBLE);
                holder.imageViewViewHolder.setImageResource(R.drawable.singlephase);
                holder.pos1PostRelayHolder.setVisibility(View.VISIBLE);
                holder.pos1FailFlagHolder.setVisibility(View.VISIBLE);
                holder.pos1ReverseCurrentHolder.setVisibility(View.VISIBLE);
                holder.pos1TimeoutHolder.setVisibility(View.VISIBLE);
            }

            if (it.voltage[1] != null) {
                holder.voltage2TextViewHolder.setText(it.voltage[1]);
                holder.current2TextViewHolder.setText(it.current[1]);
                holder.power2TextViewHolder.setText(it.power[1]);
                holder.phase2TextViewHolder.setText(it.phase[1]);
                holder.pos2TextViewHolder.setText(it.pos[1]);
                holder.relay2ImageViewHolder.setImageResource(it.relay[1]);
                holder.pos2PostRelayHolder.setText(it.postRelay[1]);
                holder.pos2FailFlagHolder.setText(it.fails[1]);
                holder.pos2ReverseCurrentHolder.setText(it.reverseCurrent[1]);
                holder.pos2TimeoutHolder.setText(it.timeout[1]);

                holder.voltage2TextViewHolder.setVisibility(View.VISIBLE);
                holder.current2TextViewHolder.setVisibility(View.VISIBLE);
                holder.power2TextViewHolder.setVisibility(View.VISIBLE);
                holder.phase2TextViewHolder.setVisibility(View.VISIBLE);
                holder.pos2TextViewHolder.setVisibility(View.VISIBLE);
                holder.layout2TextViewHolder.setVisibility(View.VISIBLE);
                holder.relay2ImageViewHolder.setVisibility(View.VISIBLE);
                holder.pos2PostRelayHolder.setVisibility(View.VISIBLE);
                holder.pos2FailFlagHolder.setVisibility(View.VISIBLE);
                holder.pos2ReverseCurrentHolder.setVisibility(View.VISIBLE);
                holder.pos2TimeoutHolder.setVisibility(View.VISIBLE);

                holder.imageViewViewHolder.setImageResource(R.drawable.biphase);
            }

            if (it.voltage[2] != null) {
                holder.voltage3TextViewHolder.setText(it.voltage[2]);
                holder.current3TextViewHolder.setText(it.current[2]);
                holder.power3TextViewHolder.setText(it.power[2]);
                holder.phase3TextViewHolder.setText(it.phase[2]);
                holder.pos3TextViewHolder.setText(it.pos[2]);
                holder.relay3ImageViewHolder.setImageResource(it.relay[2]);
                holder.pos3PostRelayHolder.setText(it.postRelay[2]);
                holder.pos3FailFlagHolder.setText(it.fails[2]);
                holder.pos3ReverseCurrentHolder.setText(it.reverseCurrent[2]);
                holder.pos3TimeoutHolder.setText(it.timeout[2]);

                holder.voltage3TextViewHolder.setVisibility(View.VISIBLE);
                holder.current3TextViewHolder.setVisibility(View.VISIBLE);
                holder.power3TextViewHolder.setVisibility(View.VISIBLE);
                holder.phase3TextViewHolder.setVisibility(View.VISIBLE);
                holder.pos3TextViewHolder.setVisibility(View.VISIBLE);
                holder.layout3TextViewHolder.setVisibility(View.VISIBLE);
                holder.relay3ImageViewHolder.setVisibility(View.VISIBLE);
                holder.pos3PostRelayHolder.setVisibility(View.VISIBLE);
                holder.pos3FailFlagHolder.setVisibility(View.VISIBLE);
                holder.pos3ReverseCurrentHolder.setVisibility(View.VISIBLE);
                holder.pos3TimeoutHolder.setVisibility(View.VISIBLE);

                holder.imageViewViewHolder.setImageResource(R.drawable.threephase);
            }
            holder.disableLinearLayoutHolder.setVisibility(View.GONE);
            holder.enableLinearLayoutHolder.setVisibility(View.VISIBLE);
        } else {
            holder.enableLinearLayoutHolder.setVisibility(View.GONE);
            holder.disableLinearLayoutHolder.setVisibility(View.VISIBLE);

            holder.disPosTextViewHolder.setText(context.getString(R.string.attr_meter_pos) + " " +  it.pos[0]);
            holder.disPhaseTextViewHolder.setText(context.getString(R.string.attr_phase) + " " + phaseMap.get(Integer.parseInt(it.pos[0])));
        }
    }

    @Override
    public int getItemCount() {
        return itens.size();
    }

    public static class ConsumerUnitItem {
        public String number;
        public String type;
        public String[] pos;
        public String direct;
        public String reverse;
        public String[] voltage;
        public String[] current;
        public String[] power;
        public String[] phase;
        public Integer[] relay;
        public String[] postRelay;
        public String[] fails;
        public String[] reverseCurrent;
        public String[] timeout;
        public boolean isOpen = true;
        public boolean isEnable = false;
    }
}