package com.eletraenergy.pantheon.mobile.ui;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.WifiConnectedService;
import com.eletraenergy.pantheon.mobile.ui.adapters.NetworksAvailableRecyclerViewAdapter;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

public class NetworksAvailableActivity extends PantheonCompatActivity {

    protected Toolbar toolbar;
    protected RelativeLayout relativeLayoutLoad;
    protected RelativeLayout relativeLayoutError;
    protected SwipeRefreshLayout swipeRefreshLayout;
    protected RecyclerView recyclerView;
    protected ProgressBar progressBar;
    protected TextView textView;
    protected Button button;

    private final int NETWORK_REQUEST_CODE = 1;
    private BroadcastReceiver receiverScan;
    private BroadcastReceiver receiverUpdate;
    private WifiManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_available);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);

        progressBar = (ProgressBar) findViewById(R.id.progressBarNetworksAvailable);
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(NetworksAvailableActivity.this);
        circularProgressDrawable.setStyle(CircularProgressDrawable.LARGE);
        circularProgressDrawable.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        progressBar.setIndeterminateDrawable(circularProgressDrawable);

        relativeLayoutLoad = (RelativeLayout) findViewById(R.id.relativeLayoutLoadNetworksAvailable);
        relativeLayoutError = (RelativeLayout) findViewById(R.id.relativeLayoutErrorNetworksAvailable);

        textView = (TextView) findViewById(R.id.textViewNetworksAvailable);
        button = (Button) findViewById(R.id.buttonNetworksAvailable);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshNetworksAvailable);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewNetworksAvailable);
        LinearLayoutManager layoutManager = new LinearLayoutManager(NetworksAvailableActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        manager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        receiverScan = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i(MainActivity.class.getSimpleName(), "Atualizando intensidade das conexões disponíveis...");
                updateNetworkList();
            }
        };
        receiverUpdate = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i(MainActivity.class.getSimpleName(), "Atualizando conexão de rede...");
                scanNetwork();
            }
        };
        swipeRefreshLayout.setOnRefreshListener(() -> NetworksAvailableActivity.this.scanNetwork());
    }

    private void scanNetwork(){
        if(isPermissionNetworkGranted()){
            if (manager.isWifiEnabled() == false) {
                if(manager.setWifiEnabled(true)) {
                    manager.startScan();
                    textView.setText("Wifi desabilitada");
                    showButtonErro(1);
                    recyclerView.setAdapter(null);
                    relativeLayoutLoad.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                    swipeRefreshLayout.setVisibility(View.GONE);
                    relativeLayoutError.setVisibility(View.VISIBLE);
                }
            } else {
                if(recyclerView.getAdapter() == null) {
                    relativeLayoutLoad.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setRefreshing(false);
                    swipeRefreshLayout.setVisibility(View.GONE);
                    relativeLayoutError.setVisibility(View.GONE);
                }
                manager.startScan();
            }
        }
    }

    protected void showButtonErro(int err) {
        button.setText(R.string.bt_retry);
        button.setVisibility(View.VISIBLE);
        button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_refresh_black_24dp), null, null, null);
        button.setOnClickListener(v -> {
            relativeLayoutError.setVisibility(View.GONE);
            relativeLayoutLoad.setVisibility(View.VISIBLE);
            this.scanNetwork();
        });
        return;
    }

    private void updateNetworkList(){
        List<ScanResult> mScanResults = manager.getScanResults();
        String connectedNetwork = "";
        if (isLocationEnabled()) {
            WifiInfo wifiInfo = manager.getConnectionInfo();
            if (wifiInfo != null) {
                NetworkInfo.DetailedState state = WifiInfo.getDetailedStateOf(wifiInfo.getSupplicantState());
                if (state == NetworkInfo.DetailedState.CONNECTED || state == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
                    connectedNetwork = wifiInfo.getSSID().replaceAll("^\"|\"$", "");
                }
            } else {
                Snackbar.make(button, "Não foi possível obter as informações da rede", Snackbar.LENGTH_LONG).show();
            }
        } else {
            //Snackbar.make(toolbarBottom, "Wifi Desabilitado", Snackbar.LENGTH_SHORT).show();
            textView.setText("GPS desabilitado.");
            showButtonErro(1);
            relativeLayoutLoad.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            swipeRefreshLayout.setVisibility(View.GONE);
            relativeLayoutError.setVisibility(View.VISIBLE);
            return;
        }
        NetworksAvailableRecyclerViewAdapter.NetworkItem networkItem = null;
        NetworksAvailableRecyclerViewAdapter.NetworkItem connectedItem = null;
        HashMap<String, NetworksAvailableRecyclerViewAdapter.NetworkItem> maps = new HashMap<>();
        for(ScanResult scanResult: mScanResults){
            Log.i(NetworksAvailableActivity.class.getSimpleName(), scanResult.SSID.replaceAll("^\"|\"$", "")  + " - " + scanResult.level);
            networkItem = new NetworksAvailableRecyclerViewAdapter.NetworkItem();
            networkItem.title = scanResult.SSID.replaceAll("^\"|\"$", "");
            networkItem.subtitle = scanResult.BSSID;
            networkItem.strength = scanResult.level;
            if(networkItem.title.equals(connectedNetwork)) {
                networkItem.connected = true;
                connectedItem = networkItem;
            } else {
                networkItem.connected = false;
            }
            if(maps.containsKey(networkItem.title)) {
                NetworksAvailableRecyclerViewAdapter.NetworkItem it = maps.get(networkItem.title);
                if(it.strength < networkItem.strength) maps.put(networkItem.title, networkItem);
            } else {
                maps.put(networkItem.title, networkItem);
            }
        }
        if(connectedItem != null) maps.remove(connectedItem.title);

        ArrayList<NetworksAvailableRecyclerViewAdapter.NetworkItem> itens = new ArrayList<>();
        if(connectedItem != null) itens.add(connectedItem);
        SortedSet<String> keys = new TreeSet<>(maps.keySet());
        for (String key : keys) {
            if(maps.get(key).title.toLowerCase().indexOf("pantheon") > -1) {
                itens.add(maps.get(key));
            }
        }
        if(itens.size() > 0) {
            recyclerView.setAdapter(new NetworksAvailableRecyclerViewAdapter(this, itens));
            relativeLayoutLoad.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            relativeLayoutError.setVisibility(View.GONE);
            swipeRefreshLayout.setVisibility(View.VISIBLE);
        } else {
            textView.setText(R.string.wifi_empty);
            showButtonErro(1);
            recyclerView.setAdapter(null);
            relativeLayoutLoad.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            swipeRefreshLayout.setVisibility(View.GONE);
            relativeLayoutError.setVisibility(View.VISIBLE);
        }
    }

    public boolean isPermissionNetworkGranted() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CHANGE_WIFI_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CHANGE_WIFI_STATE)) {
                ActivityCompat.requestPermissions(this, new String[]{
                                Manifest.permission.CHANGE_WIFI_STATE
                        },
                        NETWORK_REQUEST_CODE);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{
                                Manifest.permission.CHANGE_WIFI_STATE
                        },
                        NETWORK_REQUEST_CODE);
            }
            return false;
        } else {
            return true;
        }
    }

    public boolean isLocationEnabled() {
        int locationMode = 0;
        String locationProviders;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public void onResume() {
        super.onResume();
        IntentFilter filterScan = new IntentFilter();
        filterScan.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        this.registerReceiver(this.receiverScan, filterScan);

        IntentFilter filterUpdate = new IntentFilter();
        filterUpdate.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
        filterUpdate.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filterUpdate.addAction(android.net.ConnectivityManager.CONNECTIVITY_ACTION);
        filterUpdate.addAction(LocationManager.PROVIDERS_CHANGED_ACTION);
        filterUpdate.addAction(WifiConnectedService.UPDATE_NETWORK_CONNECTED);
        this.registerReceiver(this.receiverUpdate, filterUpdate);
        scanNetwork();
    }

    public void onPause() {
        super.onPause();
        this.unregisterReceiver(this.receiverScan);
        this.unregisterReceiver(this.receiverUpdate);
    }

    @Override
    public boolean onSupportNavigateUp(){
        this.onBackPressed();
        return true;
    }
}
