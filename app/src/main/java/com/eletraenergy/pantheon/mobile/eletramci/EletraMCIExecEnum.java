package com.eletraenergy.pantheon.mobile.eletramci;

/**
 * Created by dcsda on 16/01/2018.
 */

public enum EletraMCIExecEnum {

    RELAY_OPERATION_ACTION("relay_operation_action"),
    CHECKPOINT_READ("checkpoint_read"),
    CHECKPOINTREAD("checkpointRead"),
    CPU_FLAGS("cpuFlags"),
    CONSUMERS_DATA("consumersData"),
    METER_NUM("meterNum"),
    FLAGS("flags"),
    DATE_AND_TIME("dateAndTime"),
    CONSUMER_DATA("consumerData"),
    ACTION_RELAY("actionRelay"),
    ALARM_CONFIG("alarm_config"),
    ALARMCONFIGURATION("alarmConfiguration"),
    MASK("mask"),
    CONFIG("config");

    private final String readingCommand;

    EletraMCIExecEnum(String readingCommand) {
        this.readingCommand = readingCommand;
    }
    public String getWriteCommand() {
        return readingCommand;
    }
}
