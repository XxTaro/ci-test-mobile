package com.eletraenergy.pantheon.mobile.ui;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;

import com.eletraenergy.pantheon.mobile.R;
import com.google.android.material.appbar.AppBarLayout;

public class LicenseSettingActivity extends PantheonCompatActivity {

    protected Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_license_settings);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);
        //getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutSetting, new LicenseSettingFragment()).commit();
    }

    @Override
    public boolean onSupportNavigateUp(){
        this.onBackPressed();
        return true;
    }
}
