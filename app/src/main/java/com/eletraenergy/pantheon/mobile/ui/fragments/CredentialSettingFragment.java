package com.eletraenergy.pantheon.mobile.ui.fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreference;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.ui.widgets.ValidationEditTextPreference;
import com.eletraenergy.pantheon.mobile.ui.widgets.ValidationEditTextPreferenceDialog;


public class CredentialSettingFragment extends PreferenceFragmentCompat {

    protected ValidationEditTextPreference encrypt;
    protected ValidationEditTextPreference auth;
    protected ValidationEditTextPreference security;
    protected SwitchPreference levelSecurity;

    protected SharedPreferences prefs;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.pref_fragmented_credentials);

        FragmentActivity activity = getActivity();
        prefs = PreferenceManager.getDefaultSharedPreferences(activity);

        security = (ValidationEditTextPreference)findPreference(activity.getString(R.string.key_security));
        security.setSummary(prefs.getString(activity.getString(R.string.key_security), ""));

        encrypt = (ValidationEditTextPreference)findPreference(activity.getString(R.string.key_encrypt));
        encrypt.setSummary(prefs.getString(activity.getString(R.string.key_encrypt), ""));

        auth = (ValidationEditTextPreference)findPreference(activity.getString(R.string.key_auth));
        auth.setSummary(prefs.getString(activity.getString(R.string.key_auth), ""));

        levelSecurity = (SwitchPreference) findPreference(activity.getString(R.string.key_leve_security));
        if(levelSecurity.isChecked()) {
            encrypt.setEnabled(true);
            auth.setEnabled(true);
            levelSecurity.setSummary(R.string.total_access);
            security.setEnabled(false);
            security.setLength(32);
        } else {
            encrypt.setEnabled(false);
            auth.setEnabled(false);
            levelSecurity.setSummary(R.string.limited_access);
            security.setEnabled(true);
            security.setLength(16);
        }
        levelSecurity.setOnPreferenceClickListener(preference -> {
            levelSecurity = (SwitchPreference) findPreference(activity.getString(R.string.key_leve_security));
            prefs.edit().putString(activity.getString(R.string.key_security), "").commit();
            security.setSummary("");
            prefs.edit().putString(activity.getString(R.string.key_encrypt), "").commit();
            encrypt.setSummary("");
            prefs.edit().putString(activity.getString(R.string.key_auth), "").commit();
            auth.setSummary("");
            if(levelSecurity.isChecked()) {
                encrypt.setEnabled(true);
                auth.setEnabled(true);
                security.setEnabled(false);
                levelSecurity.setSummary(R.string.total_access);
                security.setLength(32);
            } else {
                encrypt.setEnabled(false);
                auth.setEnabled(false);
                security.setEnabled(true);
                levelSecurity.setSummary(R.string.limited_access);
                security.setLength(16);
            }
            return false;
        });
        prefs.registerOnSharedPreferenceChangeListener((sharedPreferences, s) -> {
            if(s.equals(activity.getString(R.string.key_leve_security))) {
                levelSecurity = (SwitchPreference) findPreference(activity.getString(R.string.key_leve_security));
                prefs.edit().putString(activity.getString(R.string.key_security), "").commit();
                security.setSummary("");
                prefs.edit().putString(activity.getString(R.string.key_encrypt), "").commit();
                encrypt.setSummary("");
                prefs.edit().putString(activity.getString(R.string.key_auth), "").commit();
                auth.setSummary("");
                if (levelSecurity.isChecked()) {
                    encrypt.setEnabled(true);
                    auth.setEnabled(true);
                    security.setEnabled(false);
                    levelSecurity.setSummary(R.string.total_access);
                    security.setLength(32);
                } else {
                    encrypt.setEnabled(false);
                    auth.setEnabled(false);
                    security.setEnabled(true);
                    levelSecurity.setSummary(R.string.limited_access);
                    security.setLength(16);
                }
            }
        });
    }

    @Override
    public void onDisplayPreferenceDialog(Preference preference) {
        DialogFragment dialogFragment = null;
        if (preference instanceof ValidationEditTextPreference) {
            dialogFragment = ValidationEditTextPreferenceDialog.newInstance(preference.getKey());
            dialogFragment.setTargetFragment(this, 0);
            dialogFragment.show(this.getFragmentManager(), "android.support.v7.preference.PreferenceFragment.DIALOG");
        } else {
            super.onDisplayPreferenceDialog(preference);
        }
    }
}
