package com.eletraenergy.pantheon.mobile.ui.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.ui.adapters.SettingRecyclerViewAdapter;

import java.util.ArrayList;

public class SettingsFragment extends Fragment {

    protected RecyclerView recyclerView;

    public SettingsFragment() {
        // Required empty public constructor
    }
    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        recyclerView = view.findViewById(R.id.recyclerViewSetting);

        ArrayList<SettingRecyclerViewAdapter.ItemSetting> itens = new ArrayList<>();
        SettingRecyclerViewAdapter.ItemSetting item = new SettingRecyclerViewAdapter.ItemSetting();
        item.text = getResources().getString(R.string.label_general_setting);
        item.image = getResources().getDrawable(R.drawable.baseline_settings_black_24);
        item.description = getString(R.string.integer_digits) + ", " + getString(R.string.decimal_digits);
        itens.add(item);

        item = new SettingRecyclerViewAdapter.ItemSetting();
        item.text = getResources().getString(R.string.label_credential_setting);
        item.image = getResources().getDrawable(R.drawable.ic_lock_black_24dp);
        item.description = getString(R.string.high_leve_security);
        itens.add(item);

        item = new SettingRecyclerViewAdapter.ItemSetting();
        item.text = getResources().getString(R.string.label_license_setting);
        item.image = getResources().getDrawable(R.drawable.ic_vpn_key_black_24dp);
        item.description = getString(R.string.license_key) + ", " + getString(R.string.license_reset);
        itens.add(item);

        item = new SettingRecyclerViewAdapter.ItemSetting();
        item.text = getResources().getString(R.string.label_communication_setting);
        item.image = getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp);
        item.description = getString(R.string.ip) + ", " + getString(R.string.port);
        itens.add(item);

        item = new SettingRecyclerViewAdapter.ItemSetting();
        item.text = getResources().getString(R.string.label_information_setting);
        item.image = getResources().getDrawable(R.drawable.ic_info_outline_black_24dp);
        item.description = getString(R.string.news);
        itens.add(item);

        SettingRecyclerViewAdapter adapter = new SettingRecyclerViewAdapter((AppCompatActivity) this.getActivity(), itens);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.setAdapter(adapter);

        return view;
    }


}
