package com.eletraenergy.pantheon.mobile.eletramci;

/**
 * Created by dcsda on 16/01/2018.
 */

public enum EletraMCIWriteEnum {

    ALARM_CONFIGURATION("alarm_configuration"),
    ALARMCONFIGURATION_att("alarmConfiguration"),
    MASK_att("mask"),
    CONFIG_att("config"),

    CHECKPOINT_READ("checkpoint_read"),
    CHECKPOINTREAD_att("checkpointRead"),

    ALARM_CLEAR("alarm_clear"),
    ALARMCLEAR_att("alarmClear"),
    TYPE_att("type"),
    AMOUNT_att("amount"),

    ACTION_RELAY("actionRelay");

    private final String readingCommand;

    EletraMCIWriteEnum(String readingCommand) {
        this.readingCommand = readingCommand;
    }
    public String getWriteCommand() {
        return readingCommand;
    }
}
