package com.eletraenergy.pantheon.mobile.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadTitle;
import com.eletraenergy.pantheon.mobile.ui.RelaysControlsActivity;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dcsda on 16/01/2018.
 */

public class RelaysControlsRecyclerViewAdapter extends RecyclerView.Adapter<RelaysControlsRecyclerViewAdapter.ViewHolder> {

    protected List<RelayControlItem> itens;
    protected RelaysControlsActivity context;
    protected EletraMCIReadTitle eletraMCIReadTitle;
    HashMap<Integer, String> phaseMap = new HashMap<Integer, String>();

    public String getMeterPositionHex(String num) {
        int i = Integer.parseInt(num);
        return Integer.toString(i, 16);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView numberTextViewHolder;
        public TextView pos1TextViewHolder;
        public TextView pos2TextViewHolder;
        public TextView pos3TextViewHolder;
        public TextView phase1TextViewHolder;
        public TextView phase2TextViewHolder;
        public TextView phase3TextViewHolder;
        public TextView pos1PostRelay;
        public TextView pos2PostRelay;
        public TextView pos3PostRelay;
        public TextView pos1FailFlag;
        public TextView pos2FailFlag;
        public TextView pos3FailFlag;
        public TextView pos1ReverseCurrent;
        public TextView pos2ReverseCurrent;
        public TextView pos3ReverseCurrent;
        public TextView pos1Timeout;
        public TextView pos2Timeout;
        public TextView pos3Timeout;
        public LinearLayout layout1TextViewHolder;
        public LinearLayout layout2TextViewHolder;
        public LinearLayout layout3TextViewHolder;
        public ImageView relay1ImageViewHolder;
        public ImageView relay2ImageViewHolder;
        public ImageView relay3ImageViewHolder;
        public AppCompatCheckBox checkboxItemHolder;
        public LinearLayout disableLinearLayoutHolder;
        public LinearLayout enableLinearLayoutHolder;
        public LinearLayout detailLinearLayoutItemRelaysControl;
        public TextView disPosTextViewHolder;
        public TextView disPhaseTextViewHolder;
        public ImageButton imageButtonRelaysControl;

        boolean[] checked;

        public ViewHolder(View view) {
            super(view);

            disableLinearLayoutHolder = (LinearLayout) view.findViewById(R.id.disablePositionLinearLayout);
            enableLinearLayoutHolder = (LinearLayout) view.findViewById(R.id.enablePositionLinearLayout);
            detailLinearLayoutItemRelaysControl = (LinearLayout) view.findViewById(R.id.detailLinearLayoutItemRelaysControl);

            disPosTextViewHolder = (TextView) view.findViewById(R.id.posDisTextViewItemRelaysControls);
            disPhaseTextViewHolder = (TextView) view.findViewById(R.id.phaseDisTextViewItemRelaysControls);

            numberTextViewHolder = (TextView) view.findViewById(R.id.numberTextViewItemRelaysControls);
            pos1TextViewHolder = (TextView) view.findViewById(R.id.pos1TextViewItemRelaysControls);
            pos2TextViewHolder = (TextView) view.findViewById(R.id.pos2TextViewItemRelaysControls);
            pos3TextViewHolder = (TextView) view.findViewById(R.id.pos3TextViewItemRelaysControls);

            phase1TextViewHolder = (TextView) view.findViewById(R.id.phase1TextViewItemRelaysControls);
            phase2TextViewHolder = (TextView) view.findViewById(R.id.phase2TextViewItemRelaysControls);
            phase3TextViewHolder = (TextView) view.findViewById(R.id.phase3TextViewItemRelaysControls);

            layout1TextViewHolder = (LinearLayout) view.findViewById(R.id.layout1LinearLayoutItemRelaysControls);
            layout2TextViewHolder = (LinearLayout) view.findViewById(R.id.layout2LinearLayoutItemRelaysControls);
            layout3TextViewHolder = (LinearLayout) view.findViewById(R.id.layout3LinearLayoutItemRelaysControls);

            relay1ImageViewHolder = (ImageView) view.findViewById(R.id.relay1ImageViewItemRelaysControls);
            relay2ImageViewHolder = (ImageView) view.findViewById(R.id.relay2ImageViewItemRelaysControls);
            relay3ImageViewHolder = (ImageView) view.findViewById(R.id.relay3ImageViewItemRelaysControls);

            pos1PostRelay = (TextView) view.findViewById(R.id.pos1PostRelay);
            pos2PostRelay = (TextView) view.findViewById(R.id.pos2PostRelay);
            pos3PostRelay = (TextView) view.findViewById(R.id.pos3PostRelay);

            pos1FailFlag = (TextView) view.findViewById(R.id.pos1FailFlag);
            pos2FailFlag = (TextView) view.findViewById(R.id.pos2FailFlag);
            pos3FailFlag = (TextView) view.findViewById(R.id.pos3FailFlag);

            pos1ReverseCurrent = (TextView) view.findViewById(R.id.pos1ReverseCurrent);
            pos2ReverseCurrent = (TextView) view.findViewById(R.id.pos2ReverseCurrent);
            pos3ReverseCurrent = (TextView) view.findViewById(R.id.pos3ReverseCurrent);

            pos1Timeout = (TextView) view.findViewById(R.id.pos1Timeout);
            pos2Timeout = (TextView) view.findViewById(R.id.pos2Timeout);
            pos3Timeout = (TextView) view.findViewById(R.id.pos3Timeout);

            imageButtonRelaysControl = (ImageButton) view.findViewById(R.id.imageButtonRelaysControl);

            checked = new boolean[itens.size()];
            checkboxItemHolder = (AppCompatCheckBox) view.findViewById(R.id.checkboxItemRelaysControls);

            imageButtonRelaysControl.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
            imageButtonRelaysControl.setOnClickListener(v -> {
                RelaysControlsRecyclerViewAdapter.RelayControlItem it = RelaysControlsRecyclerViewAdapter.this.itens.get(getAdapterPosition());
                if (it.isOpen) {
                    imageButtonRelaysControl.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                    detailLinearLayoutItemRelaysControl.setVisibility(View.VISIBLE);
                    RelaysControlsRecyclerViewAdapter.this.itens.get(getAdapterPosition()).isOpen = false;
                } else {
                    imageButtonRelaysControl.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                    detailLinearLayoutItemRelaysControl.setVisibility(View.GONE);
                    RelaysControlsRecyclerViewAdapter.this.itens.get(getAdapterPosition()).isOpen = true;
                }
            });
        }
    }

    public RelaysControlsRecyclerViewAdapter(RelaysControlsActivity context, List<RelayControlItem> itensEnable) {

        this.itens = itens;
        this.itens = new ArrayList<RelayControlItem>();
        int pos = 1;
        boolean add = true;
        while (pos <= 12) {

            add = true;

            for(RelayControlItem unt: itensEnable) {
                if(Integer.parseInt(unt.pos[0]) == pos) {
                    this.itens.add(unt);
                    add = false;
                }
                if (unt.pos[1] != null &&
                        Integer.parseInt(unt.pos[1]) == pos) {
                    add = false;
                }
                if(unt.pos[2] != null &&
                        Integer.parseInt(unt.pos[2]) == pos) {
                    add = false;
                }
            }
            if(add) {
                RelayControlItem it = new RelayControlItem();
                it.pos = new String[1];
                it.pos[0] = String.valueOf(pos);
                it.isEnable = false;
                this.itens.add(it);
            }
            pos++;
        }
        this.context = context;
        this.eletraMCIReadTitle = new EletraMCIReadTitle();

        phaseMap.put(1, "A");
        phaseMap.put(2, "B");
        phaseMap.put(3, "C");

        phaseMap.put(4, "A");
        phaseMap.put(5, "B");
        phaseMap.put(6, "C");

        phaseMap.put(7, "A");
        phaseMap.put(8, "B");
        phaseMap.put(9, "C");

        phaseMap.put(10, "A");
        phaseMap.put(11, "B");
        phaseMap.put(12, "C");
    }

    @Override
    public RelaysControlsRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_relays_controls, parent, false);
        RelaysControlsRecyclerViewAdapter.ViewHolder vh = new RelaysControlsRecyclerViewAdapter.ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(RelaysControlsRecyclerViewAdapter.ViewHolder holder, int position) {
        RelayControlItem it = this.itens.get(position);

        holder.checkboxItemHolder.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked) {
                context.addUnit(getMeterPositionHex(itens.get(holder.getAdapterPosition()).pos[0]));
                holder.checked[position] = true;
            } else {
                context.rmUnit(getMeterPositionHex(itens.get(holder.getAdapterPosition()).pos[0]));
                holder.checked[position] = false;
            }
        });
        holder.checkboxItemHolder.setChecked(holder.checked[position]);

        if(it.isEnable) {
            holder.numberTextViewHolder.setText(it.number);
            String meterPos = getMeterPositionHex(RelaysControlsRecyclerViewAdapter.this.itens.get(position).pos[0]);

            holder.detailLinearLayoutItemRelaysControl.setVisibility(View.VISIBLE);

            holder.pos1TextViewHolder.setVisibility(View.GONE);
            holder.pos2TextViewHolder.setVisibility(View.GONE);
            holder.pos3TextViewHolder.setVisibility(View.GONE);

            holder.phase1TextViewHolder.setVisibility(View.GONE);
            holder.phase2TextViewHolder.setVisibility(View.GONE);
            holder.phase3TextViewHolder.setVisibility(View.GONE);

            holder.layout1TextViewHolder.setVisibility(View.GONE);
            holder.layout2TextViewHolder.setVisibility(View.GONE);
            holder.layout3TextViewHolder.setVisibility(View.GONE);

            holder.relay1ImageViewHolder.setVisibility(View.GONE);
            holder.relay2ImageViewHolder.setVisibility(View.GONE);
            holder.relay3ImageViewHolder.setVisibility(View.GONE);

            holder.pos1PostRelay.setVisibility(View.GONE);
            holder.pos2PostRelay.setVisibility(View.GONE);
            holder.pos3PostRelay.setVisibility(View.GONE);

            holder.pos1FailFlag.setVisibility(View.GONE);
            holder.pos2FailFlag.setVisibility(View.GONE);
            holder.pos3FailFlag.setVisibility(View.GONE);

            holder.pos1ReverseCurrent.setVisibility(View.GONE);
            holder.pos2ReverseCurrent.setVisibility(View.GONE);
            holder.pos3ReverseCurrent.setVisibility(View.GONE);

            holder.pos1Timeout.setVisibility(View.GONE);
            holder.pos2Timeout.setVisibility(View.GONE);
            holder.pos3Timeout.setVisibility(View.GONE);

            if (it.phase[0] != null) {
                holder.pos1TextViewHolder.setText(it.pos[0]);
                holder.phase1TextViewHolder.setText(it.phase[0]);
                holder.relay1ImageViewHolder.setImageResource(it.relay[0]);
                holder.pos1PostRelay.setText(it.postRelay[0]);
                holder.pos1FailFlag.setText(it.fails[0]);
                holder.pos1ReverseCurrent.setText(it.reverseCurrent[0]);
                holder.pos1Timeout.setText(it.timeout[0]);
                holder.pos1TextViewHolder.setVisibility(View.VISIBLE);
                holder.phase1TextViewHolder.setVisibility(View.VISIBLE);
                holder.layout1TextViewHolder.setVisibility(View.VISIBLE);
                holder.relay1ImageViewHolder.setVisibility(View.VISIBLE);
                holder.pos1PostRelay.setVisibility(View.VISIBLE);
                holder.pos1FailFlag.setVisibility(View.VISIBLE);
                holder.pos1ReverseCurrent.setVisibility(View.VISIBLE);
                holder.pos1Timeout.setVisibility(View.VISIBLE);
            }
            if (it.phase[1] != null) {
                holder.pos2TextViewHolder.setText(it.pos[1]);
                holder.phase2TextViewHolder.setText(it.phase[1]);
                holder.relay2ImageViewHolder.setImageResource(it.relay[1]);
                holder.pos2PostRelay.setText(it.postRelay[1]);
                holder.pos2FailFlag.setText(it.fails[1]);
                holder.pos2ReverseCurrent.setText(it.reverseCurrent[1]);
                holder.pos2Timeout.setText(it.timeout[1]);
                holder.pos2TextViewHolder.setVisibility(View.VISIBLE);
                holder.phase2TextViewHolder.setVisibility(View.VISIBLE);
                holder.layout2TextViewHolder.setVisibility(View.VISIBLE);
                holder.relay2ImageViewHolder.setVisibility(View.VISIBLE);
                holder.pos2PostRelay.setVisibility(View.VISIBLE);
                holder.pos2FailFlag.setVisibility(View.VISIBLE);
                holder.pos2ReverseCurrent.setVisibility(View.VISIBLE);
                holder.pos2Timeout.setVisibility(View.VISIBLE);
            }
            if (it.phase[2] != null) {
                holder.pos3TextViewHolder.setText(it.pos[2]);
                holder.phase3TextViewHolder.setText(it.phase[2]);
                holder.relay3ImageViewHolder.setImageResource(it.relay[2]);
                holder.pos3PostRelay.setText(it.postRelay[2]);
                holder.pos3FailFlag.setText(it.fails[2]);
                holder.pos3ReverseCurrent.setText(it.reverseCurrent[2]);
                holder.pos3Timeout.setText(it.timeout[2]);
                holder.pos3TextViewHolder.setVisibility(View.VISIBLE);
                holder.phase3TextViewHolder.setVisibility(View.VISIBLE);
                holder.layout3TextViewHolder.setVisibility(View.VISIBLE);
                holder.relay3ImageViewHolder.setVisibility(View.VISIBLE);
                holder.pos3PostRelay.setVisibility(View.VISIBLE);
                holder.pos3FailFlag.setVisibility(View.VISIBLE);
                holder.pos3ReverseCurrent.setVisibility(View.VISIBLE);
                holder.pos3Timeout.setVisibility(View.VISIBLE);
            }
            holder.disableLinearLayoutHolder.setVisibility(View.GONE);
            holder.enableLinearLayoutHolder.setVisibility(View.VISIBLE);
        } else  {
            holder.enableLinearLayoutHolder.setVisibility(View.GONE);
            holder.disableLinearLayoutHolder.setVisibility(View.VISIBLE);

            holder.disPosTextViewHolder.setText(context.getString(R.string.attr_meter_pos) + " " +  it.pos[0]);
            holder.disPhaseTextViewHolder.setText(context.getString(R.string.attr_phase) + " " + phaseMap.get(Integer.parseInt(it.pos[0])));
        }
    }
    
    @Override
    public int getItemCount() {
        return itens.size();
    }

    public static class RelayControlItem {
        public String number;
        public String[] pos;
        public String[] phase;
        public Integer[] relay;
        public String[] postRelay;
        public String[] fails;
        public String[] reverseCurrent;
        public String[] timeout;
        public boolean isEnable = false;
        public boolean isOpen = false;
    }

}