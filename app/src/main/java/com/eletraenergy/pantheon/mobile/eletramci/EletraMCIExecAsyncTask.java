package com.eletraenergy.pantheon.mobile.eletramci;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.eletraenergy.pantheon.mobile.R;
import com.google.common.base.Strings;

import br.com.eletra.mci.app.Command;
import br.com.eletra.mci.app.ListenerMCI;
import br.com.eletra.mci.app.MeterComm;
import br.com.eletra.mci.app.MeterPhyConnectionException;
import br.com.eletra.mci.app.MeterSchema;
import br.com.eletra.mci.app.ProtocolException;
import br.com.eletra.mci.dlms.DLMSCommand;
import br.com.eletra.mci.dlms.DLMSConnection;
import br.com.eletra.mci.serial.ComPortException;
import br.com.eletra.mci.serial.Timeout;
import br.com.eletra.mci.util.HexString;

/**
 * Created by dcsda on 16/01/2018.
 */

public class EletraMCIExecAsyncTask extends AsyncTask<Void, Void, Void> {

    protected MeterComm meterComm;
    protected String encryptKey;
    protected String authKey;
    protected String securityKey;
    protected String ip;
    protected String port;
    protected boolean lavelTaypeKey;
    protected AppCompatActivity context;
    protected int error;

    protected EletraMCIPost postExecute;
    protected EletraMCIPre preExecute;

    protected DLMSCommand command;


    public EletraMCIExecAsyncTask(AppCompatActivity context, DLMSCommand command){
        this.context = context;
        this.postExecute = null;
        this.preExecute = null;
        this.command = command;
        this.error = 0;

        MeterSchema meterSchema = MeterSchema.PANTHEON_V1;
        meterComm = new MeterComm();
        meterComm.loadSchema(meterSchema, "DLMSTCP");

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.encryptKey = prefs.getString(context.getString(R.string.key_encrypt), "");
        this.authKey = prefs.getString(context.getString(R.string.key_auth), "");
        this.securityKey = Strings.isNullOrEmpty(prefs.getString(context.getString(R.string.key_security), "00000000000000000000000000000000")) ? "00000000000000000000000000000000" : prefs.getString(context.getString(R.string.key_security), "00000000000000000000000000000000");
        this.ip = prefs.getString(context.getString(R.string.key_ip), "");
        this.port = prefs.getString(context.getString(R.string.key_port), "");

        this.lavelTaypeKey = prefs.getBoolean(context.getString(R.string.key_leve_security), false);
    }

    public EletraMCIExecAsyncTask(AppCompatActivity context, DLMSCommand command, EletraMCIPost postExecute){
        this.context = context;
        this.postExecute = postExecute;
        this.preExecute = null;
        this.command = command;
        this.error = 0;

        MeterSchema meterSchema = MeterSchema.PANTHEON_V1;
        meterComm = new MeterComm();
        meterComm.loadSchema(meterSchema, "DLMSTCP");

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.encryptKey = prefs.getString(context.getString(R.string.key_encrypt), "");
        this.authKey = prefs.getString(context.getString(R.string.key_auth), "");
        this.securityKey = Strings.isNullOrEmpty(prefs.getString(context.getString(R.string.key_security), "00000000000000000000000000000000")) ? "00000000000000000000000000000000" : prefs.getString(context.getString(R.string.key_security), "00000000000000000000000000000000");
        this.ip = prefs.getString(context.getString(R.string.key_ip), "");
        this.port = prefs.getString(context.getString(R.string.key_port), "");

        this.lavelTaypeKey = prefs.getBoolean(context.getString(R.string.key_leve_security), false);
    }
    public EletraMCIExecAsyncTask(AppCompatActivity context, DLMSCommand command, EletraMCIPost postExecute, EletraMCIPre preExecute){
        this.context = context;
        this.postExecute = postExecute;
        this.preExecute = preExecute;
        this.command = command;
        this.error = 0;

        MeterSchema meterSchema = MeterSchema.PANTHEON_V1;
        meterComm = new MeterComm();
        meterComm.loadSchema(meterSchema, "DLMSTCP");

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.encryptKey = prefs.getString(context.getString(R.string.key_encrypt), "");
        this.authKey = prefs.getString(context.getString(R.string.key_auth), "");
        this.securityKey = Strings.isNullOrEmpty(prefs.getString(context.getString(R.string.key_security), "00000000000000000000000000000000")) ? "00000000000000000000000000000000" : prefs.getString(context.getString(R.string.key_security), "00000000000000000000000000000000");
        this.ip = prefs.getString(context.getString(R.string.key_ip), "");
        this.port = prefs.getString(context.getString(R.string.key_port), "");

        this.lavelTaypeKey = prefs.getBoolean(context.getString(R.string.key_leve_security), false);
    }
    @Override
    protected void onPreExecute() {
        if (preExecute != null) {
            preExecute.run(EletraMCIExecAsyncTask.this);
        }
        super.onPreExecute();
    }

    private void connectionCancel() {
        try {
            this.meterComm.disconnect();
            meterComm = null;
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (ComPortException e) {
            e.printStackTrace();
        } catch (MeterPhyConnectionException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            if(!Strings.isNullOrEmpty(this.authKey)) {
                ((DLMSConnection) meterComm.getConnection()).setAuthenticationKey(HexString.hexStringToByteArray(this.authKey));
            }
            if(!Strings.isNullOrEmpty(this.encryptKey)) {
                ((DLMSConnection) meterComm.getConnection()).setEncryptKey(HexString.hexStringToByteArray(this.encryptKey));
            }
            if(!Strings.isNullOrEmpty(this.securityKey)) {
                ((DLMSConnection) meterComm.getConnection()).setHLSkey(HexString.hexStringToByteArray(this.securityKey));
            }
            ((DLMSConnection) meterComm.getConnection()).useBroadcast();
            ((DLMSConnection) meterComm.getConnection()).setLogicalAddr(0x0001);
            Log.i(EletraMCIExecAsyncTask.class.getName(), "IP: " + this.ip);
            Log.i(EletraMCIExecAsyncTask.class.getName(), "Port: " + this.port);

            meterComm.connect(ip, Integer.parseInt(port), new EletraMCIExecAsyncTask.Listener());
            meterComm.execCommand(this.command);

            try {
                meterComm.disconnect();
                meterComm = null;
            } catch (ComPortException e) {
                e.printStackTrace();
            } catch (MeterPhyConnectionException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            final long startTime = System.currentTimeMillis();
            do {
            } while (System.currentTimeMillis() - startTime <= 8 * 1000);
            return null;

        } catch (ComPortException e) {
            e.printStackTrace();
            this.connectionCancel();
            this.error = R.string.exception_port;
        }

        catch (ProtocolException e) {
            e.printStackTrace();
            this.connectionCancel();
            /*
            * Alterado por conta do bug referente ao retorno de erros na atuação sobre o relé
            * */
            //this.error = R.string.exception_protocol;
        } catch (Timeout.TimeoutException e) {
            e.printStackTrace();
            this.connectionCancel();
            this.error = R.string.exception_timeout;
        } catch (MeterPhyConnectionException e) {
            e.printStackTrace();
            this.connectionCancel();
            this.error = R.string.exception_connection;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(EletraMCIExecAsyncTask.class.getSimpleName(), e.getMessage());
            this.connectionCancel();
            this.error = R.string.exception_generic;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if(this.error > 0) {
            //Snackbar.make(context.findViewById(android.R.id.content), this.error, Snackbar.LENGTH_SHORT).show();
        }
        if (postExecute != null) {
            postExecute.run(this.error);
        }
        super.onPostExecute(aVoid);
    }

    private class Listener implements ListenerMCI {
        @Override
        public void handle(Command command) {
            System.out.println();
        }
    }

    public interface EletraMCIPost {
        public void run(int error);
    }
    public interface EletraMCIPre {
        public void run(EletraMCIExecAsyncTask task);
    }
}
