package com.eletraenergy.pantheon.mobile.eletramci;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;

import com.eletraenergy.pantheon.mobile.R;
import com.google.common.base.Strings;

import java.util.List;

import br.com.eletra.mci.app.Command;
import br.com.eletra.mci.app.ListenerMCI;
import br.com.eletra.mci.app.MeterComm;
import br.com.eletra.mci.app.MeterPhyConnectionException;
import br.com.eletra.mci.app.MeterSchema;
import br.com.eletra.mci.app.ProtocolException;
import br.com.eletra.mci.dlms.DLMSAtt;
import br.com.eletra.mci.dlms.DLMSCommand;
import br.com.eletra.mci.dlms.DLMSConnection;
import br.com.eletra.mci.serial.ComPortException;
import br.com.eletra.mci.serial.Timeout;
import br.com.eletra.mci.util.HexString;

/**
 * Created by dcsda on 16/01/2018.
 */

public class EletraMCIReadAsyncTask extends AsyncTask<Void, Void, Void> {

    protected MeterComm meterComm;
    protected String encryptKey;
    protected String authKey;
    protected String securityKey;
    protected String ip;
    protected String port;
    protected boolean lavelTypeKey;
    protected AppCompatActivity context;
    protected int error;
    protected EletraMCIPost postExecute;
    protected EletraMCIPre preExecute;
    protected List<DLMSAtt> attributes;
    protected int logicalAddress;

    protected DLMSCommand command;

    public EletraMCIReadAsyncTask(AppCompatActivity context, EletraMCIReadEnum eletraMCIReadEnum){
        this.context = context;
        this.postExecute = null;
        this.preExecute = null;
        this.error = 0;

        MeterSchema meterSchema = MeterSchema.PANTHEON_V1;
        meterComm = new MeterComm();
        meterComm.loadSchema(meterSchema, "DLMSTCP");

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.encryptKey = prefs.getString(context.getString(R.string.key_encrypt), "");
        this.authKey = prefs.getString(context.getString(R.string.key_auth), "");
        this.securityKey = Strings.isNullOrEmpty(prefs.getString(context.getString(R.string.key_security), "00000000000000000000000000000000")) ? "00000000000000000000000000000000" : prefs.getString(context.getString(R.string.key_security), "00000000000000000000000000000000");
        this.ip = prefs.getString(context.getString(R.string.key_ip), "");
        this.port = prefs.getString(context.getString(R.string.key_port), "");
        Log.i(EletraMCIReadAsyncTask.class.getName(), "SECURITY KEY: " + this.securityKey);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "AUTH KEY: " + this.authKey);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "ENCRIPT KEY: " + this.encryptKey);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "IP: " + this.ip);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "PORT: " + this.port);

        this.lavelTypeKey = prefs.getBoolean(context.getString(R.string.key_leve_security), false);
        this.command = (DLMSCommand) meterComm.getAvailableCommands().get(eletraMCIReadEnum.getReadingCommand());
    }

    public EletraMCIReadAsyncTask(AppCompatActivity context, DLMSCommand command, EletraMCIReadEnum eletraMCIReadEnum, EletraMCIPost postExecute){
        this.context = context;
        this.postExecute = postExecute;
        this.preExecute = preExecute;
        this.error = 0;

        MeterSchema meterSchema = MeterSchema.PANTHEON_V1;
        meterComm = new MeterComm();
        meterComm.loadSchema(meterSchema, "DLMSTCP");

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.encryptKey = prefs.getString(context.getString(R.string.key_encrypt), "");
        this.authKey = prefs.getString(context.getString(R.string.key_auth), "");
        this.securityKey = Strings.isNullOrEmpty(prefs.getString(context.getString(R.string.key_security), "00000000000000000000000000000000")) ? "00000000000000000000000000000000" : prefs.getString(context.getString(R.string.key_security), "00000000000000000000000000000000");
        this.ip = prefs.getString(context.getString(R.string.key_ip), "");
        this.port = prefs.getString(context.getString(R.string.key_port), "");
        this.lavelTypeKey = prefs.getBoolean(context.getString(R.string.key_leve_security), false);

        Log.i(EletraMCIReadAsyncTask.class.getName(), "SECURITY KEY: " + this.securityKey);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "AUTH KEY: " + this.authKey);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "ENCRIPT KEY: " + this.encryptKey);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "IP: " + this.ip);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "PORT: " + this.port);

        this.command = (DLMSCommand) meterComm.getAvailableCommands().get(eletraMCIReadEnum.getReadingCommand());
    }
    public EletraMCIReadAsyncTask(AppCompatActivity context, DLMSCommand command, EletraMCIPost postExecute, EletraMCIPre preExecute){
        this.context = context;
        this.postExecute = postExecute;
        this.preExecute = preExecute;
        this.error = 0;

        MeterSchema meterSchema = MeterSchema.PANTHEON_V1;
        meterComm = new MeterComm();
        meterComm.loadSchema(meterSchema, "DLMSTCP");

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.encryptKey = prefs.getString(context.getString(R.string.key_encrypt), "");
        this.authKey = prefs.getString(context.getString(R.string.key_auth), "");
        this.securityKey = Strings.isNullOrEmpty(prefs.getString(context.getString(R.string.key_security), "00000000000000000000000000000000")) ? "00000000000000000000000000000000" : prefs.getString(context.getString(R.string.key_security), "00000000000000000000000000000000");
        this.ip = prefs.getString(context.getString(R.string.key_ip), "");
        this.port = prefs.getString(context.getString(R.string.key_port), "");
        this.lavelTypeKey = prefs.getBoolean(context.getString(R.string.key_leve_security), false);

        Log.i(EletraMCIReadAsyncTask.class.getName(), "SECURITY KEY: " + this.securityKey);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "AUTH KEY: " + this.authKey);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "ENCRIPT KEY: " + this.encryptKey);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "IP: " + this.ip);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "PORT: " + this.port);

        this.command = command;
    }
    public EletraMCIReadAsyncTask(AppCompatActivity context, EletraMCIReadEnum eletraMCIReadEnum, EletraMCIPost postExecute){
        this.context = context;
        this.postExecute = postExecute;
        this.preExecute = null;
        this.error = 0;

        MeterSchema meterSchema = MeterSchema.PANTHEON_V1;
        meterComm = new MeterComm();
        meterComm.loadSchema(meterSchema, "DLMSTCP");

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.encryptKey = prefs.getString(context.getString(R.string.key_encrypt), "");
        this.authKey = prefs.getString(context.getString(R.string.key_auth), "");
        this.securityKey = Strings.isNullOrEmpty(prefs.getString(context.getString(R.string.key_security), "00000000000000000000000000000000")) ? "00000000000000000000000000000000" : prefs.getString(context.getString(R.string.key_security), "00000000000000000000000000000000");
        this.ip = prefs.getString(context.getString(R.string.key_ip), "");
        this.port = prefs.getString(context.getString(R.string.key_port), "");

        Log.i(EletraMCIReadAsyncTask.class.getName(), "SECURITY KEY: " + this.securityKey);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "AUTH KEY: " + this.authKey);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "ENCRIPT KEY: " + this.encryptKey);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "IP: " + this.ip);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "PORT: " + this.port);

        this.lavelTypeKey = prefs.getBoolean(context.getString(R.string.key_leve_security), false);
        this.command = (DLMSCommand) meterComm.getAvailableCommands().get(eletraMCIReadEnum.getReadingCommand());
    }
    public EletraMCIReadAsyncTask(AppCompatActivity context, EletraMCIReadEnum eletraMCIReadEnum, EletraMCIPost postExecute, EletraMCIPre preExecute){
        this.context = context;
        this.postExecute = postExecute;
        this.preExecute = preExecute;
        this.error = 0;

        MeterSchema meterSchema = MeterSchema.PANTHEON_V1;
        meterComm = new MeterComm();
        meterComm.loadSchema(meterSchema, "DLMSTCP");

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.encryptKey = prefs.getString(context.getString(R.string.key_encrypt), "");
        this.authKey = prefs.getString(context.getString(R.string.key_auth), "");
        this.securityKey = Strings.isNullOrEmpty(prefs.getString(context.getString(R.string.key_security), "00000000000000000000000000000000")) ? "00000000000000000000000000000000" : prefs.getString(context.getString(R.string.key_security), "00000000000000000000000000000000");
        this.ip = prefs.getString(context.getString(R.string.key_ip), "");
        this.port = prefs.getString(context.getString(R.string.key_port), "");
        this.lavelTypeKey = prefs.getBoolean(context.getString(R.string.key_leve_security), false);

        Log.i(EletraMCIReadAsyncTask.class.getName(), "SECURITY KEY: " + this.securityKey);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "AUTH KEY: " + this.authKey);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "ENCRIPT KEY: " + this.encryptKey);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "IP: " + this.ip);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "PORT: " + this.port);

        this.command = (DLMSCommand) meterComm.getAvailableCommands().get(eletraMCIReadEnum.getReadingCommand());
    }

    public EletraMCIReadAsyncTask(int logicalAddress, AppCompatActivity context, EletraMCIReadEnum eletraMCIReadEnum, EletraMCIPost postExecute,
                                  EletraMCIPre preExecute){
        this.logicalAddress = logicalAddress;
        this.context = context;
        this.postExecute = postExecute;
        this.preExecute = preExecute;
        this.error = 0;

        MeterSchema meterSchema = MeterSchema.PANTHEON_V1;
        meterComm = new MeterComm();
        meterComm.loadSchema(meterSchema, "DLMSTCP");

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.encryptKey = prefs.getString(context.getString(R.string.key_encrypt), "");
        this.authKey = prefs.getString(context.getString(R.string.key_auth), "");
        this.securityKey = Strings.isNullOrEmpty(prefs.getString(context.getString(R.string.key_security), "00000000000000000000000000000000")) ? "00000000000000000000000000000000" : prefs.getString(context.getString(R.string.key_security), "00000000000000000000000000000000");
        this.ip = prefs.getString(context.getString(R.string.key_ip), "");
        this.port = prefs.getString(context.getString(R.string.key_port), "");
        this.lavelTypeKey = prefs.getBoolean(context.getString(R.string.key_leve_security), false);

        Log.i(EletraMCIReadAsyncTask.class.getName(), "SECURITY KEY: " + this.securityKey);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "AUTH KEY: " + this.authKey);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "ENCRIPT KEY: " + this.encryptKey);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "IP: " + this.ip);
        Log.i(EletraMCIReadAsyncTask.class.getName(), "PORT: " + this.port);

        this.command = (DLMSCommand) meterComm.getAvailableCommands().get(eletraMCIReadEnum.getReadingCommand());
    }
    @Override
    protected void onPreExecute() {
        if (preExecute != null) {
            preExecute.run(EletraMCIReadAsyncTask.this);
        }
        super.onPreExecute();
    }

    private void connectionCancel() {
        try {
            this.meterComm.disconnect();
            meterComm = null;
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (ComPortException e) {
            e.printStackTrace();
        } catch (MeterPhyConnectionException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            if(!Strings.isNullOrEmpty(this.authKey)) {
                ((DLMSConnection) meterComm.getConnection()).setAuthenticationKey(HexString.hexStringToByteArray(this.authKey));
            }
            if(!Strings.isNullOrEmpty(this.encryptKey)) {
                ((DLMSConnection) meterComm.getConnection()).setEncryptKey(HexString.hexStringToByteArray(this.encryptKey));
            }
            if(!Strings.isNullOrEmpty(this.securityKey)) {
                ((DLMSConnection) meterComm.getConnection()).setHLSkey(HexString.hexStringToByteArray(this.securityKey));
            }
            ((DLMSConnection) meterComm.getConnection()).useBroadcast();
            ((DLMSConnection) meterComm.getConnection()).setLogicalAddr(logicalAddress);
            Log.i(EletraMCIReadAsyncTask.class.getName(), "IP: " + this.ip);
            Log.i(EletraMCIReadAsyncTask.class.getName(), "Port: " + this.port);

            /*
            if(meterComm.getConnection() != null) {
                try {
                    meterComm.disconnect();
                    meterComm = null;
                } catch (ComPortException e) {
                    e.printStackTrace();
                } catch (MeterPhyConnectionException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            */
            meterComm.connect(this.ip, Integer.parseInt(this.port), new EletraMCIReadAsyncTask.Listener());

            meterComm.readCommand(this.command);
            this.attributes = ((DLMSAtt) this.command.getAttributes().get(0)).getAtt();

            try {
                meterComm.disconnect();
                meterComm = null;
            } catch (ComPortException e) {
                e.printStackTrace();
            } catch (MeterPhyConnectionException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (ComPortException e) {
            e.printStackTrace();
            this.connectionCancel();
            this.error = R.string.exception_port;
        } catch (ProtocolException e) {
            e.printStackTrace();
            this.connectionCancel();
            this.error = R.string.exception_protocol;
        } catch (Timeout.TimeoutException e) {
            e.printStackTrace();
            this.connectionCancel();
            this.error = R.string.exception_timeout;
        } catch (MeterPhyConnectionException e) {
            e.printStackTrace();
            this.connectionCancel();
            this.error = R.string.exception_connection;
        } catch (Exception e) {
            e.printStackTrace();
            this.connectionCancel();
            this.error = R.string.exception_generic;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if(this.error > 0) {
            //Snackbar.make(context.findViewById(android.R.id.content), this.error, Snackbar.LENGTH_SHORT).show();
        }
        if (postExecute != null) {
            postExecute.run(this.attributes, this.error);
        }
        super.onPostExecute(aVoid);
    }

    private class Listener implements ListenerMCI {
        @Override
        public void handle(Command command) {
            System.out.println();
        }
    }

    public interface EletraMCIPost {
        public void run(List<DLMSAtt> attributes, int error);
    }
    public interface EletraMCIPre {
        public void run(EletraMCIReadAsyncTask task);
    }
}
