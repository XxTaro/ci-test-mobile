package com.eletraenergy.pantheon.mobile.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.ui.adapters.ActionsRecyclerViewAdapter;

import java.util.ArrayList;

public class ActionsFragment extends Fragment {

    protected RecyclerView recyclerView;

    public ActionsFragment() {
        // Required empty public constructor
    }

    public static ActionsFragment newInstance() {
        ActionsFragment fragment = new ActionsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_actions, container, false);
        recyclerView = view.findViewById(R.id.recyclerViewActions);

        ArrayList<ActionsRecyclerViewAdapter.ItemMain> itens = new ArrayList<>();
        ActionsRecyclerViewAdapter.ItemMain item = new ActionsRecyclerViewAdapter.ItemMain();
        item.text = getResources().getString(R.string.relays_controls);
        item.image = getResources().getDrawable(R.drawable.ic_rewind_relays);
        itens.add(item);

        item = new ActionsRecyclerViewAdapter.ItemMain();
        item.text = getResources().getString(R.string.rollback);
        item.image = getResources().getDrawable(R.drawable.baseline_settings_backup_restore_black_24);
        itens.add(item);

        item = new ActionsRecyclerViewAdapter.ItemMain();
        item.text = getResources().getString(R.string.display_pairing);
        item.image = getResources().getDrawable(R.drawable.baseline_dvr_black_24);
        itens.add(item);

        item = new ActionsRecyclerViewAdapter.ItemMain();
        item.text = getResources().getString(R.string.alarm_clearing);
        item.image = getResources().getDrawable(R.drawable.baseline_delete_sweep_black_24);
        itens.add(item);

        ActionsRecyclerViewAdapter adapter = new ActionsRecyclerViewAdapter((AppCompatActivity) this.getActivity(), itens);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(adapter);

        return view;
    }

}
