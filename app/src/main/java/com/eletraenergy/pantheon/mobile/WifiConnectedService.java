package com.eletraenergy.pantheon.mobile;

import android.Manifest;
import android.app.IntentService;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

public class WifiConnectedService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    private int lastLevel;
    public static boolean SERVICE_CONNECTED  = true;
    public static final String UPDATE_NETWORK_CONNECTED = "com.eletraenergy.pantheonweb.mobile.UPDATE_NETWORK_CONNECTED";

    public WifiConnectedService() {
        super("WifiConnectedService");
        this.lastLevel = 0;
        SERVICE_CONNECTED = true;
    }

    public WifiConnectedService(String name) {
        super(name);
        this.lastLevel = 0;
        SERVICE_CONNECTED = true;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        while (SERVICE_CONNECTED) {
            Log.i(WifiConnectedService.class.getSimpleName(), "Verificando intensidade de sinal...");
            sendBroadcast(new Intent(UPDATE_NETWORK_CONNECTED));
            this.counter(5);
        }
    }

    public void counter(int seconds){
        final long startTime = System.currentTimeMillis();
        do {
        } while (System.currentTimeMillis() - startTime <= seconds * 1000);
    }
}
