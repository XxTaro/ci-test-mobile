package com.eletraenergy.pantheon.mobile.ui.adapters;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.ui.AlarmSettingsActivity;

import java.util.List;

/**
 * Created by dcsda on 16/01/2018.
 */

public class AlarmSettingsRecyclerViewAdapter extends RecyclerView.Adapter<AlarmSettingsRecyclerViewAdapter.ViewHolder> {

    protected List< AlarmSettingsItem> itens;
    protected AlarmSettingsActivity context;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView titleTextViewHolder;
        public SwitchCompat switchViewHolder;
        public AppCompatCheckBox checkboxItemHolder;

        public ViewHolder(View view) {
            super(view);
            titleTextViewHolder = (TextView) view.findViewById(android.R.id.text1);
            switchViewHolder = (SwitchCompat) view.findViewById(R.id.switchItemAlarmSettings);
            checkboxItemHolder = (AppCompatCheckBox) view.findViewById(R.id.checkboxItemAlarmSettings);
            checkboxItemHolder.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if(isChecked) {
                    switchViewHolder.setEnabled(isChecked);
                    context.enableItem(getAdapterPosition(), isChecked);
                } else {
                    switchViewHolder.setEnabled(isChecked);
                    context.enableItem(getAdapterPosition(), isChecked);
                }
            });
            switchViewHolder.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if(isChecked) {
                    context.seValueItem(getAdapterPosition(), isChecked);
                } else {
                    context.seValueItem(getAdapterPosition(), isChecked);
                }
            });
        }
    }

    public AlarmSettingsRecyclerViewAdapter(AlarmSettingsActivity context, List< AlarmSettingsItem> itens) {
        this.itens = itens;
        this.context = context;
    }

    @Override
    public AlarmSettingsRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                          int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_alarm_settings, parent, false);
        AlarmSettingsRecyclerViewAdapter.ViewHolder vh = new AlarmSettingsRecyclerViewAdapter.ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(AlarmSettingsRecyclerViewAdapter.ViewHolder holder, int position) {
        AlarmSettingsItem it = this.itens.get(position);
        holder.titleTextViewHolder.setText(it.title);
        holder.switchViewHolder.setChecked(it.value);
        holder.switchViewHolder.setEnabled(false);
    }

    @Override
    public int getItemCount() {
        return itens.size();
    }

    public static class  AlarmSettingsItem {
        public String title;
        public boolean value;
    }
}