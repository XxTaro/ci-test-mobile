package com.eletraenergy.pantheon.mobile.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadTitle;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dcsda on 16/01/2018.
 */

public class CSInformationRecyclerViewAdapter extends RecyclerView.Adapter<CSInformationRecyclerViewAdapter.ViewHolder> {

    protected HashMap<String, String> itensFlags;
    protected List<CSInformationItem> itens;
    protected AppCompatActivity context;
    protected EletraMCIReadTitle eletraMCIReadTitle;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView titleTextViewHolder;
        public TextView valueTextViewHolder;
        public TextView titleTextViewHolderDetail;
        public TextView valueTextViewHolderDetail;
        public TextView firVersionCPU;
        public TextView firFailCPU;
        public TextView rfFailCPU;
        public TextView busFailCPU;
        public TextView dflashFailCPU;
        public TextView invalidMeterCPU;
        public TextView firVersionRC;
        public TextView firFailRC;
        public TextView paramFailRC;
        public TextView rfFailRC;
        public TextView wifiFailRC;
        public TextView dflashFailRC;
        public TextView cpuCommFailRC;
        public ImageButton imageButtonHolder;
        public LinearLayout detailLinearLayoutCPUInformation;
        public LinearLayout detailLinearLayoutRCInformation;
        public LinearLayout disableDetailLayoutSCInformation;
        public LinearLayout enableDetailLayoutSCInformation;

        public ViewHolder(View view) {
            super(view);

            titleTextViewHolder = (TextView) view.findViewById(android.R.id.text1);
            valueTextViewHolder = (TextView) view.findViewById(android.R.id.text2);

            titleTextViewHolderDetail = (TextView) view.findViewById(R.id.textTitle);
            valueTextViewHolderDetail = (TextView) view.findViewById(R.id.textValue);

            firVersionCPU = (TextView) view.findViewById(R.id.firVerText);
            firFailCPU = (TextView) view.findViewById(R.id.firFailText);
            rfFailCPU = (TextView) view.findViewById(R.id.rfFailText);
            busFailCPU = (TextView) view.findViewById(R.id.busFailText);
            dflashFailCPU = (TextView) view.findViewById(R.id.dflashFailText);
            invalidMeterCPU = (TextView) view.findViewById(R.id.invalidMeterText);

            firVersionRC = (TextView) view.findViewById(R.id.firVerTextRC);
            firFailRC = (TextView) view.findViewById(R.id.firFailTextRC);
            paramFailRC = (TextView) view.findViewById(R.id.paramFailTextRC);
            rfFailRC = (TextView) view.findViewById(R.id.rfFailTextRC);
            wifiFailRC = (TextView) view.findViewById(R.id.wifiFailTextRC);
            dflashFailRC = (TextView) view.findViewById(R.id.dflashFailTextRC);
            cpuCommFailRC = (TextView) view.findViewById(R.id.cpuCommFailTextRC);

            detailLinearLayoutCPUInformation = (LinearLayout) view.findViewById(R.id.detailLinearLayoutCPUInformation);
            detailLinearLayoutRCInformation = (LinearLayout) view.findViewById(R.id.detailLinearLayoutRCInformation);
            disableDetailLayoutSCInformation = (LinearLayout) view.findViewById(R.id.disableDetailLayoutSCInformation);
            enableDetailLayoutSCInformation = (LinearLayout) view.findViewById(R.id.enableDetailLayoutSCInformation);

            imageButtonHolder = (ImageButton) view.findViewById(R.id.imageButtonItemCSInformation);
            imageButtonHolder.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
            imageButtonHolder.setVisibility(View.VISIBLE);

            imageButtonHolder.setOnClickListener(v -> {
                CSInformationItem it = CSInformationRecyclerViewAdapter.this.itens.get(getAdapterPosition());
                    if (!it.isOpen) {
                        if (it.title.equals("Estado Geral da CPU")){
                            imageButtonHolder.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                            detailLinearLayoutCPUInformation.setVisibility(View.VISIBLE);
                            CSInformationRecyclerViewAdapter.this.itens.get(getAdapterPosition()).isOpen = true;
                        }
                        if (it.title.equals("Estado Geral do RC")){
                            imageButtonHolder.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                            detailLinearLayoutRCInformation.setVisibility(View.VISIBLE);
                            CSInformationRecyclerViewAdapter.this.itens.get(getAdapterPosition()).isOpen = true;
                        }
                    } else {
                        if (it.title.equals("Estado Geral da CPU")){
                            imageButtonHolder.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                            detailLinearLayoutCPUInformation.setVisibility(View.GONE);
                            CSInformationRecyclerViewAdapter.this.itens.get(getAdapterPosition()).isOpen = false;
                        }
                        if(it.title.equals("Estado Geral do RC")){
                            imageButtonHolder.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                            detailLinearLayoutRCInformation.setVisibility(View.GONE);
                            CSInformationRecyclerViewAdapter.this.itens.get(getAdapterPosition()).isOpen = false;
                        }

                    }
            });
        }
    }

    public CSInformationRecyclerViewAdapter(AppCompatActivity context, List<CSInformationItem> itens, HashMap<String, String> itensFlags) {
        this.itens = itens;
        this.itensFlags = itensFlags;
        this.context = context;
        this.eletraMCIReadTitle = new EletraMCIReadTitle();
    }

    @Override
    public CSInformationRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                          int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_cs_information, parent, false);
        CSInformationRecyclerViewAdapter.ViewHolder vh = new CSInformationRecyclerViewAdapter.ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(CSInformationRecyclerViewAdapter.ViewHolder holder, int position) {
        CSInformationItem it = this.itens.get(position);
        holder.titleTextViewHolder.setText(it.title);
        holder.valueTextViewHolder.setText(it.value);

        if(it.title.equals("Estado Geral da CPU")){
            holder.disableDetailLayoutSCInformation.setVisibility(View.GONE);
            holder.enableDetailLayoutSCInformation.setVisibility(View.VISIBLE);
            holder.detailLinearLayoutCPUInformation.setVisibility(View.GONE);

            holder.titleTextViewHolderDetail.setText(it.title);
            holder.valueTextViewHolderDetail.setText(it.value);
        }
        if(it.title.equals("Estado Geral do RC")){
            holder.disableDetailLayoutSCInformation.setVisibility(View.GONE);
            holder.enableDetailLayoutSCInformation.setVisibility(View.VISIBLE);
            holder.detailLinearLayoutRCInformation.setVisibility(View.GONE);

            holder.titleTextViewHolderDetail.setText(it.title);
            holder.valueTextViewHolderDetail.setText(it.value);
        }

        for(Map.Entry<String, String> item : itensFlags.entrySet()){
            switch (item.getKey()){
                case "programVersion":
                    holder.firVersionCPU.setText(item.getValue());
                    break;
                case "firmwareFailCPU":
                    holder.firFailCPU.setText(item.getValue());
                    break;
                case "rfFailCPU":
                    holder.rfFailCPU.setText(item.getValue());
                    break;
                case "busFailCPU":
                    holder.busFailCPU.setText(item.getValue());
                    break;
                case "dflashFailCPU":
                    holder.dflashFailCPU.setText(item.getValue());
                    break;
                case "invalidMeterCPU":
                    holder.invalidMeterCPU.setText(item.getValue());
                    break;

                case "firmwareVersion":
                    holder.firVersionRC.setText(item.getValue());
                    break;
                case "firmwareFailRC":
                    holder.firFailRC.setText(item.getValue());
                    break;
                case "paramFailRC":
                    holder.paramFailRC.setText(item.getValue());
                    break;
                case "rfFailRC":
                    holder.rfFailRC.setText(item.getValue());
                    break;
                case "wifiFailRC":
                    holder.wifiFailRC.setText(item.getValue());
                    break;
                case "dflashFailRC":
                    holder.dflashFailRC.setText(item.getValue());
                    break;
                case "cpuCommFailRC":
                    holder.cpuCommFailRC.setText(item.getValue());
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return itens.size();
    }

    public static class CSInformationItem {
        public String title;
        public String value;

        public boolean isOpen = false;
        public boolean isEnable = false;

        @Override
        public String toString() {
            return "CPUInformationItem{" +
                    "title='" + title + '\'' +
                    ", value='" + value + '\'' +
                    '}';
        }
    }
}