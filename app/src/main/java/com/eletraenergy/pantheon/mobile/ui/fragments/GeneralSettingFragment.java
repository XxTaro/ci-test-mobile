package com.eletraenergy.pantheon.mobile.ui.fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.preference.EditTextPreference;
import androidx.preference.PreferenceFragmentCompat;

import com.eletraenergy.pantheon.mobile.R;


public class GeneralSettingFragment extends PreferenceFragmentCompat {

    protected EditTextPreference digitsDecimal;

    protected EditTextPreference digitsInteger;

    protected EditTextPreference waitTime;

    protected SharedPreferences prefs;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.pref_fragmented_general, rootKey);

        FragmentActivity activity = getActivity();
        prefs = PreferenceManager.getDefaultSharedPreferences(activity);

        digitsDecimal = (EditTextPreference) findPreference(activity.getString(R.string.key_decimal_digits));
        digitsDecimal.setSummary(prefs.getString(activity.getString(R.string.key_decimal_digits), "2"));
        digitsDecimal.setOnPreferenceChangeListener((preference, o) -> {
            try {
                int op = Integer.parseInt(o.toString());
                if (op > -1 && op < 3) {
                    prefs.edit().putString(activity.getString(R.string.key_decimal_digits), o.toString()).commit();
                    digitsDecimal.setSummary(o.toString());
                    digitsDecimal.setText(o.toString());
                } else {
                    digitsDecimal.setSummary(activity.getString(R.string.error_digits));
                }
            } catch (Exception e) {
                digitsDecimal.setSummary(activity.getString(R.string.error_digits));
            }
            return false;
        });
        digitsDecimal.setOnBindEditTextListener(new EditTextPreference.OnBindEditTextListener() {
            @Override
            public void onBindEditText(@NonNull EditText editText) {
                editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
            }
        });

        digitsInteger = (EditTextPreference) findPreference(activity.getString(R.string.key_integer_digits));
        digitsInteger.setSummary(prefs.getString(activity.getString(R.string.key_integer_digits), "2"));
        digitsInteger.setOnPreferenceChangeListener((preference, o) -> {
            try {
                int op = Integer.parseInt(o.toString());
                if (op > 4 && op < 7) {
                    prefs.edit().putString(activity.getString(R.string.key_integer_digits), o.toString()).commit();
                    digitsInteger.setSummary(o.toString());
                    digitsInteger.setText(o.toString());
                } else {
                    digitsInteger.setSummary(activity.getString(R.string.error_digits));
                }
            } catch (Exception e) {
                digitsInteger.setSummary(activity.getString(R.string.error_digits));
            }
            return false;
        });
        digitsInteger.setOnBindEditTextListener(new EditTextPreference.OnBindEditTextListener() {
            @Override
            public void onBindEditText(@NonNull EditText editText) {
                editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
            }
        });

        waitTime = (EditTextPreference) findPreference(activity.getString(R.string.key_wait_time));
        waitTime.setSummary(prefs.getString(activity.getString(R.string.key_wait_time), "20"));
        waitTime.setOnPreferenceChangeListener((preference, o) -> {
            try {
                int op = Integer.parseInt(o.toString());
                if (op >= 0 && op <= 60) {
                    prefs.edit().putString(activity.getString(R.string.key_wait_time), o.toString()).commit();
                    waitTime.setSummary(o.toString());
                    waitTime.setText(o.toString());
                } else {
                    waitTime.setSummary(activity.getString(R.string.error_interval_limit));
                }
            } catch (Exception e) {
                waitTime.setSummary(activity.getString(R.string.error_digits));
            }
            return false;
        });
        waitTime.setOnBindEditTextListener(new EditTextPreference.OnBindEditTextListener() {
            @Override
            public void onBindEditText(@NonNull EditText editText) {
                editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
                editText.clearFocus();
            }
        });
    }

    public String getWaitTime() {
        return waitTime.getText();
    }
}