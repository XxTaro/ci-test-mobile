package com.eletraenergy.pantheon.mobile.ui;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.UsbService;
import com.eletraenergy.pantheon.mobile.util.CRC32;
import com.eletraenergy.pantheon.mobile.util.Checksum;
import com.eletraenergy.pantheon.mobile.util.ChecksumFF;
import com.eletraenergy.pantheon.mobile.util.HexString;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.common.base.Strings;

public class DisplayPairingActivity extends PantheonCompatActivity {

    protected Toolbar toolbar;
    protected RelativeLayout relativeLayoutError;
    protected LinearLayout linearLayout;
    protected LinearLayout serialLinearLayout;
    protected TextView textView;
    protected Button button;
    protected Button sendButton;
    protected AlertDialog progressWrite;

    protected EditText editTextDisplay;
    protected EditText editTextMeter;

    protected TextView textViewUsbSerial;
    protected TextView textViewUsbSerialStatus;
    protected ImageView imageView;
    protected TextView textViewLog;

    private UsbService usbService;
    private BroadcastReceiver usbReceiver;
    private Intent intentService;
    private ServiceConnection usbConnection;

    private DisplayPairingAsyncTask displayPairingAsyncTask;

    private static final int DISPLAY_NUMBER_SIZE = 10;
    private static final int METER_NUMBER_SIZE = 10;

    private final byte beginFrame[] = {(byte)0x7E, (byte)0x00, (byte)0x05, (byte)0x08, (byte)0x01, (byte)0x41, (byte)0x54, (byte)0x01, (byte)0x60};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_pairing);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);

        linearLayout = (LinearLayout) findViewById(R.id.linearLayoutDisplayPairing);
        serialLinearLayout = (LinearLayout) findViewById(R.id.serialLinearLayoutDisplayPairing);
        relativeLayoutError = (RelativeLayout) findViewById(R.id.relativeLayoutErrorDisplayPairing);
        textView = (TextView) findViewById(R.id.textViewDisplayPairing);
        sendButton = (Button) findViewById(R.id.sendButton);
        button = (Button) findViewById(R.id.buttonDisplayPairing);
        textViewUsbSerial = (TextView) findViewById(android.R.id.text1);
        textViewUsbSerialStatus = (TextView) findViewById(android.R.id.text2);
        textViewUsbSerial.setText("USB Serial");
        textViewLog = (TextView) findViewById(R.id.logTextViewPairing);

        imageView = (ImageView) findViewById(R.id.imageViewDisplayPairing);
        editTextDisplay = (EditText) findViewById(R.id.displayEditTextDisplayPairing);
        editTextMeter = (EditText) findViewById(R.id.meterEditTextDisplayPairing);
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbService.ACTION_USB_PERMISSION_GRANTED);
        filter.addAction(UsbService.ACTION_NO_USB);
        filter.addAction(UsbService.ACTION_USB_DISCONNECTED);
        filter.addAction(UsbService.ACTION_USB_NOT_SUPPORTED);
        filter.addAction(UsbService.ACTION_USB_PERMISSION_NOT_GRANTED);

        usbReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i(MainActivity.class.getSimpleName(), "Atualizando usb serial...");
                switch (intent.getAction()) {
                    case UsbService.ACTION_USB_PERMISSION_GRANTED: // USB PERMISSION GRANTED
                            textViewUsbSerialStatus.setText(R.string.usb_ready);
                            imageView.setColorFilter(ContextCompat.getColor(context, android.R.color.holo_green_dark), android.graphics.PorterDuff.Mode.MULTIPLY);
                            sendButton.setEnabled(true);
                            editTextDisplay.setEnabled(true);
                            editTextMeter.setEnabled(true);
                        break;
                    case UsbService.ACTION_USB_PERMISSION_NOT_GRANTED: // USB PERMISSION NOT GRANTED
                            textViewUsbSerialStatus.setText(R.string.usb_not_granted);
                            imageView.setColorFilter(ContextCompat.getColor(context, android.R.color.holo_red_dark), android.graphics.PorterDuff.Mode.MULTIPLY);
                            sendButton.setEnabled(false);
                            editTextDisplay.setEnabled(false);
                            editTextMeter.setEnabled(false);
                        break;
                    case UsbService.ACTION_NO_USB: // NO USB CONNECTED
                            textViewUsbSerialStatus.setText(R.string.no_usb_connected);
                            imageView.setColorFilter(ContextCompat.getColor(context, android.R.color.holo_red_dark), android.graphics.PorterDuff.Mode.MULTIPLY);
                            sendButton.setEnabled(false);
                            editTextDisplay.setEnabled(false);
                            editTextMeter.setEnabled(false);
                        break;
                    case UsbService.ACTION_USB_DISCONNECTED: // USB DISCONNECTED
                            textViewUsbSerialStatus.setText(R.string.usb_disconnected);
                            imageView.setColorFilter(ContextCompat.getColor(context, android.R.color.holo_red_dark), android.graphics.PorterDuff.Mode.MULTIPLY);
                            sendButton.setEnabled(false);
                            editTextDisplay.setEnabled(false);
                            editTextMeter.setEnabled(false);
                        break;
                    case UsbService.ACTION_USB_NOT_SUPPORTED: // USB NOT SUPPORTED
                            textViewUsbSerialStatus.setText(R.string.usb_not_supported);
                            imageView.setColorFilter(ContextCompat.getColor(context, android.R.color.holo_red_dark), android.graphics.PorterDuff.Mode.MULTIPLY);
                            sendButton.setEnabled(false);
                            editTextDisplay.setEnabled(false);
                            editTextMeter.setEnabled(false);
                        break;
                }
            }
        };
        usbConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName arg0, IBinder arg1) {
                usbService = ((UsbService.UsbBinder) arg1).getService();
            }

            @Override
            public void onServiceDisconnected(ComponentName arg0) {
                usbService = null;
            }
        };
        intentService = new Intent(this, UsbService .class);

        this.registerReceiver(this.usbReceiver, filter);
        this.bindService(intentService, usbConnection, Context.BIND_AUTO_CREATE);

    }

    @Override
    public void onPause() {
        super.onPause();
        this.unregisterReceiver(this.usbReceiver);
        this.stopService(this.intentService);
    }

    protected void showButtonErro(int err) {
        if(err == R.string.error_level) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(DisplayPairingActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_auth) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(DisplayPairingActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_encrypt) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(DisplayPairingActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_security) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(DisplayPairingActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_ip) {
            button.setText(R.string.bt_communication_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(DisplayPairingActivity.this, CommunicationSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_port) {
            button.setText(R.string.bt_communication_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(DisplayPairingActivity.this, CommunicationSettingActivity.class));
            });
            return;
        }
        button.setText(R.string.bt_retry);
        button.setVisibility(View.VISIBLE);
        button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_refresh_black_24dp), null, null, null);
        button.setOnClickListener(v -> {
            relativeLayoutError.setVisibility(View.GONE);
        });
        return;
    }

    @Override
    public boolean onSupportNavigateUp(){
        this.onBackPressed();
        return true;
    }

    @Override
    public void onDestroy() {
        if(displayPairingAsyncTask != null) displayPairingAsyncTask.cancel(true);
        super.onDestroy();
    }

    private boolean isValid() {
        boolean res = true;
        if(Strings.isNullOrEmpty(editTextDisplay.getText().toString())) {
            Toast.makeText(this, R.string.valid_display, Toast.LENGTH_SHORT).show();
            res = false;
            return res;
        }
        if(Strings.isNullOrEmpty(editTextMeter.getText().toString())) {
            Toast.makeText(this, R.string.valid_meter, Toast.LENGTH_SHORT).show();
            res = false;
            return res;
        }
        if(editTextDisplay.getText().toString().length() < DISPLAY_NUMBER_SIZE) {
            Toast.makeText(this, R.string.valid_display, Toast.LENGTH_SHORT).show();
            res = false;
            return res;
        }
        if(editTextMeter.getText().toString().length() < METER_NUMBER_SIZE) {
            Toast.makeText(this, R.string.valid_meter, Toast.LENGTH_SHORT).show();
            res = false;
            return res;
        }
        if(editTextDisplay.getText().toString().trim().equals("0000000000")) {
            Toast.makeText(this, R.string.valid_display, Toast.LENGTH_SHORT).show();
            res = false;
            return res;
        }
        if(editTextMeter.getText().toString().trim().equals("0000000000")) {
            Toast.makeText(this, R.string.valid_meter, Toast.LENGTH_SHORT).show();
            res = false;
            return res;
        }
        return res;
    }

    public void showPairingDialog(View view) {
        if(isValid()) {

            AlertDialog.Builder builder = new AlertDialog.Builder(DisplayPairingActivity.this);
            builder.setTitle(getString(R.string.warning));
            builder.setMessage(R.string.app_confirm_pairing);
            builder.setIcon(R.drawable.eletra_icon);
            builder.setView(R.layout.dialog_pairing_confirm);
            builder.setPositiveButton(getString(R.string.yes),
                    (dialog, which) -> {
                        //sendDisplayPairingCommand();
                        CheckBox checkBox = ((AlertDialog) dialog).findViewById(R.id.confirmCheckBoxPairing);
                        if(checkBox.isChecked()){
                            displayPairingAsyncTask = new DisplayPairingAsyncTask();
                            displayPairingAsyncTask.execute();
                        } else {
                            Toast.makeText(this, R.string.action_canceled, Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    });
            builder.setNegativeButton(getString(R.string.no),
                    (dialog, which) -> dialog.dismiss());
            AlertDialog dialog = builder.create();
            dialog.show();

            TextView display = (TextView) dialog.findViewById(R.id.displayTextViewPairing);
            display.setText(editTextDisplay.getText());
            TextView meter = (TextView) dialog.findViewById(R.id.meterTextViewPairing);
            meter.setText(editTextMeter.getText());
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);

            CheckBox checkBox = ((AlertDialog) dialog).findViewById(R.id.confirmCheckBoxPairing);
            checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if(isChecked) {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                } else {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                }
            });
        }
    }

    public static byte[] prepareFrequencyFrame(byte meterNumber[]) {
        byte retval[] = new byte[]{(byte)0x7E, (byte)0x00, (byte)0x06, (byte)0x08, (byte)0x01, (byte)0x48, (byte)0x41, (byte)0x24, (byte)0x04, (byte)0x45};
        retval[7] = meterNumber[3];
        retval[8] = meterNumber[4];
        retval[retval.length-1] = ChecksumFF.calc(retval, 3, 6);
        Log.i(DisplayPairingActivity.class.getSimpleName(),"Pairing Frequency: "+ HexString.fromBytes(retval));
        return retval;
    }

    public static void add0x33(byte[] data) {
        for (int i = 0; i < data.length; ++i) {
            data[i] += 0x33;
        }
    }

    public static void reverse(byte[] data) {
        int left = 0;
        int right = data.length - 1;

        while( left < right ) {
            // swap the values at the left and right indices
            byte temp = data[left];
            data[left] = data[right];
            data[right] = temp;

            // move the left and right index pointers in toward the center
            left++;
            right--;
        }
    }

    public class DisplayPairingAsyncTask extends AsyncTask<Void, String, Void> {
        byte disp[];
        byte meter[];
        @Override
        protected void onPreExecute() {
            disp = HexString.toBytes(editTextDisplay.getText().toString().trim());
            meter = HexString.toBytes(editTextMeter.getText().toString().trim());

            sendButton.setEnabled(false);
            AlertDialog.Builder builder = new AlertDialog.Builder(DisplayPairingActivity.this);
            LayoutInflater inflater = LayoutInflater.from(DisplayPairingActivity.this);
            View view = inflater.inflate(R.layout.dialog_progress, null, false);
            ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBarProgress);
            TextView textView = (TextView) view.findViewById(R.id.textViewProgress);
            textView.setText(R.string.processing_display_pairing);
            CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(DisplayPairingActivity.this);
            circularProgressDrawable.setStyle(CircularProgressDrawable.LARGE);
            circularProgressDrawable.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
            progressBar.setIndeterminateDrawable(circularProgressDrawable);
            builder.setView(view);
            builder.setCancelable(false);
            DisplayPairingActivity.this.progressWrite = builder.create();
            DisplayPairingActivity.this.progressWrite.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            publishProgress("Iniciando...");
            publishProgress("Enviando frame de início...");
            usbService.changeBaudRate(4800);
            Log.i(DisplayPairingActivity.class.getSimpleName(),"Pairing Begin Frame: "+ HexString.fromBytes(beginFrame));
            usbService.write(beginFrame);
            countSeconds((float) 2);

            publishProgress("Enviando frame de frequencia...");
            Log.i(DisplayPairingActivity.class.getSimpleName(),"Frequency Frame: "+ HexString.fromBytes(beginFrame));
            usbService.write(prepareFrequencyFrame(new byte[]{0x00, 0x00, 0x00, 0x00, 0x00}));
            countSeconds((float) 2);

            byte buffer[] = new byte[28];
            System.arraycopy(disp, 0, buffer, 12, disp.length);
            System.arraycopy(meter, 0, buffer, 12+disp.length, meter.length);
            byte[] crc32 = CRC32.calc(buffer, 12, meter.length + disp.length);
            reverse(meter);
            reverse(disp);
            reverse(crc32);
            add0x33(meter);
            add0x33(disp);
            add0x33(crc32);
            buffer[0] = 0x68;
            buffer[1] = 0x00;
            buffer[2] = 0x00;
            buffer[3] = 0x00;
            buffer[4] = 0x00;
            buffer[5] = 0x00;
            buffer[6] = 0x00;
            buffer[7] = 0x68;
            buffer[8] = 0x1F;
            buffer[9] = 0x10;
            buffer[10] = (byte)(0x10 + 0x33);
            buffer[11] = (byte)(0xD0 + 0x33);
            System.arraycopy(disp, 0, buffer, 12, disp.length);
            System.arraycopy(meter, 0, buffer, 12+disp.length, meter.length);
            System.arraycopy(crc32, 0, buffer, 12+disp.length+meter.length, crc32.length);
            buffer[26] = Checksum.calc(buffer, 0, buffer.length-2);
            buffer[27] = 0x16;

            publishProgress("Enviando frame de pareamento...");
            Log.i(DisplayPairingActivity.class.getSimpleName(),"Pairing Data Frame: "+ HexString.fromBytes(buffer));
            usbService.write(buffer);
            countSeconds((float) 2);
            return null;
        }

        @Override
        protected void onProgressUpdate(String... params){
            //Codigo
            textViewLog.setText(params[0]);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if(DisplayPairingActivity.this.progressWrite != null) DisplayPairingActivity.this.progressWrite.dismiss();
            sendButton.setEnabled(true);
            if (usbService != null) { // if UsbService was correctly binded, Send data
                textViewLog.setText("USBService Ativo.");
            } else {
                textViewLog.setText("USBService Inativo.");
            }
            Snackbar.make(toolbar, getString(R.string.success_action), Snackbar.LENGTH_SHORT).show();
            super.onPostExecute(aVoid);
        }

        private void countSeconds(float seconds) {
            final long startTime = System.currentTimeMillis();
            float timeTotal = seconds;
            do {
            } while (System.currentTimeMillis() - startTime <= timeTotal * 1000);
        }
    }
}
