package com.eletraenergy.pantheon.mobile.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentActivity;

import com.eletraenergy.pantheon.mobile.R;
import com.google.android.material.appbar.AppBarLayout;

public class GeneralSettingActivity extends PantheonCompatActivity {
    protected Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general_settings);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);

        //getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutSetting, new GeneralSettingFragment()).commit();
    }
    @Override
    public boolean onSupportNavigateUp(){
        this.onBackPressed();
        return true;
    }
}
