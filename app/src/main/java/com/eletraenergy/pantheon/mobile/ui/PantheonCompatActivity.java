package com.eletraenergy.pantheon.mobile.ui;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.KeyEvent;

import androidx.appcompat.app.AppCompatActivity;

import com.eletraenergy.pantheon.mobile.R;
import com.google.common.base.Strings;

public class PantheonCompatActivity extends AppCompatActivity {

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    protected int isConfiguredAddress(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String ip = prefs.getString(this.getString(R.string.key_ip), "");
        String port = prefs.getString(this.getString(R.string.key_port), "");

        if(Strings.isNullOrEmpty(ip)) {
            //Snackbar.make(this.findViewById(android.R.id.content), R.string.error_security, Snackbar.LENGTH_SHORT).show();
            return R.string.error_ip;
        }
        if(Strings.isNullOrEmpty(port)) {
            //Snackbar.make(this.findViewById(android.R.id.content), R.string.error_encrypt, Snackbar.LENGTH_SHORT).show();
            return R.string.error_port;
        }
        return 0;
    }

    protected int isHightLevel(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String encryptKey = prefs.getString(this.getString(R.string.key_encrypt), "");
        String authKey = prefs.getString(this.getString(R.string.key_auth), "");
        String securityKey = prefs.getString(this.getString(R.string.key_security), "");

        boolean lavelTaypeKey = prefs.getBoolean(this.getString(R.string.key_leve_security), false);

        if(lavelTaypeKey) {
            /*
            if(Strings.isNullOrEmpty(securityKey)) {
                //Snackbar.make(this.findViewById(android.R.id.content), R.string.error_security, Snackbar.LENGTH_SHORT).show();
                return R.string.error_security;
            }
            */
            if(Strings.isNullOrEmpty(encryptKey)) {
                //Snackbar.make(this.findViewById(android.R.id.content), R.string.error_encrypt, Snackbar.LENGTH_SHORT).show();
                return R.string.error_encrypt;
            }
            if(Strings.isNullOrEmpty(authKey)) {
                //Snackbar.make(this.findViewById(android.R.id.content), R.string.error_auth, Snackbar.LENGTH_SHORT).show();
                return R.string.error_auth;
            }
            return this.isConfiguredAddress();
        } else {
            //Snackbar.make(this.findViewById(android.R.id.content), R.string.error_level, Snackbar.LENGTH_SHORT).show();
            return R.string.error_level;
        }
    }

    protected int isLowLevel(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String securityKey = prefs.getString(this.getString(R.string.key_security), "");
        if(Strings.isNullOrEmpty(securityKey)) {
            //Snackbar.make(this.findViewById(android.R.id.content), R.string.error_security, Snackbar.LENGTH_SHORT).show();
            return R.string.error_security;
        }
        return this.isConfiguredAddress();
    }
}
