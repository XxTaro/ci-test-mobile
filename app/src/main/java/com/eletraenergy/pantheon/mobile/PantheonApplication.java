package com.eletraenergy.pantheon.mobile;

import android.app.Application;
import android.content.Context;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDex;

import com.newrelic.agent.android.NewRelic;


public class PantheonApplication extends Application {
    // This flag should be set to true to enable VectorDrawable support for API < 21
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate(){
        super.onCreate();

        NewRelic.withApplicationToken(
                "AA51637d86082fb0346dcd95cf5a4bff51a75e07a2"
        ).start(this);
    }
}