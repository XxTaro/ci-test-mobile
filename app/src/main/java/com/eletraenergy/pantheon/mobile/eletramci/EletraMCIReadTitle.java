package com.eletraenergy.pantheon.mobile.eletramci;

import com.eletraenergy.pantheon.mobile.R;

import java.util.HashMap;

/**
 * Created by dcsda on 16/01/2018.
 */

public class EletraMCIReadTitle {

    protected HashMap<String, Integer> titles = new HashMap<>();

    public EletraMCIReadTitle() {
        this.titles = new HashMap<String, Integer>();

        this.titles.put(EletraMCIReadEnum.CPU_INFORMATION.getReadingCommand(), R.string.attr_cpu_info);
        this.titles.put(EletraMCIReadEnum.RC_INFORMATION.getReadingCommand(), R.string.attr_sc_info);
        this.titles.put(EletraMCIReadEnum.SOFTWARE_VERSION.getReadingCommand(), R.string.attr_software_version);
        this.titles.put(EletraMCIReadEnum.CPU_ADDRESS.getReadingCommand(), R.string.attr_serial_number);
        this.titles.put(EletraMCIReadEnum.CPU_DATE_TIME.getReadingCommand(), R.string.attr_cpu_date_time);
        this.titles.put(EletraMCIReadEnum.DIGITAL_INPUT_STATUS.getReadingCommand(), R.string.attr_port_status);
        this.titles.put(EletraMCIReadEnum.DIGITAL_INPUT_STATUS.getReadingCommand(), R.string.attr_port_status);
        this.titles.put(EletraMCIReadEnum.COLLECTIVE_OPEN_RELAY_STATUS.getReadingCommand(), R.string.attr_collective_open_rele_status);
        this.titles.put(EletraMCIReadEnum.COLLECTIVE_CLOSE_RELAY_STATUS.getReadingCommand(), R.string.attr_collective_close_rele_status);
        this.titles.put(EletraMCIReadEnum.INSTALLATION_STATUS.getReadingCommand(), R.string.attr_instalation_status);
        this.titles.put(EletraMCIReadEnum.CONSUMER_UNIT.getReadingCommand(), R.string.attr_consumer_units);
        this.titles.put(EletraMCIReadEnum.RELAY_OPERATION_ACTION.getReadingCommand(), R.string.attr_relay_operation_action);
        this.titles.put(EletraMCIReadEnum.METER_AUTO_DETECTION.getReadingCommand(), R.string.attr_meter_auto_detection_information);
        this.titles.put(EletraMCIReadEnum.CPU_METER_LIST.getReadingCommand(), R.string.attr_cpu_meter_list);
        this.titles.put(EletraMCIReadEnum.METERS_SPECIFICATION.getReadingCommand(), R.string.attr_meters_specification);

        this.titles.put(EletraMCIReadEnum.RELAY_STATUS.getReadingCommand(), R.string.attr_relays_status);
        this.titles.put(EletraMCIReadEnum.DIRECT_ACTIVE_ENERGY.getReadingCommand(), R.string.attr_direct_active_energy);
        this.titles.put(EletraMCIReadEnum.REVERSE_ACTIVE_ENERGY.getReadingCommand(), R.string.attr_reverse_active_energy);
        this.titles.put(EletraMCIReadEnum.VOLTAGE.getReadingCommand(), R.string.attr_voltage);
        this.titles.put(EletraMCIReadEnum.CURRENT.getReadingCommand(), R.string.attr_current);
        this.titles.put(EletraMCIReadEnum.POWER.getReadingCommand(), R.string.attr_active_power);
        this.titles.put(EletraMCIReadEnum.UPDATE_METER_FILE.getReadingCommand(), R.string.attr_meter_file_update);
        this.titles.put(EletraMCIReadEnum.CONTROL_PARAMETER.getReadingCommand(), R.string.attr_control_parameter);
        this.titles.put(EletraMCIReadEnum.FLAGS.getReadingCommand(), R.string.attr_flag);
        this.titles.put(EletraMCIReadEnum.CPU_GENERAL_STATE.getReadingCommand(), R.string.sc_cpu_general_state);
        this.titles.put(EletraMCIReadEnum.METER_NUM.getReadingCommand(), R.string.attr_meter_num);
        this.titles.put(EletraMCIReadEnum.METER_POS.getReadingCommand(), R.string.attr_meter_pos);
        this.titles.put(EletraMCIReadEnum.METER_TYPE.getReadingCommand(), R.string.attr_meter_type);

        this.titles.put(EletraMCIReadEnum.RC_GENERAL_STATE.getReadingCommand(), R.string.sc_rc_general_state);
    }

    public int getResTitle(String command) {
        return this.titles.get(command);
    }

}
