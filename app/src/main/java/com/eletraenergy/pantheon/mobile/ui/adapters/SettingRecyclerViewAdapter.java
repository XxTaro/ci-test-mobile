package com.eletraenergy.pantheon.mobile.ui.adapters;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.ui.CommunicationSettingActivity;
import com.eletraenergy.pantheon.mobile.ui.CredentialSettingActivity;
import com.eletraenergy.pantheon.mobile.ui.GeneralSettingActivity;
import com.eletraenergy.pantheon.mobile.ui.InfoActivity;
import com.eletraenergy.pantheon.mobile.ui.LicenseSettingActivity;

import java.util.ArrayList;

/**
 * Created by dcsda on 10/01/2018.
 */

public class SettingRecyclerViewAdapter extends RecyclerView.Adapter<SettingRecyclerViewAdapter.ViewHolder> {

    protected ArrayList<ItemSetting> itens;
    protected AppCompatActivity context;

    public class ViewHolder extends RecyclerView.ViewHolder implements AdapterView.OnClickListener {
        public TextView textViewHolder;
        public TextView descriptionTextViewHolder;
        public ImageView imageViewHolder;
        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            textViewHolder = (TextView) view.findViewById(R.id.textViewItemListSetting);
            descriptionTextViewHolder = (TextView) view.findViewById(R.id.descTextViewItemListSetting);
            imageViewHolder = (ImageView) view.findViewById(R.id.imageViewItemListSetting);
        }

        @Override
        public void onClick(View v) {
            if(getAdapterPosition() == 0) {
                Intent intent = new Intent(SettingRecyclerViewAdapter.this.context, GeneralSettingActivity.class);
                SettingRecyclerViewAdapter.this.context.startActivity(intent);
            }
            if(getAdapterPosition() == 1) {
                Intent intent = new Intent(SettingRecyclerViewAdapter.this.context, CredentialSettingActivity.class);
                SettingRecyclerViewAdapter.this.context.startActivity(intent);
            }
            if(getAdapterPosition() == 2) {
                Intent intent = new Intent(SettingRecyclerViewAdapter.this.context, LicenseSettingActivity.class);
                SettingRecyclerViewAdapter.this.context.startActivity(intent);
            }
            if(getAdapterPosition() == 3) {
                Intent intent = new Intent(SettingRecyclerViewAdapter.this.context, CommunicationSettingActivity.class);
                SettingRecyclerViewAdapter.this.context.startActivity(intent);
            }
            if(getAdapterPosition() == 4) {
                Intent intent = new Intent(SettingRecyclerViewAdapter.this.context, InfoActivity.class);
                SettingRecyclerViewAdapter.this.context.startActivity(intent);
            }
        }
    }
    public SettingRecyclerViewAdapter(AppCompatActivity context, ArrayList<ItemSetting> itens) {
        this.itens = itens;
        this.context = context;
    }

    @Override
    public SettingRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                    int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_setting, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ItemSetting it = this.itens.get(position);
        holder.textViewHolder.setText(it.text);
        holder.imageViewHolder.setImageDrawable(it.image);
        holder.descriptionTextViewHolder.setText(it.description);
    }
    @Override
    public int getItemCount() {
        return itens.size();
    }

    public static class ItemSetting {
        public Drawable image;
        public String text;
        public String description;
    }
}
