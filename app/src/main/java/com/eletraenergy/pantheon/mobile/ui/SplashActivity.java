package com.eletraenergy.pantheon.mobile.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.eletraenergy.pantheon.mobile.R;
import com.google.common.base.Strings;

public class SplashActivity extends PantheonCompatActivity {

    protected SplashAsyncTask splashAsyncTask;
    protected ProgressBar progressBarSplash;
    protected TextView versionTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        PackageInfo pinfo = null;
        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        int versionNumber = pinfo.versionCode;
        String versionName = pinfo.versionName;

        versionTextView = (TextView) findViewById(R.id.versionTextView);
        versionTextView.setText(getString(R.string.version) + " " + versionName);

        progressBarSplash = (ProgressBar) findViewById(R.id.progressBarSplash);
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(SplashActivity.this);
        circularProgressDrawable.setStyle(CircularProgressDrawable.LARGE);
        circularProgressDrawable.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        progressBarSplash.setIndeterminateDrawable(circularProgressDrawable);
        //progressBarSplash.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        splashAsyncTask = new SplashAsyncTask();
        splashAsyncTask.execute();
    }

    @Override
    public void onDestroy() {
        if (splashAsyncTask != null) splashAsyncTask.cancel(true);
        super.onDestroy();
    }

    protected class SplashAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);
            if(Strings.isNullOrEmpty(prefs.getString(SplashActivity.this.getString(R.string.key_ip), ""))) {
                prefs.edit().putString(SplashActivity.this.getString(R.string.key_ip), "11.11.11.254").commit();
            }
            if(Strings.isNullOrEmpty(prefs.getString(SplashActivity.this.getString(R.string.key_decimal_digits), ""))) {
                prefs.edit().putString(SplashActivity.this.getString(R.string.key_decimal_digits), "2").commit();
            }
            if(Strings.isNullOrEmpty(prefs.getString(SplashActivity.this.getString(R.string.key_integer_digits), ""))) {
                prefs.edit().putString(SplashActivity.this.getString(R.string.key_integer_digits), "6").commit();
            }
            if(Strings.isNullOrEmpty(prefs.getString(SplashActivity.this.getString(R.string.key_port), ""))) {
                prefs.edit().putString(SplashActivity.this.getString(R.string.key_port), "2000").commit();
            }
            if(prefs.getString(SplashActivity.this.getString(R.string.key_ip), "").equals("11.11.11.254") &&
                    prefs.getString(SplashActivity.this.getString(R.string.key_port), "").equals("2000")) {
                prefs.edit().putBoolean(SplashActivity.this.getString(R.string.key_communication_default), true).commit();
            }
            final long startTime = System.currentTimeMillis();
            do {
            } while (System.currentTimeMillis() - startTime <= 3 * 1000);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);
            String hash = prefs.getString(getString(R.string.key_hash), "");
            Intent intent = null;
            if(Strings.isNullOrEmpty(hash)) {
                intent = new Intent(SplashActivity.this, LoginActivity.class);
            } else {
                intent = new Intent(SplashActivity.this, MainActivity.class);
            }
            startActivity(intent);
            finish();
        }
    }
}
