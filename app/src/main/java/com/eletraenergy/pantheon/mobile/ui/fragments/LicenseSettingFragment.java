package com.eletraenergy.pantheon.mobile.ui.fragments;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.ui.LoginActivity;


public class LicenseSettingFragment extends PreferenceFragmentCompat {

    protected Preference key;
    protected Preference reset;
    protected SharedPreferences prefs;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.pref_fragmented_license);

        FragmentActivity activity = getActivity();
        prefs = PreferenceManager.getDefaultSharedPreferences(activity);

        key = (Preference)findPreference(activity.getString(R.string.key_hash));
        key.setSummary(prefs.getString(activity.getString(R.string.key_hash), ""));

        reset = (Preference) findPreference("key_license_reset");
        reset.setOnPreferenceClickListener(preference -> {

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(activity.getString(R.string.warning));
            builder.setIcon(R.drawable.eletra_icon);
            builder.setMessage(activity.getString(R.string.app_reset));
            builder.setPositiveButton(getString(R.string.yes),
                    (dialog, which) -> {
                        prefs.edit().remove(activity.getString(R.string.key_hash)).commit();
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                        activity.finish();
                        //super.onBackPressed();
                    });
            builder.setNegativeButton(activity.getString(R.string.no),
                    (dialog, which) -> dialog.dismiss());
            AlertDialog dialog = builder.create();
            dialog.show();
            return true;
        });

    }
}
