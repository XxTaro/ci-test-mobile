package com.eletraenergy.pantheon.mobile.util;

public class ChecksumFF {
    public ChecksumFF() {
    }

    public static byte calc(byte[] data) {
        return (byte)(255 - Checksum.calc(data));
    }

    public static byte calc(byte[] data, int offset, int size) {
        return (byte)(255 - Checksum.calc(data, offset, size));
    }
}