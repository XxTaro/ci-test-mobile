package com.eletraenergy.pantheon.mobile.ui.adapters;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.ui.AlarmSettingsActivity;
import com.eletraenergy.pantheon.mobile.ui.DateTimeSyncActivity;

import java.util.ArrayList;

/**
 * Created by dcsda on 10/01/2018.
 */

public class ChangesRecyclerViewAdapter extends RecyclerView.Adapter<ChangesRecyclerViewAdapter.ViewHolder> {

    protected ArrayList<ItemMain> itens;
    protected AppCompatActivity context;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView textViewHolder;
        public ImageView imageViewHolder;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            textViewHolder = (TextView) view.findViewById(R.id.textViewItemListMain);
            imageViewHolder = (ImageView) view.findViewById(R.id.imageViewItemListMain);
        }

        @Override
        public void onClick(View view) {

            //AlarmSettingsActivity
            if(getAdapterPosition() == 0) {
                Intent intent = new Intent(ChangesRecyclerViewAdapter.this.context, AlarmSettingsActivity.class);
                ChangesRecyclerViewAdapter.this.context.startActivity(intent);
            }

            //DateTimeSyncActivity
            if(getAdapterPosition() == 1) {
                Intent intent = new Intent(ChangesRecyclerViewAdapter.this.context, DateTimeSyncActivity.class);
                ChangesRecyclerViewAdapter.this.context.startActivity(intent);
            }
        }
    }

    public ChangesRecyclerViewAdapter(AppCompatActivity context, ArrayList<ItemMain> itens) {
        this.itens = itens;
        this.context = context;
    }

    @Override
    public ChangesRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                    int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_main, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ItemMain it = this.itens.get(position);
        holder.textViewHolder.setText(it.text);
        holder.imageViewHolder.setImageDrawable(it.image);
    }

    @Override
    public int getItemCount() {
        return itens.size();
    }

    public static class ItemMain {
        public Drawable image;
        public String text;
    }
}
