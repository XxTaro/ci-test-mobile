package com.eletraenergy.pantheon.mobile.ui.adapters;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.eletraenergy.pantheon.mobile.R;
import com.google.common.base.Strings;

import java.util.List;

/**
 * Created by dcsda on 16/01/2018.
 */

public class NetworksAvailableRecyclerViewAdapter extends RecyclerView.Adapter<NetworksAvailableRecyclerViewAdapter.ViewHolder> {

    protected List<NetworkItem> itens;
    protected AppCompatActivity context;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView text1;
        public TextView text2;
        public ImageView imageView;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            text1 = (TextView) view.findViewById(android.R.id.text1);
            text2 = (TextView) view.findViewById(android.R.id.text2);
            imageView = (ImageView) view.findViewById(R.id.imageViewItemListNetwork);
        }

        @Override
        public void onClick(View v) {
            NetworkItem item = itens.get(getAdapterPosition());
            if(item.connected) {
                View layout = context.getLayoutInflater().inflate(R.layout.dialog_disconnect, null);
                TextView textView = layout.findViewById(R.id.networkTextViewDisconnect);
                textView.setText(item.title.trim());
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(context.getString(R.string.disconnect));
                builder.setView(layout);
                builder.setIcon(R.drawable.eletra_icon);
                builder.setPositiveButton(context.getString(R.string.disconnect),
                        (dialog, which) -> {
                            dialog.dismiss();
                            WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                            wifiManager.disconnect();
                        });
                builder.setNeutralButton(context.getString(R.string.forget),
                        (dialog, which) -> {
                            dialog.dismiss();
                            WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                            int networkId = wifiManager.getConnectionInfo().getNetworkId();
                            wifiManager.removeNetwork(networkId);
                            wifiManager.disconnect();
                        });
                builder.setNegativeButton(context.getString(R.string.cancel),
                        (dialog, which) -> dialog.dismiss());
                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                View layout = context.getLayoutInflater().inflate(R.layout.dialog_connect, null);
                TextView textView = layout.findViewById(R.id.networkTextViewConnect);
                textView.setText(item.title.trim());
                TextView passTextView = layout.findViewById(R.id.passwordEditTextConnect);
                textView.setText(item.title.trim());
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(context.getString(R.string.connect));
                builder.setView(layout);
                builder.setIcon(R.drawable.eletra_icon);
                builder.setPositiveButton(context.getString(R.string.connect),
                        (dialog, which) -> {
                            dialog.dismiss();
                            WifiConfiguration wifiConfig = new WifiConfiguration();
                            wifiConfig.SSID = String.format("\"%s\"", item.title);
                            wifiConfig.preSharedKey = String.format("\"%s\"", passTextView.getText());

                            WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                            int netId = wifiManager.addNetwork(wifiConfig);
                            wifiManager.disconnect();
                            wifiManager.enableNetwork(netId, true);
                            wifiManager.reconnect();
                        });
                builder.setNegativeButton(context.getString(R.string.cancel),
                        (dialog, which) -> dialog.dismiss());
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }
    }

    public NetworksAvailableRecyclerViewAdapter(AppCompatActivity context, List<NetworkItem> itens) {
        this.itens = itens;
        this.context = context;
    }

    @Override
    public NetworksAvailableRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                              int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_network, parent, false);
        NetworksAvailableRecyclerViewAdapter.ViewHolder vh = new NetworksAvailableRecyclerViewAdapter.ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(NetworksAvailableRecyclerViewAdapter.ViewHolder holder, int position) {
        NetworkItem it = this.itens.get(position);
        holder.text1.setText(it.title);
        holder.text2.setText(it.subtitle);
        if(it.connected) {
            holder.imageView.setColorFilter(ContextCompat.getColor(context, android.R.color.holo_green_dark), android.graphics.PorterDuff.Mode.MULTIPLY);
        } else {
            holder.imageView.setColorFilter(ContextCompat.getColor(context, R.color.colorGray), android.graphics.PorterDuff.Mode.MULTIPLY);
        }
        if (it.strength <= 0 && it.strength >= -50) {
            //Best signal
            holder.imageView.setImageResource(R.drawable.baseline_signal_wifi_4_bar_white_24);
        } else if (it.strength < -50 && it.strength >= -70) {
            //Good signal
            holder.imageView.setImageResource(R.drawable.baseline_signal_wifi_3_bar_white_24);
        } else if (it.strength < -70 && it.strength >= -80) {
            //Low signal
            holder.imageView.setImageResource(R.drawable.baseline_signal_wifi_2_bar_white_24);
        } else if (it.strength < -80 && it.strength >= -100) {
            //Very weak signal
            holder.imageView.setImageResource(R.drawable.baseline_signal_wifi_1_bar_white_24);
        } else {
            //
            holder.imageView.setImageResource(R.drawable.baseline_signal_wifi_0_bar_white_24);
        }
    }

    @Override
    public int getItemCount() {
        return itens.size();
    }

    public static class NetworkItem {
        public String title;
        public String subtitle;
        public int strength;
        public boolean connected;
    }

}