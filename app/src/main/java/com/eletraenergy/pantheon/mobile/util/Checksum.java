package com.eletraenergy.pantheon.mobile.util;

public class Checksum {
    public Checksum() {
    }

    public static byte calc(byte[] data) {
        return calc(data, 0, data.length);
    }

    public static byte calc(byte[] data, int offset, int size) {
        byte checksum = 0;
        if (size >= 0 && offset >= 0) {
            if (offset + size > data.length) {
                throw new IllegalArgumentException();
            } else {
                for(int i = 0; i < size; ++i) {
                    checksum += data[offset + i];
                }

                return checksum;
            }
        } else {
            throw new IllegalArgumentException();
        }
    }
}