package com.eletraenergy.pantheon.mobile.ui.fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.preference.CheckBoxPreference;
import androidx.preference.EditTextPreference;
import androidx.preference.PreferenceFragmentCompat;

import com.eletraenergy.pantheon.mobile.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CommunicationSettingFragment extends PreferenceFragmentCompat {

    protected EditTextPreference port;
    protected EditTextPreference ip;

    protected CheckBoxPreference defaultSetting;

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.pref_fragmented_communication);
        FragmentActivity activity = getActivity();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);

        port = (EditTextPreference)findPreference(activity.getString(R.string.key_port));
        port.setSummary(prefs.getString(activity.getString(R.string.key_port), "00000"));
        port.setOnPreferenceChangeListener((preference, o) -> {
            prefs.edit().putString(activity.getString(R.string.key_port), o.toString()).commit();
            port.setSummary(o.toString());
            port.setText(o.toString());
            return false;
        });

        ip = (EditTextPreference)findPreference(activity.getString(R.string.key_ip));
        ip.setSummary(prefs.getString(activity.getString(R.string.key_ip), "0.0.0.0"));
        ip.setOnPreferenceChangeListener((preference, o) -> {
            prefs.edit().putString(activity.getString(R.string.key_ip), o.toString()).commit();
            ip.setSummary(o.toString());
            ip.setText(o.toString());
            return false;
        });

        defaultSetting = (CheckBoxPreference) findPreference(activity.getString(R.string.key_communication_default));
        if(!defaultSetting.isChecked()) {
            port.setEnabled(true);
            ip.setEnabled(true);
        } else {
            port.setEnabled(false);
            ip.setEnabled(false);
        }

        defaultSetting.setOnPreferenceClickListener(preference -> {
            defaultSetting = (CheckBoxPreference) findPreference(activity.getString(R.string.key_communication_default));
            prefs.edit().putString(activity.getString(R.string.key_ip), "11.11.11.254").commit();
            ip.setSummary("11.11.11.254");
            prefs.edit().putString(activity.getString(R.string.key_port), "2000").commit();
            port.setSummary("2000");
            if(!defaultSetting.isChecked()) {
                port.setEnabled(true);
                ip.setEnabled(true);
            } else {
                port.setEnabled(false);
                ip.setEnabled(false);
            }
            return false;
        });

        prefs.registerOnSharedPreferenceChangeListener((sharedPreferences, s) -> {
            if(s.equals(activity.getString(R.string.key_communication_default))) {
                defaultSetting = (CheckBoxPreference) findPreference(activity.getString(R.string.key_communication_default));
                prefs.edit().putString(activity.getString(R.string.key_ip), "11.11.11.254").commit();
                ip.setSummary("11.11.11.254");
                prefs.edit().putString(activity.getString(R.string.key_port), "2000").commit();
                port.setSummary("2000");
                if(!defaultSetting.isChecked()) {
                    port.setEnabled(true);
                    ip.setEnabled(true);
                } else {
                    port.setEnabled(false);
                    ip.setEnabled(false);
                }
            }
        });
    }
}
