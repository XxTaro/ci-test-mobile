package com.eletraenergy.pantheon.mobile.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadAsyncTask;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadEnum;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIReadTitle;
import com.eletraenergy.pantheon.mobile.ui.adapters.CSInformationRecyclerViewAdapter;
import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.eletra.mci.dlms.DLMSAtt;

public class CSInformationActivity extends PantheonCompatActivity {

    protected Toolbar toolbar;
    protected EletraMCIReadAsyncTask eletraMCIReadAsyncTask;
    protected EletraMCIReadAsyncTask eletraMCIReadAsyncTask2;
    protected RecyclerView recyclerView;
    protected RelativeLayout relativeLayoutLoad;
    protected ProgressBar progressBar;
    protected RelativeLayout relativeLayoutError;
    protected TextView textView;
    protected EletraMCIReadTitle eletraMCIReadTitle;
    protected SwipeRefreshLayout swipeRefreshLayout;
    protected Button button;
    protected ArrayList<CSInformationRecyclerViewAdapter.CSInformationItem> csList;
    protected HashMap<String, String> flagsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cs_information);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);

        eletraMCIReadTitle = new EletraMCIReadTitle();

        progressBar = (ProgressBar) findViewById(R.id.progressBarCpuInformation);
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(CSInformationActivity.this);
        circularProgressDrawable.setStyle(CircularProgressDrawable.LARGE);
        circularProgressDrawable.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        progressBar.setIndeterminateDrawable(circularProgressDrawable);

        relativeLayoutLoad = (RelativeLayout) findViewById(R.id.relativeLayoutLoadCpuInformation);
        relativeLayoutError = (RelativeLayout) findViewById(R.id.relativeLayoutErrorCpuInformation);
        textView = (TextView) findViewById(R.id.textViewCpuInformation);
        button = (Button) findViewById(R.id.buttonCpuInformation);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshCPUInformation);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewCPUInformation);
        LinearLayoutManager layoutManager = new LinearLayoutManager(CSInformationActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        swipeRefreshLayout.setOnRefreshListener(() -> CSInformationActivity.this.onResume());
    }

    @Override
    protected void onResume() {
        super.onResume();

        csList = new ArrayList<CSInformationRecyclerViewAdapter.CSInformationItem>();
        flagsList = new HashMap<String, String>();

        relativeLayoutError.setVisibility(View.GONE);
        swipeRefreshLayout.setVisibility(View.GONE);
        relativeLayoutLoad.setVisibility(View.VISIBLE);
        this.loadCPUInformation();
        this.loadRCInformation();

        setScreenAdapter(csList, flagsList);
    }

    protected void loadCPUInformation() {
        if (eletraMCIReadAsyncTask != null) eletraMCIReadAsyncTask.cancel(true);
        this.eletraMCIReadAsyncTask = new EletraMCIReadAsyncTask(0x3FFE,
                CSInformationActivity.this,
                EletraMCIReadEnum.CPU_INFORMATION,
                (atts, error)-> {
                    if(error > 0) {
                        textView.setText(error);
                        if(error == R.string.exception_generic) {
                            textView.setText(R.string.exception_generic_cs);
                        }
                        this.showButtonErro(error);
                        relativeLayoutLoad.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        swipeRefreshLayout.setVisibility(View.GONE);
                        relativeLayoutError.setVisibility(View.VISIBLE);
                    } else {
                        this.postReadCPU(atts);
                    }
                },
                task -> {
                    if(this.isHightLevel() > 0) {
                        this.preRead(task);
                    }
                }
        );
        this.eletraMCIReadAsyncTask.execute();
    }

    protected void loadRCInformation() {
        if (eletraMCIReadAsyncTask2 != null) eletraMCIReadAsyncTask2.cancel(true);
        this.eletraMCIReadAsyncTask2 = new EletraMCIReadAsyncTask(0x0001,
                CSInformationActivity.this,
                EletraMCIReadEnum.RC_INFORMATION,
                (atts, error)-> {
                    if(error > 0) {
                        textView.setText(error);
                        if(error == R.string.exception_generic) {
                            textView.setText(R.string.exception_generic_cs);
                        }
                        this.showButtonErro(error);
                        relativeLayoutLoad.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        swipeRefreshLayout.setVisibility(View.GONE);
                        relativeLayoutError.setVisibility(View.VISIBLE);
                    } else {
                        this.postReadRC(atts);
                    }
                },
                task -> {
                    if(this.isHightLevel() > 0) {
                        this.preRead(task);
                    }
                }
        );
        this.eletraMCIReadAsyncTask2.execute();
    }

    protected void showButtonErro(int err) {
        if(err == R.string.error_level) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(CSInformationActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_auth) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(CSInformationActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_encrypt) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(CSInformationActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_security) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(CSInformationActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_ip) {
            button.setText(R.string.bt_communication_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(CSInformationActivity.this, CommunicationSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_port) {
            button.setText(R.string.bt_communication_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(CSInformationActivity.this, CommunicationSettingActivity.class));
            });
            return;
        }
        button.setText(R.string.bt_retry);
        button.setVisibility(View.VISIBLE);
        button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_refresh_black_24dp), null, null, null);
        button.setOnClickListener(v -> {
            relativeLayoutError.setVisibility(View.GONE);
            relativeLayoutLoad.setVisibility(View.VISIBLE);
            this.loadCPUInformation();
        });
        return;
    }

    @Override
    public boolean onSupportNavigateUp(){
        this.onBackPressed();
        return true;
    }

    @Override
    public void onDestroy() {
        if (eletraMCIReadAsyncTask != null) eletraMCIReadAsyncTask.cancel(true);
        if (eletraMCIReadAsyncTask2 != null) eletraMCIReadAsyncTask2.cancel(true);
        super.onDestroy();
    }

    protected void postReadCPU(List<DLMSAtt> attributes) {
        String firmwareId = attributes.get(0).getIdProperty();
        String firmwareValue = attributes.get(0).getValueProperty();

        flagsList.put(firmwareId, firmwareValue);

        setFlags(attributes.get(7).getValueProperty(), "cpu");

        DLMSAtt att = attributes.get(7);
        att.setIdProperty(EletraMCIReadEnum.CPU_GENERAL_STATE.getReadingCommand());
        int flag = Integer.parseInt(att.getValueProperty());

        if ((flag & 0x01) == 0x01 || (flag & 0x02) == 0x02 || (flag & 0x04) == 0x04 || (flag & 0x08) == 0x08 ||
                (flag & 0x10) == 0x10) {
            // unstable
            att.setValueProperty(CSInformationActivity.this.getString(R.string.unstable));
        } else { // Stable
            att.setValueProperty(CSInformationActivity.this.getString(R.string.stable));
        }

        for(int i = 0; i < 4; i++) {
            attributes.remove(attributes.size()-1);
        }
        attributes.remove(0);
        attributes.add(att);
        Log.i(CSInformationActivity.class.getName(), attributes.get(2).getValueProperty());

        CSInformationRecyclerViewAdapter.CSInformationItem it;
        ArrayList<CSInformationRecyclerViewAdapter.CSInformationItem> itens = new ArrayList<CSInformationRecyclerViewAdapter.CSInformationItem>();
        for(DLMSAtt attribute : attributes) {
            it = new CSInformationRecyclerViewAdapter.CSInformationItem();

            String itTittle = attribute.getIdProperty();
            Log.i(CSInformationRecyclerViewAdapter.class.getName(), itTittle);
            int rTitle = eletraMCIReadTitle.getResTitle(itTittle);
            it.title = CSInformationActivity.this.getString(rTitle);

            if(attribute.getIdProperty().equals(EletraMCIReadEnum.DIGITAL_INPUT_STATUS.getReadingCommand())) {
                int value = Integer.parseInt(attribute.getValueProperty());
                if(value > 0) {
                    it.value = CSInformationActivity.this.getString(R.string.opened);
                } else {
                    it.value = CSInformationActivity.this.getString(R.string.closed);
                }
            } else {
                it.value = attribute.getValueProperty();
            }
            itens.add(it);
        }

        csList.addAll(itens);
    }

    protected void postReadRC(List<DLMSAtt> attributes){
        String firmwareId = attributes.get(1).getIdProperty();
        String firmwareValue = attributes.get(1).getValueProperty();

        flagsList.put(firmwareId, firmwareValue);

        setFlags(attributes.get(6).getValueProperty(), "rc");

        DLMSAtt att = attributes.get(6);
        att.setIdProperty(EletraMCIReadEnum.RC_GENERAL_STATE.getReadingCommand());
        int flag = Integer.parseInt(att.getValueProperty());

        if ((flag & 0x01) == 0x01 || (flag & 0x02) == 0x02 || (flag & 0x04) == 0x04 || (flag & 0x08) == 0x08 ||
                (flag & 0x10) == 0x10 || (flag & 0x20) == 0x20) {
            // fail
            att.setValueProperty(CSInformationActivity.this.getString(R.string.unstable));
        } else { // Stable
            att.setValueProperty(CSInformationActivity.this.getString(R.string.stable));
        }

        for(int i = 0; i < 6  ; i++) {
            attributes.remove(0);
        }

        CSInformationRecyclerViewAdapter.CSInformationItem it;
        ArrayList<CSInformationRecyclerViewAdapter.CSInformationItem> itens = new ArrayList<CSInformationRecyclerViewAdapter.CSInformationItem>();
        for(DLMSAtt attr : attributes){
            it = new CSInformationRecyclerViewAdapter.CSInformationItem();
            String itTittle = attr.getIdProperty();
            int rTitle = eletraMCIReadTitle.getResTitle(itTittle);
            it.title = CSInformationActivity.this.getString(rTitle);

            it.value = attr.getValueProperty();

            itens.add(it);
        }

        csList.addAll(itens);

        relativeLayoutError.setVisibility(View.GONE);
        relativeLayoutLoad.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setVisibility(View.VISIBLE);
    }

    protected void setScreenAdapter(ArrayList<CSInformationRecyclerViewAdapter.CSInformationItem> itens, HashMap<String, String> itensFlags){
        CSInformationRecyclerViewAdapter adapter = new CSInformationRecyclerViewAdapter((AppCompatActivity) CSInformationActivity.this, itens, itensFlags);
        recyclerView.setAdapter(adapter);
    }

    private String getResult(String binStr, int begin, int end) {
        return binStr.subSequence(begin, end).equals("1") ? CSInformationActivity.this.getString(R.string.yes) : CSInformationActivity.this.getString(R.string.no);
    }

    protected void setFlags(String flags, String comm){
        if(flags == null || flags.isEmpty()) flags = "0";

        int decFlags = Integer.parseInt(flags);
        String binStrVal = Integer.toBinaryString(decFlags);

        if(comm.equals("cpu")){
            while(binStrVal.length() < 5) binStrVal = "0"+binStrVal;

            flagsList.put("firmwareFailCPU", getResult(binStrVal, 4, 5));
            flagsList.put("rfFailCPU", getResult(binStrVal, 3, 4));
            flagsList.put("busFailCPU", getResult(binStrVal, 2, 3));
            flagsList.put("dflashFailCPU", getResult(binStrVal, 1, 2));
            flagsList.put("invalidMeterCPU", getResult(binStrVal, 0, 1));

        } else if(comm.equals("rc")){
            while(binStrVal.length() < 6) binStrVal = "0"+binStrVal;

            flagsList.put("firmwareFailRC", getResult(binStrVal, 5, 6));
            flagsList.put("paramFailRC", getResult(binStrVal, 4, 5));
            flagsList.put("rfFailRC", getResult(binStrVal, 3, 4));
            flagsList.put("wifiFailRC", getResult(binStrVal, 2, 3));
            flagsList.put("dflashFailRC", getResult(binStrVal, 1, 2));
            flagsList.put("cpuCommFailRC", getResult(binStrVal, 0, 1));
        }
    }

    protected void preRead(AsyncTask task) {
        task.cancel(true);
        textView.setText(this.isHightLevel());
        this.showButtonErro(this.isHightLevel());
        relativeLayoutLoad.setVisibility(View.GONE);
        swipeRefreshLayout.setVisibility(View.GONE);
        relativeLayoutError.setVisibility(View.VISIBLE);
    }
}
