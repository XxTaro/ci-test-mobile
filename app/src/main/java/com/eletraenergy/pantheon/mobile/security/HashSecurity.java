package com.eletraenergy.pantheon.mobile.security;

import android.util.Base64;

import com.eletraenergy.pantheon.mobile.util.HexString;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Diego Maia.
 */

public class HashSecurity implements HashSecurityInterface {

    public static final String DECRYPTED_SPLIT_CHAR = "-";
    public final String salt = "123456789";
    public final String eletraPassword = "3l3tr@";

    @Override
    public String calculateHash(String androidUniqueID) throws GeneralSecurityException, UnsupportedEncodingException, IllegalArgumentException {
        if (androidUniqueID == null || androidUniqueID.isEmpty()) {
            throw new IllegalArgumentException("Android unique id is required");
        }
        SecretKeySpec key = createSecretKey(eletraPassword.trim().toCharArray(), salt.trim().getBytes(), 40000, 128);
        System.out.println("key: " + key);
        String encryptedText = encrypt(androidUniqueID.trim(), key);
        System.out.println("Encrypted Text: " + encryptedText);
        return encryptedText;
    }

    @Override
    public SecretKeySpec createSecretKey(char[] password, byte[] salt, int iterationCount, int keyLength) throws NoSuchAlgorithmException, InvalidKeySpecException {
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2withHmacSHA1");
        PBEKeySpec keySpec = new PBEKeySpec(password, salt, iterationCount, keyLength);
        SecretKey keyTmp = keyFactory.generateSecret(keySpec);
        return new SecretKeySpec(keyTmp.getEncoded(), "AES");
    }

    @Override
    public String encrypt(String property, SecretKeySpec key) throws GeneralSecurityException, UnsupportedEncodingException {
        Cipher pbeCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        pbeCipher.init(Cipher.ENCRYPT_MODE, key);
        AlgorithmParameters parameters = pbeCipher.getParameters();
        IvParameterSpec ivParameterSpec = parameters.getParameterSpec(IvParameterSpec.class);
        System.out.println("Property: " + property);
        byte[] cryptoText = pbeCipher.doFinal(property.getBytes("UTF-8"));
        byte[] iv = ivParameterSpec.getIV();

        System.out.println("cryptoText: " + HexString.fromBytes(cryptoText));
        System.out.println("parameter: " + HexString.fromBytes(iv));

        String sIv = base64Encode(iv).trim();
        String sCrypt = base64Encode(cryptoText).trim();

        return sIv + ":" + sCrypt;
    }

    public String base64Encode(byte[] bytes) {
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    @Override
    public String decrypt(String string, SecretKeySpec key) throws GeneralSecurityException, IOException {
        String iv = string.split(":")[0];
        String property = string.split(":")[1];
        Cipher pbeCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        pbeCipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(base64Decode(iv)));
        return new String(pbeCipher.doFinal(base64Decode(property)), "UTF-8");
    }

    @Override
    public boolean isHashValid(String hash, String androidID) {
        try {
            if (hash == null || hash.isEmpty()) {
                return false;
            }
            String decryptedHash = decrypt(hash, createSecretKey(eletraPassword.trim().toCharArray(), salt.trim().getBytes(), 40000, 128));
            return verifyHash(decryptedHash, androidID);
        } catch (GeneralSecurityException | IOException | IllegalArgumentException e) {
            e.printStackTrace();
            return false;
        }
    }

    public byte[] base64Decode(String property) throws IOException {
        return Base64.decode(property, Base64.DEFAULT);
    }

    public boolean verifyHash(String decryptedHash, String androidID) {
        String[] splitted = decryptedHash.split(DECRYPTED_SPLIT_CHAR);
        String extractedAndroidID, extractedPassword;
        if (splitted.length > 1) {
            extractedAndroidID = splitted[0];
            extractedPassword = splitted[1];
            return extractedAndroidID.equals(androidID) && extractedPassword.equals(eletraPassword);
        }
        return false;
    }
}
