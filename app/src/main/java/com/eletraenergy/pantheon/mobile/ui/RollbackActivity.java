package com.eletraenergy.pantheon.mobile.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIWriteAsyncTask;
import com.eletraenergy.pantheon.mobile.eletramci.EletraMCIWriteEnum;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;

import br.com.eletra.mci.app.MeterComm;
import br.com.eletra.mci.app.MeterSchema;
import br.com.eletra.mci.dlms.DLMSCommand;
import br.com.eletra.mci.util.HexString;

public class RollbackActivity extends PantheonCompatActivity {

    protected Toolbar toolbar;
    protected EletraMCIWriteAsyncTask eletraMCIWriteAsyncTask;
    protected RelativeLayout relativeLayoutError;
    protected LinearLayout linearLayout;
    protected TextView textView;
    protected Button button;
    protected Button sendButton;
    protected AlertDialog progressWrite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rollback);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);

        linearLayout = (LinearLayout) findViewById(R.id.linearLayoutRollback);
        relativeLayoutError = (RelativeLayout) findViewById(R.id.relativeLayoutErrorRollback);
        textView = (TextView) findViewById(R.id.textViewRollback);
        sendButton = (Button) findViewById(R.id.sendButton);
        button = (Button) findViewById(R.id.buttonRollback);

    }

    protected void showButtonErro(int err) {
        if(err == R.string.error_level) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(RollbackActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_auth) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(RollbackActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_encrypt) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(RollbackActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_security) {
            button.setText(R.string.bt_credential_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(RollbackActivity.this, CredentialSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_ip) {
            button.setText(R.string.bt_communication_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(RollbackActivity.this, CommunicationSettingActivity.class));
            });
            return;
        }
        if(err == R.string.error_port) {
            button.setText(R.string.bt_communication_setting);
            button.setVisibility(View.VISIBLE);
            button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speaker_phone_black_24dp), null, null, null);
            button.setOnClickListener(v -> {
                this.startActivity(new Intent(RollbackActivity.this, CommunicationSettingActivity.class));
            });
            return;
        }
        button.setText(R.string.bt_retry);
        button.setVisibility(View.VISIBLE);
        button.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_refresh_black_24dp), null, null, null);
        button.setOnClickListener(v -> {
            relativeLayoutError.setVisibility(View.GONE);
        });
        return;
    }

    @Override
    public boolean onSupportNavigateUp(){
        this.onBackPressed();
        return true;
    }

    @Override
    public void onDestroy() {
        if (eletraMCIWriteAsyncTask != null) eletraMCIWriteAsyncTask.cancel(true);
        super.onDestroy();
    }

    public void showSendTransferDialog(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(RollbackActivity.this);
        builder.setTitle(getString(R.string.warning));
        builder.setMessage(R.string.app_rollback);
        builder.setIcon(R.drawable.eletra_icon);
        builder.setPositiveButton(getString(R.string.yes),
                (dialog, which) -> {
                    sendRollbackCommand();
                    dialog.dismiss();
                });
        builder.setNegativeButton(getString(R.string.no),
                (dialog, which) -> dialog.dismiss());
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void sendRollbackCommand() {
        if (eletraMCIWriteAsyncTask != null) eletraMCIWriteAsyncTask.cancel(true);
        MeterSchema meterSchema = MeterSchema.PANTHEON_V1;
        MeterComm meterComm = new MeterComm();
        meterComm.loadSchema(meterSchema, "DLMSTCP");

        //DLMSCommand dlmsCommand = (DLMSCommand) meterComm.getAvailableCommands().get(EletraMCIWriteEnum.ALARM_CONFIG.getWriteCommand());
        DLMSCommand dlmsCommand = (DLMSCommand) meterComm.getAvailableCommands().get(EletraMCIWriteEnum.ALARM_CONFIGURATION.getWriteCommand());

        String attr = getMask() + " | " + getSettings();
        Log.i("Alarm Settings", attr);

        //dlmsCommand.getAttById(EletraMCIWriteEnum.ALARMCONFIGURATION.getWriteCommand()).getAttById(EletraMCIWriteEnum.MASK.getWriteCommand()).setValueProperty(getMask());
        //dlmsCommand.getAttById(EletraMCIWriteEnum.ALARMCONFIGURATION.getWriteCommand()).getAttById(EletraMCIWriteEnum.CONFIG.getWriteCommand()).setValueProperty(getSettings());
        dlmsCommand.getAttById(EletraMCIWriteEnum.ALARMCONFIGURATION_att.getWriteCommand()).getAttById(EletraMCIWriteEnum.MASK_att.getWriteCommand()).setValueProperty(getMask());
        dlmsCommand.getAttById(EletraMCIWriteEnum.ALARMCONFIGURATION_att.getWriteCommand()).getAttById(EletraMCIWriteEnum.CONFIG_att.getWriteCommand()).setValueProperty(getSettings());

        this.eletraMCIWriteAsyncTask = new EletraMCIWriteAsyncTask(RollbackActivity.this,
                dlmsCommand,
                (error)-> {
                    if(error > 0) {
                        textView.setText(error);
                        if(error == R.string.exception_generic) {
                            textView.setText(R.string.exception_generic_relays_rollback);
                        }
                        //errorButton.setVisibility(View.GONE);
                        this.showButtonErro(this.isHightLevel());
                        sendButton.setEnabled(false);
                        relativeLayoutError.setVisibility(View.VISIBLE);
                        linearLayout.setVisibility(View.GONE);
                        if(this.progressWrite != null) this.progressWrite.dismiss();
                    } else {
                        if(this.progressWrite != null) this.progressWrite.dismiss();
                        sendButton.setEnabled(true);
                        Snackbar.make(toolbar, getString(R.string.success_action), Snackbar.LENGTH_SHORT).show();
                    }
                },
                task -> {
                    if(this.isHightLevel() > 0) {
                        this.preRead(task);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(RollbackActivity.this);
                        LayoutInflater inflater = LayoutInflater.from(RollbackActivity.this);
                        View view = inflater.inflate(R.layout.dialog_progress, null, false);
                        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBarProgress);
                        TextView textView = (TextView) view.findViewById(R.id.textViewProgress);
                        textView.setText(R.string.processing_rollback);
                        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(RollbackActivity.this);
                        circularProgressDrawable.setStyle(CircularProgressDrawable.LARGE);
                        circularProgressDrawable.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
                        progressBar.setIndeterminateDrawable(circularProgressDrawable);
                        builder.setView(view);
                        builder.setCancelable(false);
                        this.progressWrite = builder.create();
                        this.progressWrite.show();
                    }
                }
        );
        this.eletraMCIWriteAsyncTask.execute();

    }
    private String getMask() {
        int mask = 0;
        mask |= 0x04;
        return HexString.IntToString(mask).substring(3);
    }

    private String getSettings() {
        int value = 0;
        value |= 0x04;
        return HexString.IntToString(value).substring(3);
    }

    protected void preRead(AsyncTask task) {
        task.cancel(true);
        textView.setText(this.isHightLevel());
        this.showButtonErro(this.isHightLevel());
        linearLayout.setVisibility(View.GONE);
        relativeLayoutError.setVisibility(View.VISIBLE);
    }
}
