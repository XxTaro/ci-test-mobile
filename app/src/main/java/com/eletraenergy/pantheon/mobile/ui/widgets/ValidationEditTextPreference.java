package com.eletraenergy.pantheon.mobile.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import androidx.preference.EditTextPreference;

import com.eletraenergy.pantheon.mobile.R;

/**
 * Created by dcsda on 15/01/2018.
 */

public class ValidationEditTextPreference extends EditTextPreference {

    private int mDialogLayoutResId = R.layout.dialog_validation_edit_text_preference;
    private int mLength = 0;

    public ValidationEditTextPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        TypedArray array = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.ValidationEditTextPreference,
                0, 0);
        try {
            mLength = array.getInteger(R.styleable.ValidationEditTextPreference_validationLength, 0);
        } finally {
            array.recycle();
        }
    }

    public ValidationEditTextPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray array = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.ValidationEditTextPreference,
                0, 0);
        try {
            mLength = array.getInteger(R.styleable.ValidationEditTextPreference_validationLength, 0);
        } finally {
            array.recycle();
        }
    }

    public ValidationEditTextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray array = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.ValidationEditTextPreference,
                0, 0);
        try {
            mLength = array.getInteger(R.styleable.ValidationEditTextPreference_validationLength, 0);
        } finally {
            array.recycle();
        }

    }

    public ValidationEditTextPreference(Context context) {
        super(context);
    }

    public int getLength() {
        return mLength;
    }

    public void setLength(int lenght) {
        mLength = lenght;
    }

    @Override
    public int getDialogLayoutResource() {
        return mDialogLayoutResId;
    }
}
