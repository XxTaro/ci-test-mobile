package com.eletraenergy.pantheon.mobile.ui;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.security.HashSecurity;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;

public class LoginActivity extends PantheonCompatActivity {

    protected EditText tokenEditText;
    protected TextInputLayout tokenTextInputLayout;
    protected LoginAsyncTask loginAsyncTask;
    protected HashGenerateAsyncTask hashGenerateAsyncTask;
    protected Button enterButton;
    protected Button tokenButton;
    protected HashSecurity hashSecurity;
    protected String androidId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        hashSecurity = new HashSecurity();
        androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.i(LoginActivity.class.getName(), androidId);

        tokenEditText = (EditText) findViewById(R.id.tokenEditTextLogin);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
        tokenEditText.setText(prefs.getString(LoginActivity.this.getString(R.string.key_hash), ""));

        tokenTextInputLayout = (TextInputLayout) findViewById(R.id.tokenTextInputLayoutLogin);
        enterButton = (Button) findViewById(R.id.enterButtonLogin);
        tokenButton = (Button) findViewById(R.id.tokenButtonLogin);
    }

    @Override
    public void onDestroy() {
        if (loginAsyncTask != null) loginAsyncTask.cancel(true);
        if (hashGenerateAsyncTask != null) hashGenerateAsyncTask.cancel(true);
        super.onDestroy();
    }

    public void generateToken(View view) {
        hashGenerateAsyncTask = new HashGenerateAsyncTask();
        hashGenerateAsyncTask.execute();
    }

    public void login(View view) {
        loginAsyncTask = new LoginAsyncTask();
        loginAsyncTask.execute();
    }

    protected class LoginAsyncTask extends AsyncTask<Void, Void, Void> {

        protected String token;
        protected int error;
        protected AlertDialog progressLogin;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
            LayoutInflater inflater = LayoutInflater.from(LoginActivity.this);
            View view = inflater.inflate(R.layout.dialog_progress, null, false);
            ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBarProgress);
            CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(LoginActivity.this);
            circularProgressDrawable.setStyle(CircularProgressDrawable.LARGE);
            circularProgressDrawable.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
            progressBar.setIndeterminateDrawable(circularProgressDrawable);
            builder.setView(view);
            builder.setCancelable(false);
            this.progressLogin = builder.create();
            this.token = tokenEditText.getText().toString();
            this.error = 0;
            this.progressLogin.show();
            Log.i(LoginActivity.class.getName(), "Login Validation");
        }

        @Override
        protected Void doInBackground(Void... params) {
            if(Strings.isNullOrEmpty(this.token)) {
                this.error = R.string.error_key_validation;
            }
            try {
                if (!hashSecurity.isHashValid(this.token, androidId)) {
                    this.error = R.string.error_key_validation;
                }
            } catch (Exception e) {
                e.printStackTrace();
                this.error = R.string.error_key_validation;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            this.progressLogin.dismiss();
            Log.i(LoginActivity.class.getName(), "Erro: " + this.error);
            if(this.error == 0) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(LoginActivity.this.getString(R.string.key_hash), this.token);
                editor.commit();
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                tokenTextInputLayout.setError(LoginActivity.this.getString(this.error));
            }
        }
    }

    protected class HashGenerateAsyncTask extends AsyncTask<Void, Void, Void> {

        protected String hash;
        protected AlertDialog progressHash;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
            LayoutInflater inflater = LayoutInflater.from(LoginActivity.this);
            View view = inflater.inflate(R.layout.dialog_progress, null, false);
            ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBarProgress);
            CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(LoginActivity.this);
            circularProgressDrawable.setStyle(CircularProgressDrawable.LARGE);
            circularProgressDrawable.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
            progressBar.setIndeterminateDrawable(circularProgressDrawable);
            builder.setView(view);
            builder.setCancelable(false);
            this.progressHash = builder.create();
            this.progressHash.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                this.hash = hashSecurity.calculateHash(androidId);
                Log.i(LoginActivity.class.getName(), hash);
            } catch (Exception e) {
                this.hash = "";
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            this.progressHash.dismiss();
            if(Strings.isNullOrEmpty(this.hash)) {
                Snackbar.make(LoginActivity.this.findViewById(android.R.id.content), LoginActivity.this.getString(R.string.error_token_generation), Snackbar.LENGTH_SHORT).show();
            } else {
                this.showTokenDialog(this.hash);
            }
        }

        private void showTokenDialog(String token) {
            View layout = getLayoutInflater().inflate(R.layout.dialog_token, null);
            TextView textView = layout.findViewById(R.id.textViewToken);
            textView.setText(token.trim());
            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
            builder.setTitle(getString(R.string.generated_token));
            builder.setView(layout);
            builder.setIcon(R.drawable.eletra_icon);
            Log.i("TOKEN", token.trim());
            builder.setPositiveButton(getString(R.string.copy),
                    (dialog, which) -> {
                        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("hash", token.trim());
                        clipboard.setPrimaryClip(clip);
                        //tokenEditText.setText(token);
                        dialog.dismiss();
                        Snackbar.make(LoginActivity.this.findViewById(android.R.id.content), LoginActivity.this.getString(R.string.key_copied), Snackbar.LENGTH_SHORT).show();
                    });
            builder.setNegativeButton(getString(R.string.close),
                    (dialog, which) -> dialog.dismiss());
            builder.setNeutralButton(R.string.share, (dialog, which) -> {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, token.trim());
                startActivity(Intent.createChooser(share, getString(R.string.share)));
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    public void instructions(View view) {
        Intent intent = new Intent(LoginActivity.this, InstructionsActivity.class);
        startActivity(intent);
    }
}
