package com.eletraenergy.pantheon.mobile.util;

public class HexString {
    public HexString() {
    }

    public static byte[] toBytes(String s) {
        s = s.replaceAll(" ", "");
        int len = s.length();
        if ((len & 1) != 0) {
            s = "0" + s;
            ++len;
        }

        byte[] data = new byte[len / 2];

        for(int i = 0; i < len; i += 2) {
            data[i / 2] = (byte)((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }

        return data;
    }

    public static String fromBytesNoSpace(byte[] data) {
        return fromBytes(data, "", 0, data.length);
    }

    public static String fromBytes(byte[] data) {
        return fromBytes(data, " ", 0, data.length);
    }

    public static String fromBytes(byte[] data, int offset, int length) {
        return fromBytes(data, "", offset, length);
    }

    private static String fromBytes(byte[] data, String separator, int offset, int length) {
        StringBuilder sb = new StringBuilder();
        byte[] var5 = data;
        int var6 = data.length;

        for(int var7 = 0; var7 < var6; ++var7) {
            byte b = var5[var7];
            sb.append(String.format("%02X", b));
            sb.append(separator);
        }

        return sb.toString();
    }

    public static String fromByte(byte data) {
        return String.format("%02X", data);
    }

    public static String fromInt(int data) {
        return String.format("%04X", data);
    }

    public static void printBytes(byte[] data) {
        StringBuilder sb = new StringBuilder();
        byte[] var2 = data;
        int var3 = data.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            byte b = var2[var4];
            sb.append(String.format("%02X ", b));
        }

        sb.append("\r\n");
        System.out.println(sb.toString());
    }
}
