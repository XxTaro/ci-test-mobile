package com.eletraenergy.pantheon.mobile.ui;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.location.LocationManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.WifiConnectedService;
import com.eletraenergy.pantheon.mobile.ui.adapters.MainSectionsPagerAdapter;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends PantheonCompatActivity {

    protected Toolbar toolbarTop;
    protected Toolbar toolbarBottom;
    protected MainSectionsPagerAdapter sectionsPagerAdapter;
    protected ViewPager viewPager;

    private final int LOCATION_REQUEST_CODE = 1;
    private BroadcastReceiver receiverUpdate;
    private Intent intentService;
    private WifiManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbarTop = (Toolbar) findViewById(R.id.toolbarTop);
        setSupportActionBar(toolbarTop);
        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbarTop.getLayoutParams();
        params.setScrollFlags(0);

        toolbarBottom = (Toolbar) findViewById(R.id.toolbarBottom);
        toolbarBottom.setTitle("Aguardando...");
        toolbarBottom.inflateMenu(R.menu.menu_wifi_info);
        toolbarBottom.setOnMenuItemClickListener(menuItem -> {
            Intent intent = new Intent(MainActivity.this, NetworksAvailableActivity.class);
            startActivity(intent);
            return false;
        });

        sectionsPagerAdapter = new MainSectionsPagerAdapter(getSupportFragmentManager());

        viewPager = (ViewPager) findViewById(R.id.viewPagerMain);
        viewPager.setAdapter(sectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayoutMain);
        tabLayout.getTabAt(0).getIcon().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(1).getIcon().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(2).getIcon().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));

        manager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
    }
    public boolean isLocationEnabled() {
        int locationMode = 0;
        String locationProviders;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    private void updateNetworkInfo(){
        if(isPermissionLocationGranted()){
            if(isLocationEnabled()) {
                if (manager.isWifiEnabled()) {
                    WifiInfo wifiInfo = manager.getConnectionInfo();
                    if (wifiInfo != null) {
                        NetworkInfo.DetailedState state = WifiInfo.getDetailedStateOf(wifiInfo.getSupplicantState());
                        if (state == NetworkInfo.DetailedState.CONNECTED || state == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
                            toolbarBottom.setTitle(wifiInfo.getSSID().replaceAll("^\"|\"$", ""));
                            toolbarBottom.getMenu().clear();
                            int numberOfLevels = 5;
                            int level = WifiManager.calculateSignalLevel(wifiInfo.getRssi(), numberOfLevels);
                            if(level == 0) {
                                toolbarBottom.inflateMenu(R.menu.menu_wifi_on_0);
                            } else if(level == 1) {
                                toolbarBottom.inflateMenu(R.menu.menu_wifi_on_1);
                            } else if(level == 2) {
                                toolbarBottom.inflateMenu(R.menu.menu_wifi_on_2);
                            } else if(level == 3) {
                                toolbarBottom.inflateMenu(R.menu.menu_wifi_on_3);
                            } else if(level == 4) {
                                toolbarBottom.inflateMenu(R.menu.menu_wifi_on_4);
                            } else {
                                toolbarBottom.inflateMenu(R.menu.menu_wifi_on);
                            }
                        } else {
                            //Snackbar.make(toolbarBottom, "Wifi Desconectado", Snackbar.LENGTH_SHORT).show();
                            toolbarBottom.setTitle(R.string.wifi_diconected);
                            toolbarBottom.getMenu().clear();
                            toolbarBottom.inflateMenu(R.menu.menu_wifi_off);
                        }
                    } else {
                        Snackbar.make(toolbarBottom, "Não foi possível obter as informações da rede", Snackbar.LENGTH_LONG).show();
                        toolbarBottom.setTitle(R.string.wifi_error);
                        toolbarBottom.getMenu().clear();
                        toolbarBottom.inflateMenu(R.menu.menu_wifi_off);
                    }
                } else {
                    //Snackbar.make(toolbarBottom, "Wifi Desabilitado", Snackbar.LENGTH_SHORT).show();
                    toolbarBottom.setTitle(R.string.wifi_disabled);
                    toolbarBottom.getMenu().clear();
                    toolbarBottom.inflateMenu(R.menu.menu_wifi_off);
                }
            } else {
                Snackbar.make(toolbarBottom, "O GPS precisa está habilitado para que seja possível se obter as informações de rede", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Fechar", v -> {
                        }).setActionTextColor(getResources().getColor(R.color.colorSecundary)).show();
                toolbarBottom.setTitle(R.string.wifi_standby);
                toolbarBottom.getMenu().clear();
                toolbarBottom.inflateMenu(R.menu.menu_wifi_info);
            }
        }
    }

    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(android.net.ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(LocationManager.PROVIDERS_CHANGED_ACTION);
        filter.addAction(WifiConnectedService.UPDATE_NETWORK_CONNECTED);

        receiverUpdate = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i(MainActivity.class.getSimpleName(), "Atualizando conexão de rede...");
                updateNetworkInfo();
            }
        };


        this.registerReceiver(this.receiverUpdate, filter);
        WifiConnectedService.SERVICE_CONNECTED = true;
        intentService = new Intent(this, WifiConnectedService .class);
        this.startService(this.intentService);
        updateNetworkInfo();
    }

    public void onPause() {
        super.onPause();
        this.unregisterReceiver(this.receiverUpdate);
        WifiConnectedService.SERVICE_CONNECTED = false;
        this.stopService(this.intentService);

    }

    public boolean isPermissionLocationGranted() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{
                                Manifest.permission.ACCESS_FINE_LOCATION
                        },
                        LOCATION_REQUEST_CODE);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{
                                Manifest.permission.ACCESS_FINE_LOCATION
                        },
                        LOCATION_REQUEST_CODE);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == LOCATION_REQUEST_CODE) {
            if (permissions.length == 2 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                this.startService(this.intentService);
                if (manager.isWifiEnabled()) {
                    if(isLocationEnabled()) {
                        WifiInfo wifiInfo = manager.getConnectionInfo();
                        if (wifiInfo != null) {
                            NetworkInfo.DetailedState state = WifiInfo.getDetailedStateOf(wifiInfo.getSupplicantState());
                            if (state == NetworkInfo.DetailedState.CONNECTED || state == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
                                toolbarBottom.setTitle(wifiInfo.getSSID().replaceAll("^\"|\"$", ""));
                                int numberOfLevels = 5;
                                int level = WifiManager.calculateSignalLevel(wifiInfo.getRssi(), numberOfLevels);
                                if(level == 0) {
                                    toolbarBottom.inflateMenu(R.menu.menu_wifi_on_0);
                                } else if(level == 1) {
                                    toolbarBottom.inflateMenu(R.menu.menu_wifi_on_1);
                                } else if(level == 2) {
                                    toolbarBottom.inflateMenu(R.menu.menu_wifi_on_2);
                                } else if(level == 3) {
                                    toolbarBottom.inflateMenu(R.menu.menu_wifi_on_3);
                                } else if(level == 4) {
                                    toolbarBottom.inflateMenu(R.menu.menu_wifi_on_4);
                                } else {
                                    toolbarBottom.inflateMenu(R.menu.menu_wifi_on);
                                }
                            } else {
                                //Snackbar.make(toolbarBottom, "Wifi Desconectado", Snackbar.LENGTH_SHORT).show();
                                toolbarBottom.setTitle(R.string.wifi_diconected);
                                toolbarBottom.getMenu().clear();
                                toolbarBottom.inflateMenu(R.menu.menu_wifi_off);
                            }
                        } else {
                            Snackbar.make(toolbarBottom, "Não foi possível obter as informações da rede", Snackbar.LENGTH_LONG).show();
                            toolbarBottom.setTitle(R.string.wifi_error);
                            toolbarBottom.getMenu().clear();
                            toolbarBottom.inflateMenu(R.menu.menu_wifi_off);
                        }
                    } else {
                        Snackbar.make(toolbarBottom, "O GPS precisa está habilitado para que seja possível se obter as informações de rede", Snackbar.LENGTH_INDEFINITE)
                                .setAction("Fechar", v -> {
                                }).setActionTextColor(getResources().getColor(R.color.colorSecundary)).show();
                        toolbarBottom.setTitle(R.string.wifi_standby);
                        toolbarBottom.getMenu().clear();
                        toolbarBottom.inflateMenu(R.menu.menu_wifi_info);
                    }
                } else {
                    //Snackbar.make(toolbarBottom, "Wifi Desabilitado", Snackbar.LENGTH_SHORT).show();
                    toolbarBottom.setTitle(R.string.wifi_disabled);
                    toolbarBottom.getMenu().clear();
                    toolbarBottom.inflateMenu(R.menu.menu_wifi_off);
                }
            } else {
                this.stopService(this.intentService);
                Snackbar.make(toolbarBottom, "Não foi possível obter as informações da rede", Snackbar.LENGTH_SHORT).show();
                toolbarBottom.setTitle(R.string.wifi_error);
                toolbarBottom.getMenu().clear();
                toolbarBottom.inflateMenu(R.menu.menu_wifi_off);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getString(R.string.warning));
        builder.setIcon(R.drawable.eletra_icon);
        builder.setMessage(getString(R.string.app_exit));
        builder.setPositiveButton(getString(R.string.yes),
                (dialog, which) -> {
                    super.onBackPressed();
                });
        builder.setNegativeButton(getString(R.string.no),
                (dialog, which) -> dialog.dismiss());
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up transferButton, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
