package com.eletraenergy.pantheon.mobile.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.Settings;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.espresso.intent.Intents;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.security.HashSecurity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.crypto.spec.SecretKeySpec;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by Darlan on 14/02/2018.
 */

@RunWith(AndroidJUnit4.class)
public class SplashActivityInstrumentedTest extends ActivityInstrumentedTest {

    @Rule
    public ActivityTestRule<SplashActivity> activityTestRule = new ActivityTestRule<SplashActivity>(SplashActivity.class, true, false);

    public Intent intent;

    @Before
    public void setup(){
        Intents.init();
        intent = new Intent();
    }

    @After
    public void tearDown(){
        Intents.release();
    }

    @Test
    public void openSplashActivityTest() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ApplicationProvider.getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(ApplicationProvider.getApplicationContext().getString(R.string.key_hash));
        editor.commit();
        activityTestRule.launchActivity(intent);
        intended(hasComponent(LoginActivity.class.getName()));
    }

    @Test
    public void goToLoginTest() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ApplicationProvider.getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(ApplicationProvider.getApplicationContext().getString(R.string.key_hash));
        editor.commit();

        activityTestRule.launchActivity(intent);
        intended(hasComponent(LoginActivity.class.getName()));
    }

    @Test
    public void goToMainTest() {
        String androidId = Settings.Secure.getString(ApplicationProvider.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        HashSecurity hashSecurity = new HashSecurity();
        String hash = null;
        String key = null;
        try {
            hash = hashSecurity.calculateHash(androidId);
            SecretKeySpec keySpec = hashSecurity.createSecretKey(hashSecurity.eletraPassword.trim().toCharArray(),
                    hashSecurity.salt.trim().getBytes(), 40000, 128);
            String decryptedPassword = hashSecurity.decrypt(hash, keySpec);
            key = hashSecurity.encrypt(decryptedPassword + "-" + hashSecurity.eletraPassword, keySpec);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertNotNull("Erro ao gerar hash", hash);
        assertNotNull("Erro ao gerar chave", key);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ApplicationProvider.getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_hash), key);
        editor.commit();

        activityTestRule.launchActivity(intent);
        onView(withId(R.id.toolbarTop)).check(matches(isDisplayed()));
        onView(withId(R.id.toolbarBottom)).check(matches(isDisplayed()));
        onView(withText(R.string.label_main)).check(matches(withParent(withId(R.id.toolbarTop))));
    }
}