package com.eletraenergy.pantheon.mobile.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.eletraenergy.pantheon.mobile.R;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by Darlan on 14/02/2018.
 */

@RunWith(AndroidJUnit4.class)
public class CSInformationActivityInstrumentedTest extends ActivityInstrumentedTest {

    @Rule
    public ActivityTestRule<CSInformationActivity> activityTestRule = new ActivityTestRule<CSInformationActivity>(CSInformationActivity.class, true, false);

    public Intent intent;

    @Before
    public void setup(){
        intent = new Intent();
    }

    @Test
    public void openCPUInformationActivityTest() {
        activityTestRule.launchActivity(intent);

        onView(withId(R.id.toolbar)).check(matches(isDisplayed()));
        onView(withText(R.string.label_cs_information)).check(matches(withParent(withId(R.id.toolbar))));
    }

    @Test
    public void credentialsErrorTest() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ApplicationProvider.getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_auth), "");
        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_encrypt), "");
        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_security), "");

        editor.putBoolean(ApplicationProvider.getApplicationContext().getString(R.string.key_leve_security), false);
        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_ip), "11.11.11.254");
        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_port), "2000");
        editor.commit();

        activityTestRule.launchActivity(intent);
        onView(withText(R.string.bt_credential_setting)).check(matches(isDisplayed()));
    }

    @Test
    public void communicationErrorTest() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ApplicationProvider.getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();

        editor.remove(ApplicationProvider.getApplicationContext().getString(R.string.key_ip));
        editor.remove(ApplicationProvider.getApplicationContext().getString(R.string.key_port));
        editor.commit();

        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_security), "00000000000000000000000000000000");
        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_auth), "00000000000000000000000000000000");
        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_encrypt), "00000000000000000000000000000000");
        editor.putBoolean(ApplicationProvider.getApplicationContext().getString(R.string.key_leve_security), true);
        editor.commit();

        activityTestRule.launchActivity(intent);
        onView(withText(R.string.bt_communication_setting)).check(matches(isDisplayed()));
    }

    @Test
    public void readTest() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ApplicationProvider.getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_security), "00000000000000000000000000000000");
        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_auth), "00000000000000000000000000000000");
        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_encrypt), "00000000000000000000000000000000");
        editor.putBoolean(ApplicationProvider.getApplicationContext().getString(R.string.key_leve_security), true);
        editor.commit();

        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_ip), "192.168.137.1");
        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_port), "8000");
        editor.commit();

        activityTestRule.launchActivity(intent);
        onView(withText(R.string.attr_software_version)).check(matches(isDisplayed()));
        onView(withText(R.string.attr_serial_number)).check(matches(isDisplayed()));
        onView(withText(R.string.attr_cpu_date_time)).check(matches(isDisplayed()));
        onView(withText(R.string.attr_port_status)).check(matches(isDisplayed()));
        onView(withText(R.string.attr_general_state)).check(matches(isDisplayed()));
    }
}
