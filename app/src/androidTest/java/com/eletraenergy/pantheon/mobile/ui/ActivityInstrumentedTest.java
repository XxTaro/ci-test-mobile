package com.eletraenergy.pantheon.mobile.ui;

/**
 * Created by Darlan on 06/03/2018.
 */

public class ActivityInstrumentedTest {
    public static final String IP = "192.168.137.1";
    public static final String PORT = "8000";

    public static final String KEY_AUTH = "00000000000000000000000000000000";
    public static final String KEY_SECURITY = "00000000000000000000000000000000";
    public static final String KEY_ENCRYPT = "00000000000000000000000000000000";
}
