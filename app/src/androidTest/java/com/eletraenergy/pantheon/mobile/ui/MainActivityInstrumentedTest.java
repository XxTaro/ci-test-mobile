package com.eletraenergy.pantheon.mobile.ui;


import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.intent.Intents;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.eletraenergy.pantheon.mobile.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by Darlan on 14/02/2018.
 */

@RunWith(AndroidJUnit4.class)
public class MainActivityInstrumentedTest extends ActivityInstrumentedTest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    @Before
    public void setup(){
        Intents.init();
    }

    @After
    public void tearDown(){
        Intents.release();
    }

    @Test
    public void openMainActivityTest() {
        onView(withId(R.id.toolbarTop)).check(matches(isDisplayed()));
        onView(withId(R.id.toolbarBottom)).check(matches(isDisplayed()));
        onView(withText(R.string.label_main)).check(matches(withParent(withId(R.id.toolbarTop))));
        onView(withId(R.id.viewPagerMain)).check(matches(isDisplayed()));

        onView(withText(R.string.label_actions)).perform(click());
        onView(withText(R.string.relays_controls)).check(matches(isDisplayed()));
        onView(withText(R.string.rollback)).check(matches(isDisplayed()));
        onView(withText(R.string.display_pairing)).check(matches(isDisplayed()));
        onView(withText(R.string.alarm_clearing)).check(matches(isDisplayed()));

        onView(withText(R.string.label_changes)).perform(click());
        onView(withText(R.string.alarm_settings)).check(matches(isDisplayed()));
        onView(withText(R.string.sync_date_time)).check(matches(isDisplayed()));

        onView(withText(R.string.label_readings)).perform(click());
        onView(withText(R.string.cs_informations)).check(matches(isDisplayed()));
        onView(withText(R.string.consumers_units)).check(matches(isDisplayed()));
        onView(withText(R.string.online_read)).check(matches(isDisplayed()));
        onView(withText(R.string.checkpoint_read)).check(matches(isDisplayed()));
        onView(withText(R.string.alarm_settings_read)).check(matches(isDisplayed()));
        onView(withText(R.string.alarm_readings)).check(matches(isDisplayed()));
    }

    @Test
    public void backTest() {
        onView(isRoot()).perform(ViewActions.pressBack());
        onView(withText(R.string.warning)).check(matches(isDisplayed()));
        onView(withText(R.string.no)).perform(click());
        onView(withText(R.string.warning)).check(doesNotExist());
    }

    @Test
    public void goToSettingsTest() {
        openActionBarOverflowOrOptionsMenu(activityTestRule.getActivity());
        onView(withText(R.string.settings)).perform(click());
        intended(hasComponent(SettingsActivity.class.getName()));
    }

    @Test
    public void goToCPUInformationTest() {
        onView(withText(R.string.label_readings)).perform(click());
        onView(withText(R.string.cs_informations)).check(matches(isDisplayed()));

        onView(withText(R.string.cs_informations)).perform(click());
        intended(hasComponent(CSInformationActivity.class.getName()));
    }

    @Test
    public void goToConsumersUnitsTest() {
        onView(withText(R.string.label_readings)).perform(click());
        onView(withText(R.string.consumers_units)).check(matches(isDisplayed()));

        onView(withText(R.string.consumers_units)).perform(click());
        intended(hasComponent(ConsumersUnitsActivity.class.getName()));
    }

    @Test
    public void goToRelaysControlsTest() {
        onView(withText(R.string.label_actions)).perform(click());
        onView(withText(R.string.relays_controls)).check(matches(isDisplayed()));

        onView(withText(R.string.relays_controls)).perform(click());
        intended(hasComponent(RelaysControlsActivity.class.getName()));
    }
}