package com.eletraenergy.pantheon.mobile.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.eletraenergy.pantheon.mobile.R;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by Darlan on 14/02/2018.
 */

@RunWith(AndroidJUnit4.class)
public class ConsumersUnitsActivityInstrumentedTest extends ActivityInstrumentedTest {

    @Rule
    public ActivityTestRule<ConsumersUnitsActivity> activityTestRule = new ActivityTestRule<ConsumersUnitsActivity>(ConsumersUnitsActivity.class, true, false);

    public Intent intent;

    @Before
    public void setup(){
        intent = new Intent();
    }



    @Test
    public void openCPUInformationActivityTest() {
        activityTestRule.launchActivity(intent);

        onView(withId(R.id.toolbar)).check(matches(isDisplayed()));
        onView(withText(R.string.label_consumers_units)).check(matches(withParent(withId(R.id.toolbar))));
    }

    @Test
    public void credentialsErrorTest() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ApplicationProvider.getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_auth), "");
        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_encrypt), "");
        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_security), "");

        editor.putBoolean(ApplicationProvider.getApplicationContext().getString(R.string.key_leve_security), false);
        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_ip), "11.11.11.254");
        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_port), "2000");
        editor.commit();

        activityTestRule.launchActivity(intent);
        onView(withText(R.string.bt_credential_setting)).check(matches(isDisplayed()));
    }

    @Test
    public void communicationErrorTest() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ApplicationProvider.getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();

        editor.remove(ApplicationProvider.getApplicationContext().getString(R.string.key_ip));
        editor.remove(ApplicationProvider.getApplicationContext().getString(R.string.key_port));
        editor.commit();

        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_security), "00000000000000000000000000000000");
        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_auth), "00000000000000000000000000000000");
        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_encrypt), "00000000000000000000000000000000");
        editor.putBoolean(ApplicationProvider.getApplicationContext().getString(R.string.key_leve_security), true);
        editor.commit();

        activityTestRule.launchActivity(intent);
        onView(withText(R.string.bt_communication_setting)).check(matches(isDisplayed()));
    }

    @Test
    public void readTest() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ApplicationProvider.getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_security), KEY_SECURITY);
        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_auth), KEY_AUTH);
        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_encrypt), KEY_ENCRYPT);
        editor.putBoolean(ApplicationProvider.getApplicationContext().getString(R.string.key_leve_security), true);
        editor.commit();

        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_ip), IP);
        editor.putString(ApplicationProvider.getApplicationContext().getString(R.string.key_port), PORT);
        editor.commit();

        activityTestRule.launchActivity(intent);
        onView(withId(R.id.swipeRefreshConsumersUnits)).check(matches(isDisplayed()));
        onView(withId(R.id.recyclerViewConsumersUnits)).check(matches(isDisplayed()));

        Integer[] itemLenght = new Integer[1];
        onView(withId(R.id.recyclerViewConsumersUnits)).perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isAssignableFrom(RecyclerView.class);
            }

            @Override
            public String getDescription() {
                return "";
            }

            @Override
            public void perform(UiController uiController, View view) {
                RecyclerView rv = (RecyclerView)view;
                itemLenght[0] = rv.getAdapter().getItemCount();
            }
        });
        /*
        for(int i = 0; i < itemLenght[0]; i++) {
            onView(withId(R.id.recyclerViewConsumersUnits))
                    .perform(RecyclerViewActions.scrollToPosition(i))
                    .check(matches(atPosition(i, isDisplayed())));
            onView(withRecyclerView(R.id.recyclerViewConsumersUnits).atPosition(i + 1)).check(matches(isDisplayed()));
        }
        */
    }
}

