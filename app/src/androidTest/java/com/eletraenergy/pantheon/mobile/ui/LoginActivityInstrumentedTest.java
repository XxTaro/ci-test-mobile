package com.eletraenergy.pantheon.mobile.ui;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.intent.Intents;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.eletraenergy.pantheon.mobile.R;
import com.eletraenergy.pantheon.mobile.security.HashSecurity;

import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.crypto.spec.SecretKeySpec;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.RootMatchers.isDialog;
import static androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by Darlan on 14/02/2018.
 */

@RunWith(AndroidJUnit4.class)
public class LoginActivityInstrumentedTest extends ActivityInstrumentedTest {

    @Rule
    public ActivityTestRule<LoginActivity> activityTestRule = new ActivityTestRule<LoginActivity>(LoginActivity.class);

    @Before
    public void setup(){
        Intents.init();
    }

    @After
    public void tearDown(){
        Intents.release();
    }

    @Test
    public void openLoginActivityTest() {
        onView(withId(R.id.tokenEditTextLogin)).check(matches(isDisplayed()));
        onView(withId(R.id.tokenEditTextLogin))
                .perform(replaceText(""), closeSoftKeyboard());
        onView(withId(R.id.enterButtonLogin)).check(matches(isDisplayed()));
        onView(withId(R.id.tokenButtonLogin)).check(matches(isDisplayed()));
        onView(withId(R.id.instructionsTextViewLogin)).check(matches(isDisplayed()));
    }

    @Test
    public void goToInstructionsTest() {
        onView(withId(R.id.tokenEditTextLogin))
                .perform(replaceText(""), closeSoftKeyboard());
        onView(withId(R.id.instructionsTextViewLogin)).perform(click());
        onView(withId(R.id.toolbar)).check(matches(isDisplayed()));
        onView(withText(R.string.label_instructions)).check(matches(withParent(withId(R.id.toolbar))));
        intended(hasComponent(InstructionsActivity.class.getName()));
    }

    @Test
    public void cancelGenerateTokenTest() {
        onView(withId(R.id.tokenEditTextLogin))
                .perform(replaceText(""), closeSoftKeyboard());
        onView(withId(R.id.tokenButtonLogin)).perform(click());
        onView(withText(R.string.generated_token)).check(matches(isDisplayed()));
        onView(withText(R.string.close)).perform(click());
        onView(withText(R.string.generated_token)).check(doesNotExist());
    }

    @Test
    public void copyGenerateTokenTest() {
        final String[] tokenText = new String[1];
        onView(withId(R.id.tokenEditTextLogin))
                .perform(replaceText(""), closeSoftKeyboard());
        onView(withId(R.id.tokenButtonLogin)).perform(click());
        onView(withText(R.string.generated_token)).check(matches(isDisplayed()));
        onView(withId(R.id.textViewToken)).inRoot(isDialog()).check(matches(isDisplayed()));
        onView(withId(R.id.textViewToken)).inRoot(isDialog()).perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isAssignableFrom(TextView.class);
            }

            @Override
            public String getDescription() {
                return "";
            }

            @Override
            public void perform(UiController uiController, View view) {
                TextView tv = (TextView)view;
                tokenText[0] = tv.getText().toString();
            }
        });
        onView(withText(R.string.copy)).perform(click());
        onView(withText(R.string.generated_token)).check(doesNotExist());
        ClipboardManager clipboard = (ClipboardManager) activityTestRule.getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = clipboard.getPrimaryClip();
        clip.getItemAt(0).getText();
        assertEquals("Erro ao copiar valor do token", tokenText[0], clip.getItemAt(0).getText());
    }

    @Test
    public void errorLoginTest() {
        // Vazio
        onView(withId(R.id.tokenEditTextLogin))
                .perform(clearText(), closeSoftKeyboard());
        onView(withId(R.id.enterButtonLogin)).perform(click());
        onView(withText(R.string.error_key_validation)).check(matches(isDisplayed()));
        // Aleatório
        onView(withId(R.id.tokenEditTextLogin))
                .perform(typeText("1234567890"), closeSoftKeyboard());
        onView(withId(R.id.enterButtonLogin)).perform(click());
        onView(withText(R.string.error_key_validation)).check(matches(isDisplayed()));
        // Usando Hash
        final String[] tokenText = new String[1];
        onView(withId(R.id.tokenEditTextLogin))
                .perform(clearText(), closeSoftKeyboard());
        onView(withId(R.id.tokenButtonLogin)).perform(click());
        onView(withText(R.string.generated_token)).check(matches(isDisplayed()));
        onView(withId(R.id.textViewToken)).inRoot(isDialog()).check(matches(isDisplayed()));
        onView(withId(R.id.textViewToken)).inRoot(isDialog()).perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isAssignableFrom(TextView.class);
            }

            @Override
            public String getDescription() {
                return "";
            }

            @Override
            public void perform(UiController uiController, View view) {
                TextView tv = (TextView)view;
                tokenText[0] = tv.getText().toString();
            }
        });
        System.out.println(tokenText[0]);
        onView(withText(R.string.copy)).perform(click());
        onView(withId(R.id.tokenEditTextLogin))
                .perform(typeText(tokenText[0]), closeSoftKeyboard());
        onView(withText(R.string.error_key_validation)).check(matches(isDisplayed()));
    }

    @Test
    public void successLoginTest() {
        final String[] tokenText = new String[1];
        onView(withId(R.id.tokenEditTextLogin))
                .perform(clearText(), closeSoftKeyboard());
        onView(withId(R.id.tokenButtonLogin)).perform(click());
        onView(withText(R.string.generated_token)).check(matches(isDisplayed()));
        onView(withId(R.id.textViewToken)).inRoot(isDialog()).check(matches(isDisplayed()));
        onView(withId(R.id.textViewToken)).inRoot(isDialog()).perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isAssignableFrom(TextView.class);
            }

            @Override
            public String getDescription() {
                return "";
            }

            @Override
            public void perform(UiController uiController, View view) {
                TextView tv = (TextView)view;
                tokenText[0] = tv.getText().toString();
            }
        });
        onView(withText(R.string.copy)).perform(click());
        onView(withText(R.string.generated_token)).check(doesNotExist());
        ClipboardManager clipboard = (ClipboardManager) activityTestRule.getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = clipboard.getPrimaryClip();
        HashSecurity hashSecurity = new HashSecurity();
        String hash = clip.getItemAt(0).getText().toString();
        String key = null;
        try {
            SecretKeySpec keySpec = hashSecurity.createSecretKey(hashSecurity.eletraPassword.trim().toCharArray(),
                    hashSecurity.salt.trim().getBytes(), 40000, 128);
            String decryptedPassword = hashSecurity.decrypt(hash, keySpec);
            key = hashSecurity.encrypt(decryptedPassword + "-" + hashSecurity.eletraPassword, keySpec);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertNotNull("Erro ao gerar hash", hash);
        assertNotNull("Erro ao gerar chave", key);
        onView(withId(R.id.tokenEditTextLogin))
                .perform(clearText(), closeSoftKeyboard());
        onView(withId(R.id.tokenEditTextLogin))
                .perform(typeText(key), closeSoftKeyboard());

        onView(withId(R.id.enterButtonLogin)).perform(click());
        intended(hasComponent(MainActivity.class.getName()));
    }
}
