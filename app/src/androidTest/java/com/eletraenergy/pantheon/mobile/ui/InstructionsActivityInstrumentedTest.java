package com.eletraenergy.pantheon.mobile.ui;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.eletraenergy.pantheon.mobile.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by Darlan on 14/02/2018.
 */

@RunWith(AndroidJUnit4.class)
public class InstructionsActivityInstrumentedTest extends ActivityInstrumentedTest {

    @Rule
    public ActivityTestRule<InstructionsActivity> activityTestRule = new ActivityTestRule<InstructionsActivity>(InstructionsActivity.class);

    @Test
    public void openInstructionsActivityTest() {
        onView(withId(R.id.toolbar)).check(matches(isDisplayed()));
        onView(withText(R.string.label_instructions)).check(matches(withParent(withId(R.id.toolbar))));
        onView(withId(R.id.scrollViewInstructions))
                .perform(swipeUp());
        onView(withId(R.id.versionTextView)).check(matches(isDisplayed()));
    }
}
