package com.eletraenergy.pantheon.mobile.security;

import android.util.Base64;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.crypto.spec.SecretKeySpec;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

/**
 * Created by Darlan on 15/02/2018.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({Base64.class})
public class HashSecurityTest {

    @InjectMocks
    HashSecurity hashSecurity;

    @Test
    public void generateHashTest() throws Exception {

        PowerMockito.mockStatic(Base64.class);

        String androidId = "6b1302d0e63d88e6";
        String hash = hashSecurity.calculateHash(androidId);
        assertEquals("null:null", hash);
    }

    @Test
    public void validateHashTest() throws Exception {
        String androidId = "6b1302d0e63d88e6";
        String hash = null;
        String key = null;
        try {
            hash = hashSecurity.calculateHash(androidId);
            SecretKeySpec keySpec = hashSecurity.createSecretKey(hashSecurity.eletraPassword.trim().toCharArray(),
                    hashSecurity.salt.trim().getBytes(), 40000, 128);
            String decryptedPassword = hashSecurity.decrypt(hash, keySpec);
            key = hashSecurity.encrypt(decryptedPassword + "-" + hashSecurity.eletraPassword, keySpec);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertNotNull("Erro ao gerar hash", hash);
        assertNotNull("Erro ao gerar chave", key);
        assertEquals(true,  hashSecurity.isHashValid(key, androidId));
    }
}
